﻿namespace ScheduleMaker.Logic
{
    public static class Constants
    {
        public const double FloatingPointTolerance = 0.001;
    }
}
