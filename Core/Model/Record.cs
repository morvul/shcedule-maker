﻿using System;

namespace ScheduleMaker.Logic.Model
{
    public abstract class Record<TEntity> 
        where TEntity : Record<TEntity>, new()
    {
        /// <summary>
        /// Идентификатор сущности.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Копирование значений полей объекта.
        /// </summary>
        /// <param name="destItem">Приемник значений.</param>
        public abstract void CloneTo(TEntity destItem);

        /// <summary>
        /// Создание копии объекта.
        /// </summary>
        public virtual TEntity Clone()
        {
            TEntity newInstance = new TEntity { Id = Id };
            CloneTo(newInstance);
            return newInstance;
        }

        /// <summary>
        /// Установка значений по умолчанию для полей записи.
        /// </summary>
        /// <returns>Признак того, что значение было установлено.</returns>
        public abstract bool SetDefaults();

        /// <summary>
        /// Определяет, содержат ли поля сущности искомый текст.
        /// </summary>
        /// <param name="findText">Текст фильтра.</param>
        /// <returns>
        /// True - нужно перевалидировать значения;
        /// False - не нужно.
        /// </returns>
        public abstract bool Contains(string findText);

        protected Record()
        {
            Id = Guid.Empty;
        }
    }
}
