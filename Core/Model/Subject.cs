﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Сущность "Предмет"
    /// </summary>
    public class Subject : Record<Subject>
    {
        #region Свойства

        /// <summary>
        /// Название предмета.
        /// </summary>
        [Index(IsUnique = true)]
        [StringLength(200)]
        public string Name { set; get; }

        public virtual ICollection<Lesson> Lessons { get; set; }

        public virtual ICollection<LearningPlan> LearningPlans { get; set; }

        #endregion

        #region Методы

        public override void CloneTo(Subject destItem)
        {
            destItem.Name = Name;
        }

        public override bool SetDefaults()
        {
            Name = "";
            return false;
        }

        public override bool Contains(string findText)
        {
            return Name.Contains(findText);
        }

        #endregion
    }
}
