﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ScheduleMaker.Logic.Model
{
    public enum CompletenceState
    {
        NotComplete,
        Complete,
        Overflow
    }
    public class LearningPlan : Record<LearningPlan>
    {
        private ICollection<Teacher> _lectionTeachers;
        private ICollection<Teacher> _practTeachers;
        private ICollection<Room> _practRooms;

        public Schedule Schedule { set; get; }

        public Group Group { set; get; }

        public Subject Subject { set; get; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int LessonsCount => (LectionsCount + PracticeCount);

        [NotMapped]
        public double LessonsFactCount => (LectionsFactCount + PracticeFactCount + UnspecifiedFactCount);

        public int LectionsCount { set; get; }
 
        [NotMapped]
        public CompletenceState LessonsCompletence => LessonsFactCount == LessonsCount
            ? CompletenceState.Complete
            : (LessonsFactCount > LessonsCount
                ? CompletenceState.Overflow
                : CompletenceState.NotComplete
            );

        [NotMapped]
        public CompletenceState LectionsCompletence => LectionsFactCount == LectionsCount
            ? CompletenceState.Complete
            : (LectionsFactCount > LectionsCount
                ? CompletenceState.Overflow
                : CompletenceState.NotComplete
            );


        [NotMapped]
        public CompletenceState PracticesCompletence => PracticeFactCount == PracticeCount
            ? CompletenceState.Complete
            : (PracticeFactCount > PracticeCount
                ? CompletenceState.Overflow
                : CompletenceState.NotComplete
            );

        public int PracticeCount { set; get; }
        
        [NotMapped]
        public double LectionsFactCount { set; get; }


        [NotMapped]
        public double PracticeFactCount => SeparatlesPracticeFactCount + Math.Max(Group1PracticeFactCount, Group2PracticeFactCount);

        [NotMapped]
        public double Group1PracticeFactCount { set; get; }

        [NotMapped]
        public double Group2PracticeFactCount { set; get; }

        [NotMapped]
        public double UnspecifiedFactCount { get; set; }

        [NotMapped]
        public double SeparatlesPracticeFactCount { get; internal set; }

        [NotMapped]
        public string LectionTeachersList => String.Join(",\n", LectionTeachers.Select(x => x.Name));

        public virtual ICollection<Teacher> LectionTeachers
        {
            set => _lectionTeachers = value;
            get => _lectionTeachers ?? (_lectionTeachers = new List<Teacher>());
        }

        [NotMapped]
        public string PractTeachersList => String.Join(",\n", PractTeachers.Select(x => x.Name));

        [NotMapped]
        public string PractRoomsList => string.Join(",\n", PractRooms.Select(x => x.Name));

        [NotMapped]
        public int StreamSize { get; internal set; }
        
        public virtual ICollection<Teacher> PractTeachers
        {
            set => _practTeachers = value;
            get => _practTeachers ?? (_practTeachers = new List<Teacher>());
        }

        public virtual ICollection<Room> PractRooms
        {
            set => _practRooms = value;
            get => _practRooms ?? (_practRooms = new List<Room>());
        }

        public override void CloneTo(LearningPlan destItem)
        {
            destItem.Group = Group;
            destItem.Subject = Subject;
            destItem.LectionsCount = LectionsCount;
            destItem.PracticeCount = PracticeCount;
            destItem.LectionsFactCount = LectionsFactCount;
            destItem.SeparatlesPracticeFactCount = SeparatlesPracticeFactCount;
            destItem.UnspecifiedFactCount = UnspecifiedFactCount;
            destItem.LectionTeachers = LectionTeachers.ToList();
            destItem.PractTeachers = PractTeachers.ToList();
            destItem.PractRooms = PractRooms.ToList();
            destItem.Schedule = Schedule;
            destItem.Group1PracticeFactCount = Group1PracticeFactCount;
            destItem.Group2PracticeFactCount = Group2PracticeFactCount;
        }

        public override bool SetDefaults()
        {
            LectionsCount = 0;
            PracticeCount = 0;
            return false;
        }

        public override bool Contains(string findText)
        {
            var result = LectionTeachersList.Contains(findText) ||
                          PractTeachersList.Contains(findText) ||
                          PractRoomsList.Contains(findText) ||
                          Group.FullName.Contains(findText) ||
                          Subject.Name.Contains(findText);
            if (int.TryParse(findText, out var bufInt))
            {
                result |= LessonsCount == bufInt;
                result |= LectionsCount == bufInt;
                result |= PracticeCount == bufInt;
                result |= LessonsFactCount == bufInt;
                result |= LectionsFactCount == bufInt;
                result |= PracticeFactCount == bufInt;
            }

            return result;
        }
    }
}
