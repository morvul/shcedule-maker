﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Сущность "Группа".
    /// </summary>
    public class Group : Record<Group>
    {
        #region Константы

        public const int DefaultMembersCount = 25;
        public const int StudyIsComplete = -1;
        public const int CourseStartDay = 1;
        public const int CourseStartMonth = 9;
        public const int DefaultCourseCount = 5;

        #endregion

        #region Свойства

        public static Group CurrentGroup { get; set; }

        /// <summary>
        /// Имя группы.
        /// </summary>
        [StringLength(200)]
        public string Name { set; get; }

        /// <summary>
        /// Имя группы с указанием текущего курса.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string FullName
        {
            get
            {
                if (Id == Guid.Empty)
                {
                    return Name;
                }

                string numberPart = Regex.Match(Name, @"\d+$").Value;
                string stringPart = Regex.Match(Name, @"^[^\d]+").Value;
                if (!string.IsNullOrEmpty(stringPart) && stringPart.Last() != '-')
                {
                    stringPart += '-';
                }

                if (StartLern > DateTime.Now)
                {
                    return stringPart + numberPart + "(" + StartLern.Year + ")";
                }

                if (CurrentCourse == StudyIsComplete)
                {
                    return stringPart + numberPart + "(" + EndLern.Year + ")";
                }

                return stringPart + CurrentCourse + numberPart;
            }
        }

        /// <summary>
        /// Имя группы с указанием числа элементов учебного плана.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string PlanName{
            get
            {
                if (Id == Guid.Empty)
                {
                    return Name;
                }

                var plannedCount = LearningPlans
                    .Where(x => x.Schedule.Id == Schedule.CurrentSchedule?.Id)
                    .Sum(x => x.LessonsCount);
                return $"{FullName} (Назначено часов: {plannedCount})";
            }
        }

        /// <summary>
        /// Количество курсов обучения.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int CourseСount
        {
            get
            {
                return (EndLern.Year - StartLern.Year +
                            (DateTime.UtcNow > new DateTime(DateTime.UtcNow.Year, CourseStartMonth, CourseStartDay) ? 1 : 0));
            }
        }

        /// <summary>
        /// Номер текущего курса обучения.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int CurrentCourse
        {
            get
            {
                if (DateTime.UtcNow.Year <= EndLern.Year)
                {
                    return (DateTime.UtcNow.Year - StartLern.Year +
                                    (DateTime.UtcNow > new DateTime(DateTime.UtcNow.Year, CourseStartMonth, CourseStartDay) ? 1 : 0));
                }
                return StudyIsComplete;
            }
        }

        /// <summary>
        /// Дата начала обучения группы.
        /// </summary>
        public DateTime StartLern { set; get; }

        /// <summary>
        /// Дата окончания обучения группы.
        /// </summary>
        public DateTime EndLern { set; get; }

        /// <summary>
        /// Число учащихся в группе.
        /// </summary>
        public int MembersCount { set; get; }

        public int Raiting { get; set; }

        public virtual ICollection<Lesson> Lessons { get; set; }

        public virtual ICollection<LearningPlan> LearningPlans { get; set; }

        #endregion

        #region Методы

        public override void CloneTo(Group destItem)
        {
            destItem.Name = Name;
            destItem.StartLern = StartLern;
            destItem.EndLern = EndLern;
            destItem.MembersCount = MembersCount;
            destItem.Raiting = Raiting;
        }

        public sealed override bool SetDefaults()
        {
            Name = "";
            int year = DateTime.Now <
                new DateTime(DateTime.Now.Year, CourseStartMonth, CourseStartDay)
                    ? DateTime.Now.Year - 1
                    : DateTime.Now.Year;
            StartLern = new DateTime(year, CourseStartMonth, CourseStartDay);
            EndLern = DateTime.UtcNow.AddYears(DefaultCourseCount);
            MembersCount = DefaultMembersCount;
            Raiting = 0;
            return true;
        }

        public override bool Contains(string findText)
        {
            bool result = FullName.Contains(findText);

            int bufInt;
            if (int.TryParse(findText, out bufInt))
            {
                result |= MembersCount == bufInt || StartLern.Year == bufInt || EndLern.Year == bufInt ||
                    CourseСount == bufInt || CurrentCourse == bufInt || Raiting == bufInt;
            }

            return result;
        }

        #endregion

        #region Конструкторы

        static Group()
        {
            CurrentGroup = null;
        }

        public Group()
        {
            SetDefaults();
        }

        #endregion

    }
}
