﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using ScheduleMaker.Logic.Properties;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Сущность лекционный час.
    /// </summary>
    public class LessonHour : Record<LessonHour>
    {
        static LessonHour()
        {
            BreakSize = Settings.Default.LessonBreak;
        }

        /// <summary>
        /// День недели.
        /// </summary>
        public DayOfWeek Day { set; get; }

        [NotMapped]
        public string DayName
        {
            get
            {
                var dayName = Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetDayName(Day);
                dayName = dayName.First().ToString().ToUpper() + dayName.Substring(1);
                return dayName;
            }
        }

        /// <summary>
        /// Время начала лекции.
        /// </summary>
        public DateTime StartTime { set; get; }

        /// <summary>
        /// Расписание, к которому привязан учебный час.
        /// </summary>
        public Schedule Schedule { set; get; }

        /// <summary>
        /// Строковое представление учебного часа.
        /// </summary>
        [NotMapped]
        public string DisplayName => $"{StartTime:t} - {EndTime:t}";

        /// <summary>
        /// Строковое представление учебного часа.
        /// </summary>
        [NotMapped]
        public bool HasAssignments
        {
            get { return Lessons.Any(); }
        }

        [NotMapped]
        public DateTime EndTime
        {
            get
            {
                return (StartTime + Schedule.LessonDuration);
            }
        }

        public virtual ICollection<Lesson> Lessons { set; get; }

        public override void CloneTo(LessonHour destItem)
        {
            destItem.Day = Day;
            destItem.StartTime = StartTime;
            destItem.Schedule = Schedule;
            destItem.Lessons = Lessons;
            destItem.TeacherForFilter = TeacherForFilter;
        }

        public override bool SetDefaults()
        {
            var hoursForDay = Schedule.LessonHours.Where(x => x.Day == Day).ToList();
            StartTime = hoursForDay.Any()
                ? hoursForDay.Max(x => x.StartTime) + Schedule.LessonDuration + BreakSize
                : Schedule.LessonsStart;
            return true;
        }

        [NotMapped]
        public Teacher TeacherForFilter { set; get; }

        [NotMapped]
        public Room AuditoryForFilter { set; get; }

        public override bool Contains(string findText)
        {
            throw new NotImplementedException();
        }

        public LessonHour()
        {
            Schedule = new Schedule();
            Schedule.SetDefaults();
            Lessons = new List<Lesson>();
            TeacherForFilter = null;
        }

        public LessonHour(Schedule schedule, DayOfWeek day)
            : this()
        {
            Schedule = schedule;
            Day = day;
            SetDefaults();
        }

        public bool HasAssignmentsForTeacher
        {
            get
            {
                if (TeacherForFilter == null) return false;
                bool hasAnyAssignment = false;
                foreach (var lesson in Lessons)
                {
                    if (lesson.Teachers.Any(z => z.Id == TeacherForFilter.Id))
                    {
                        hasAnyAssignment = true;
                        lesson.IsFiltered = false;
                    }
                    else
                    {
                        lesson.IsFiltered = true;
                    }
                }

                return hasAnyAssignment;
            }
        }

        public bool HasAssignmentsForRoom
        {
            get
            {
                if (AuditoryForFilter == null) return false;
                bool hasAnyAssignment = false;
                foreach (var lesson in Lessons)
                {
                    if (lesson.Room != null && lesson.Room.Id == AuditoryForFilter.Id)
                    {
                        hasAnyAssignment = true;
                        lesson.IsFiltered = false;
                    }
                    else
                    {
                        lesson.IsFiltered = true;
                    }
                }

                return hasAnyAssignment;
            }
        }

        public static TimeSpan BreakSize { get; set; }

        public LessonHour(LessonHour source)
        {
            source.CloneTo(this);
        }


        public bool HasAssignmentForTeacher(Teacher teacherForFilter)
        {
            if (teacherForFilter == null) return false;
            TeacherForFilter = teacherForFilter;
            return HasAssignmentsForTeacher;
                
        }

        public bool HasAssignmentForRoom(Room auditoryForFilter)
        {
            if (auditoryForFilter == null) return false;
            AuditoryForFilter = auditoryForFilter;
            return HasAssignmentsForRoom;
        }

        public void UpdateLessonsFilters()
        {
            foreach (var lesson in Lessons)
            {
                lesson.IsFiltered = false;
                if (lesson.Room != null && AuditoryForFilter !=null)
                {
                    lesson.IsFiltered = lesson.Room.Id != AuditoryForFilter.Id;
                }
                if (TeacherForFilter != null)
                {
                    lesson.IsFiltered = lesson.Teachers.All(x=>x.Id != TeacherForFilter.Id);
                }
            }
        }
    }
}
