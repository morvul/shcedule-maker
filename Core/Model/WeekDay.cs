﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ScheduleMaker.Logic.Model
{
    public class WeekDay
    {
        public WeekDay(DayOfWeek weekDay, Schedule schedule)
        {
            Day = weekDay;
            Schedule = schedule;
            LessonHours = schedule.LessonHours.Where(hour => hour.Day == weekDay).OrderBy(x=>x.StartTime).ToList();
            TeacherForFilter = null;
            AuditoryForFilter = null;
        }

        public WeekDay(DayOfWeek weekDay, Schedule schedule, Teacher teacher, Room room) 
            : this(weekDay, schedule)
        {
            TeacherForFilter = teacher;
            AuditoryForFilter = room;
            foreach (var lessonHour in LessonHours)
            {
                lessonHour.TeacherForFilter = TeacherForFilter;
                lessonHour.AuditoryForFilter = AuditoryForFilter;
                lessonHour.UpdateLessonsFilters();
            }
        }

        public Schedule Schedule { set; get; }

        public DayOfWeek Day { set; get; }

        public string Name
        {
            get
            {
                var dayName = Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetDayName(Day);
                return dayName.First().ToString().ToUpper() + dayName.Substring(1);
            }
        }

        public bool HasAssignments
        {
            get { return (LessonHours.Any()); }
        }

        public IEnumerable<LessonHour> LessonHours { set; get; }

        public bool HasAssignmentsForTeacher
        {
            get
            {
                if (TeacherForFilter == null) return false;
                return LessonHours.Any(x=>x.HasAssignmentForTeacher(TeacherForFilter));
            }
        }

        public bool HasAssignmentsForRoom
        {
            get
            {
                if (AuditoryForFilter == null) return false;
                return LessonHours.Any(x => x.HasAssignmentForRoom(AuditoryForFilter));
            }
        }

        public Teacher TeacherForFilter { set; get; }

        public Room AuditoryForFilter { set; get; }
    }
}
