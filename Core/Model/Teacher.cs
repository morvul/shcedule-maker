﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Сущность "Преподаватель"
    /// </summary>
    public class Teacher : Record<Teacher>
    {
        static Teacher()
        {
            CurrentTeacher = null;
        }

        #region Свойства

        [Index(IsUnique = true)]
        [StringLength(200)] 
        public string Name { set; get; }

        public string Description { get; set; }

        public virtual ICollection<Lesson> Lessons { get; set; }

        public static Teacher CurrentTeacher { get; set; }

        [StringLength(200)]
        public string Rank { get; set; }

        public int Raiting { get; set; }

        [NotMapped]
        public virtual ICollection<LearningPlan> LearningPlans => PractLearningPlans.Union(LectLearningPlans).ToList();

        public virtual ICollection<LearningPlan> PractLearningPlans { get; internal set; }

        public virtual ICollection<LearningPlan> LectLearningPlans { get; internal set; }

        #endregion

        #region Методы

        public override void CloneTo(Teacher destItem)
        {
            destItem.Name = Name;
            destItem.Rank = Rank;
            destItem.Description = Description;
            destItem.Raiting = Raiting;
        }

        public override bool SetDefaults()
        {
            Name = "";
            Rank = "";
            Raiting = 0;
            return false;
        }

        public override bool Contains(string findText)
        {
            var result = Name.Contains(findText) || (Rank != null && Rank.Contains(findText));
            int bufInt;
            if (int.TryParse(findText, out bufInt))
            {
                result |= Raiting == bufInt;
            }

            return result;
        }

        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}
