﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using ScheduleMaker.Logic.Helpers;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Разделение на подгруппы
    /// </summary>
    public enum SubGroup
    {
        [Description("Без разделения")]
        Separateless,

        [Description("Подгруппа 1")]
        Group1,

        [Description("Подгруппа 2")]
        Group2
    }

    /// <summary>
    /// Типы занятий
    /// </summary>
    public enum LessonType
    {

        [Description("Не определен")]
        Unspecified,

        [Description("Лекция")]
        Lection,

        [Description("Практика")]
        Practice
    }

    /// <summary>
    /// Типы недель
    /// </summary>
    public enum WeekType
    {
        [Description("Обычная")]
        Simple,

        [Description("Над чертой")]
        UpLine,

        [Description("Под чертой")]
        UnderLine
    }

    /// <summary>
    /// Сущность "Занятие".
    /// </summary>
    public class Lesson : Record<Lesson>
    {
        private ICollection<Teacher> _teachers;
        private LessonHour _lessonHour;

        public LessonHour LessonHour
        {
            get { return _lessonHour; }
            set { _lessonHour = value; }
        }

        public Group Group { set; get; }

        public Subject Subject { set; get; }

        public Room Room { set; get; }

        [NotMapped]
        public string TeachersList
        {
            get
            {
                return SubGroup == SubGroup.Separateless
                    ? String.Join(", ", Teachers.Select(x => x.Rank + " " + x.Name))
                    : String.Join(", \n", Teachers.Select(x => x.Name));
            }
        }

        public virtual ICollection<Teacher> Teachers
        {
            set { _teachers = value; }
            get { return _teachers ?? (_teachers = new List<Teacher>()); }
        }

        public LessonType LessonType { set; get; }

        public WeekType WeekType { set; get; }

        public SubGroup SubGroup { set; get; }

        public string Description { get; set; }

        public override void CloneTo(Lesson destItem)
        {
            destItem.Group = Group;
            destItem.Room = Room;
            destItem.Subject = Subject;
            destItem.Teachers = Teachers;
            destItem.LessonHour = LessonHour;
            destItem.LessonType = LessonType;
            destItem.WeekType = WeekType;
            destItem.SubGroup = SubGroup;
            destItem.Description = Description;
            destItem.IsFiltered = IsFiltered;
        }

        public override bool SetDefaults()
        {
            WeekType = WeekType.Simple;
            SubGroup = SubGroup.Separateless;
            LessonType = LessonType.Unspecified;
            Description = "";
            IsFiltered = false;
            return false;
        }

        public override bool Contains(string findText)
        {
            return Group.FullName.Contains(findText) || Subject.Name.Contains(findText)
                || TeachersList.Contains(findText) || LessonType.Description().Contains(findText)
                || WeekType.Description().Contains(findText)
                || SubGroup.Description().Contains(findText)
                || (Description != null && Description.Contains(findText)) 
                || (Room != null && Room.Name.Contains(findText));
        }

        [NotMapped]
        public bool IsFiltered { get; set; }
    }
}
