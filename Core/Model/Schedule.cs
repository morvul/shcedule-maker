﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Сущность "Расписание".
    /// </summary>
    public class Schedule : Record<Schedule>
    {
        public static Schedule CurrentSchedule { set; get; }

        static Schedule()
        {
            CurrentSchedule = null;
        }

        public Schedule()
        {
            LessonHours = new List<LessonHour>();
            LearningPlans = new List<LearningPlan>();
        }

        /// <summary>
        /// Название расписания.
        /// </summary>
        [Index(IsUnique = true)]
        [StringLength(200)]
        public string Name { get; set; }

        /// <summary>
        /// Время начала занятий.
        /// </summary>
        public DateTime LessonsStart { get; set; }

        /// <summary>
        /// Длительность занятия.
        /// </summary>
        [NotMapped]
        public TimeSpan LessonDuration
        {
            get { return TimeSpan.FromTicks(TimeSpanTicks); }
            set { TimeSpanTicks = value.Ticks; }
        }

        /// <summary>
        /// MSSQL timespan bugfix.
        /// </summary>
        public Int64 TimeSpanTicks { get; set; }   

        /// <summary>
        /// Список назначенных занятий.
        /// </summary>
        public virtual ICollection<LessonHour> LessonHours { get; set; }

        public virtual ICollection<LearningPlan> LearningPlans { get; set; }

        public DateTime FirstDay { get; set; }

        public DateTime LastDay { get; set; }

        public override void CloneTo(Schedule destItem)
        {
            destItem.Name = Name;
            destItem.LessonsStart = LessonsStart;
            destItem.LessonDuration = LessonDuration;
            destItem.FirstDay = FirstDay;
            destItem.LastDay = LastDay;
        }

        public override bool SetDefaults()
        {
            if (DateTime.Now < new DateTime(DateTime.Now.Year, Group.CourseStartMonth, Group.CourseStartDay))
            {
                Name = (DateTime.Now.Year - 1) + "/" + DateTime.Now.Year + " (2 семестр)";
            }
            else
            {
                Name = DateTime.UtcNow.Year + "/" + (DateTime.Now.Year + 1) + " (1 семестр)";
            }

            LessonsStart = Properties.Settings.Default.LessonsStart;
            LessonDuration = Properties.Settings.Default.LessonDuration;
            FirstDay = DateTime.Now;
            LastDay = DateTime.Now;
            return true;
        }

        public override bool Contains(string findText)
        {
            bool result = Name.Contains(findText) 
                || LessonsStart.ToString(CultureInfo.InvariantCulture).Contains(findText) 
                || LessonDuration.ToString().Contains(findText)
                || FirstDay.ToString("d").Contains(findText)
                || LastDay.ToString("d").Contains(findText);
            return result;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
