﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Сущность "Аудитория"
    /// </summary>
    public class Room : Record<Room>
    {
        const int DefaultStudentsCount = 30;

        static Room()
        {
            CurrentRoom = null;
        }

        #region Свойства

        /// <summary>
        /// Наименование аудитории.
        /// </summary>
        [Index(IsUnique = true)]
        [StringLength(200)] 
        public string Name { set; get; }

        /// <summary>
        /// Максимальное число мест.
        /// </summary>
        public int? SeatsCount { set; get; }

        /// <summary>
        /// Описание аудитории.
        /// </summary>
        [StringLength(400)] 
        public string Description { set; get; }

        public static Room CurrentRoom{ get; set; }

        [NotMapped]
        public double Occupied { get; internal set; }
        public ICollection<LearningPlan> PracticeLearningPlans { get; internal set; }

        #endregion

        #region Методы

        public override void CloneTo(Room destItem)
        {
            destItem.Name = Name;
            destItem.SeatsCount = SeatsCount;
            destItem.Description = Description;
        }

        public override bool SetDefaults()
        {
            Name = "";
            Description = "";
            SeatsCount = DefaultStudentsCount;
            return false;
        }

        public override bool Contains(string findText)
        {
            return (Name.Contains(findText) || Description.Contains(findText));
        }


        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}
