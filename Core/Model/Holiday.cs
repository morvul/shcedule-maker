﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Сущность "Праздник".
    /// </summary>
    public sealed class Holiday : Record<Holiday>
    {
        [Index(IsUnique = true)]
        [StringLength(200)]
        public string Name { get; set; }

        public DateTime Date { get; set; }

        public override void CloneTo(Holiday destItem)
        {
            destItem.Name = Name;
            destItem.Date = Date;
        }

        public override bool SetDefaults()
        {
            Date = DateTime.Today;
            Name = "";
            return true;
        }

        public override bool Contains(string findText)
        {
            bool result = Name.Contains(findText) || Date.ToString("d").Contains(findText);
            return result;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
