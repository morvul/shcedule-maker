﻿using System.Data.Entity;

namespace ScheduleMaker.Logic.Model
{
    /// <summary>
    /// Контекст данных
    /// </summary>
    public class DataContext : DbContext
    {
        /// <summary>
        /// Список групп.
        /// </summary>
        public DbSet<Group> Groups { get; set; }

        /// <summary>
        /// Список предметов.
        /// </summary>
        public DbSet<Subject> Subjects { get; set; }

        /// <summary>
        /// Список преподавателей.
        /// </summary>
        public DbSet<Teacher> Teachers { get; set; }

        /// <summary>
        /// Список аудиторий.
        /// </summary>
        public DbSet<Room> Rooms { get; set; }

        /// <summary>
        /// Список расписаний.
        /// </summary>
        public DbSet<Schedule> Schedules { get; set; }

        /// <summary>
        /// Список занятий.
        /// </summary>
        public DbSet<Lesson> Lessons { get; set; }

        /// <summary>
        /// Список праздников.
        /// </summary>
        public DbSet<Holiday> Holidays { get; set; }

        /// <summary>
        /// Учебные планы.
        /// </summary>
        public DbSet<LearningPlan> LearningPlans { get; set; }

        /// <summary>
        /// Список учебных часов.
        /// </summary>
        public DbSet<LessonHour> LessonHours { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LessonHour>()
                .HasOptional(a => a.Schedule)
                .WithMany(x => x.LessonHours)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Lesson>()
                .HasOptional(a => a.LessonHour)
                .WithMany(x => x.Lessons)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Lesson>()
                .HasOptional(a => a.Subject)
                .WithMany(x => x.Lessons)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Lesson>()
                .HasOptional(a => a.Group)
                .WithMany(x => x.Lessons)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Lesson>()
                .HasMany(s => s.Teachers)
                .WithMany(c => c.Lessons)
                .Map(cs =>
                {
                    cs.MapLeftKey("TeacherRefId");
                    cs.MapRightKey("LessonRefId");
                    cs.ToTable("TeacherAssignments");
                });
            modelBuilder.Entity<LearningPlan>()
               .HasOptional(a => a.Schedule)
               .WithMany(x => x.LearningPlans)
               .WillCascadeOnDelete(true);
            modelBuilder.Entity<LearningPlan>()
               .HasOptional(a => a.Subject)
               .WithMany(x => x.LearningPlans)
               .WillCascadeOnDelete(true);
            modelBuilder.Entity<LearningPlan>()
                .HasOptional(a => a.Group)
                .WithMany(x => x.LearningPlans)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<LearningPlan>()
                .HasMany(a => a.PractTeachers)
                .WithMany(x => x.PractLearningPlans)
                .Map(cs =>
                {
                    cs.MapLeftKey("LearningPlanRefId");
                    cs.MapRightKey("TeacherRefId");
                    cs.ToTable("LearningPlanPractTichers");
                });
            modelBuilder.Entity<LearningPlan>()
                .HasMany(a => a.LectionTeachers)
                .WithMany(x => x.LectLearningPlans)
                .Map(cs =>
                {
                    cs.MapLeftKey("LearningPlanRefId");
                    cs.MapRightKey("TeacherRefId");
                    cs.ToTable("LearningPlanLecttTichers");
                });
            modelBuilder.Entity<LearningPlan>()
                .HasMany(a => a.PractRooms)
                .WithMany(x => x.PracticeLearningPlans)
                .Map(cs =>
                {
                    cs.MapLeftKey("LearningPlanRefId");
                    cs.MapRightKey("PracticeRoomRefId");
                    cs.ToTable("PracticeRooms");
                });
        }
    }
}
