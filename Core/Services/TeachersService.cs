﻿using System.Linq;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис "Преподаватели"
    /// </summary>
    public class TeachersService : BaseReferenceService<Teacher>
    {
        public TeachersService(DataContext dataContext)
            : base(dataContext)
        {
        }

        public override IQueryable<Teacher> GetAll()
        {
            return base.GetAll().OrderBy(x=>x.Name);
        }
    }
}
