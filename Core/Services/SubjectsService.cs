﻿using System.Linq;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис "Предметы"
    /// </summary>
    public class SubjectsService : BaseReferenceService<Subject>
    {
        public SubjectsService(DataContext dataContext)
            : base(dataContext)
        {
        }

        public override IQueryable<Subject> GetAll()
        {
            return base.GetAll().OrderBy(x=>x.Name);
        }
    }
}
