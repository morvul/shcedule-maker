﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис "Расписания"
    /// </summary>
    public class SchedulesService : BaseReferenceService<Schedule>
    {
        private readonly LessonHoursService _lessonHoursService;
        private readonly LearningPlansService _learningPlansService;

        public SchedulesService(DataContext dataContext, 
            LessonHoursService lessonHoursService, 
            LearningPlansService learningPlansService)
            : base(dataContext)
        {
            _lessonHoursService = lessonHoursService;
            _learningPlansService = learningPlansService;
        }

        public IEnumerable<WeekDay> GetWeekDays(Schedule schedule,
            Teacher teacher = null, Room auditory = null)
        {
            var result = new List<WeekDay>();
            if (schedule != null)
            {
                foreach (DayOfWeek weekDay in Enum.GetValues(typeof(DayOfWeek)))
                {
                    var day = new WeekDay(weekDay, schedule, teacher, auditory);
                    result.Add(day);
                }
            }

            return result.OrderBy(x => x.Day == DayOfWeek.Sunday ? (DayOfWeek) 7 : x.Day);
        }

        public override int Update(Schedule record)
        {
            TimeSpan def = TimeSpan.Zero;
            var oldRecord = GetById(record.Id);
            if (oldRecord != null)
            {
                oldRecord = oldRecord.Clone();
                def = (record.LessonDuration - oldRecord.LessonDuration)
                      + (record.LessonsStart - oldRecord.LessonsStart);
            }

            int result = base.Update(record);
            if (result > 0 && def != TimeSpan.Zero)
            {
                _lessonHoursService.ShiftLessons(oldRecord, def);
            }

            return result;
        }

        public IEnumerable<WeekDay> GetNotFreeWeekDays(Schedule schedule, DayOfWeek currentDay)
        {
            var result = new List<WeekDay>();
            if (schedule != null)
            {
                foreach (DayOfWeek weekDay in Enum.GetValues(typeof(DayOfWeek)))
                {
                    if (weekDay != currentDay && schedule.LessonHours.Any(x => x.Day == weekDay))
                    {
                        result.Add(new WeekDay(weekDay, schedule));
                    }
                }
            }

            return result.OrderBy(x => x.Day == DayOfWeek.Sunday ? (DayOfWeek) 7 : x.Day);
        }

        public IEnumerable<WeekDay> GetWeekDaysForTeacher(Schedule schedule, Teacher teacher)
        {
            return GetWeekDays(schedule, teacher);
        }

        public IEnumerable<WeekDay> GetWeekDaysForAuditory(Schedule schedule, Room auditory)
        {
            return GetWeekDays(schedule, null, auditory);
        }

        public Schedule AddCopy(Schedule schedule)
        {
            var newSchedule = schedule.Clone();
            newSchedule.Id = Guid.Empty;
            newSchedule.Name = GetCopyName(newSchedule.Name);
            Add(newSchedule);
            CopyLessonHours(schedule, newSchedule);
            CopyLerningPlan(schedule, newSchedule);
            return newSchedule;
        }

        private void CopyLerningPlan(Schedule sourceSchedule, Schedule destSchedule)
        {
            var learningPlanCopy = new List<LearningPlan>();
            foreach (var learningPlanItem in sourceSchedule.LearningPlans)
            {
                var learningPlanItemCopy = learningPlanItem.Clone();
                learningPlanItemCopy.Id = Guid.Empty;
                learningPlanItemCopy.Schedule = destSchedule;
                learningPlanCopy.Add(learningPlanItemCopy);
            }

            _learningPlansService.AddRange(learningPlanCopy);
        }

        private void CopyLessonHours(Schedule sourceSchedule, Schedule destSchedule)
        {
            var hoursCopy = new List<LessonHour>();
            foreach (var lessonHour in sourceSchedule.LessonHours)
            {
                var hourCopy = lessonHour.Clone();
                hourCopy.Id = Guid.Empty;
                hourCopy.Schedule = destSchedule;
                hourCopy.Lessons = new List<Lesson>();
                hoursCopy.Add(hourCopy);
            }

            _lessonHoursService.AddRange(hoursCopy);
        }

        private string GetCopyName(string name)
        {
            var todayDate = DateTime.Now.ToString("G");
            var pattern = @"[\d]{2}[.][\d]{2}[.][\d]{4} [\d]{1,2}[:][\d]{2}[:][\d]{2}";
            var result = Regex.Replace(name, pattern, todayDate);
            if (name.Equals(result))
            {
                result += $" (дата генерации {todayDate})";
            }

            return result;
        }
    }
}