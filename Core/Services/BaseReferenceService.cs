﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Базовая реализация сервиса типа "Справочник".
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности, хранимой в справочнике.</typeparam>
    public abstract class BaseReferenceService<TEntity> where TEntity : Record<TEntity>, new()
    {
        #region Конструкторы

        public BaseReferenceService(DataContext dataContext)
        {
            DataContext = dataContext;
        }

        #endregion

        #region Свойства 

        /// <summary>
        /// Контекст данных.
        /// </summary>
        public DataContext DataContext { get; private set; }

        #endregion

        #region Методы

        /// <summary>
        /// Запрос всех записей справочника.
        /// </summary>
        public virtual IQueryable<TEntity> GetAll()
        {
            return DataContext.Set<TEntity>();
        }

        /// <summary>
        /// Добавление записи в справочник.
        /// </summary>
        /// <param name="newRecord">Добавляемая запись.</param>
        /// <returns>Чисто модифицированных записей.</returns>
        public virtual int Add(TEntity newRecord)
        {
            if (newRecord.Id == Guid.Empty)
            {
                newRecord.Id = Guid.NewGuid();
            }

            DataContext.Set<TEntity>().Add(newRecord);
            return DataContext.SaveChanges();
        }

        /// <summary>
        /// Удаление записи из справочника.
        /// </summary>
        /// <param name="record">Удаляемая запись.</param>
        /// <returns>Чисто модифицированных записей.</returns>
        public virtual int Delete(TEntity record)
        {
            DataContext.Set<TEntity>().Remove(record);
            return DataContext.SaveChanges();
        }

        /// <summary>
        /// Изменение записи справочника.
        /// </summary>
        /// <param name="record">Изменяемая запись.</param>
        /// <returns>Чисто модифицированных записей.</returns>
        public virtual int Update(TEntity record)
        {
            if (record != null)
            {
                TEntity efEntity = DataContext.Set<TEntity>().FirstOrDefault(x => x.Id == record.Id);
                if (efEntity != null)
                {
                    record.CloneTo(efEntity);
                    return DataContext.SaveChanges();
                }
            }
            return 0;
        }

        /// <summary>
        /// Проверка на сущесвование записи в справочнике.
        /// </summary>
        public bool Exists(Func<TEntity, bool> func)
        {
            return DataContext.Set<TEntity>().Where(func).Any();
        }

        /// <summary>
        /// Запрос записи по полю "Id".
        /// </summary>
        public virtual TEntity GetById(Guid id)
        {
            return DataContext.Set<TEntity>().FirstOrDefault(rec => rec.Id == id);
        }


        public int AddRange(List<TEntity> newItems)
        {
            foreach (var newRecord in newItems)
            {
                if (newRecord.Id == Guid.Empty)
                {
                    newRecord.Id = Guid.NewGuid();
                }
            }

            DataContext.Set<TEntity>().AddRange(newItems);
            return DataContext.SaveChanges();
        }

        #endregion
    }
}
