﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    public class LearningPlansService : BaseReferenceService<LearningPlan>
    {
        private readonly LessonHoursService _lessonHoursService;
        private readonly LessonsService _lessonsService;

        public LearningPlansService(DataContext dataContext,
            LessonHoursService lessonHoursService,
            LessonsService lessonsService)
            : base(dataContext)
        {
            _lessonHoursService = lessonHoursService;
            _lessonsService = lessonsService;
        }

        /// <summary>
        /// Получение всех учебных планов
        /// </summary>
        /// <returns>Список учебных планов</returns>
        public override IQueryable<LearningPlan> GetAll()
        {
            var records = base.GetAll().Include(x => x.Subject).Include(x => x.Schedule);
            foreach (var record in records)
            {
                UpdateFactLessonCounts(record);
            }

            return records;
        }

        /// <summary>
        /// Получение учебного плана по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор учебного плана</param>
        /// <returns>Учебный план</returns>
        public override LearningPlan GetById(Guid id)
        {
            var record = base.GetById(id);
            UpdateFactLessonCounts(record);
            return record;
        }

        /// <summary>
        /// Получение учебного плана по предмету расписанию и группе
        /// </summary>
        /// <param name="subject">Предмет</param>
        /// <param name="schedule">Расписание</param>
        /// <param name="group">Группа</param>
        /// <returns>Учебный план</returns>
        public LearningPlan GetPlanItem(Subject subject, Schedule schedule,
            Group group)
        {
            if (subject == null || schedule == null || group == null)
            {
                return null;
            }

            var planItem = DataContext.LearningPlans.FirstOrDefault(x =>
                x.Schedule.Id == schedule.Id && x.Group.Id == group.Id &&
                x.Subject.Id == subject.Id);
            return planItem;
        }

        /// <summary>
        /// Обновление полей записей учебного плана
        /// </summary>
        /// <param name="record">Обновленные записи</param>
        public void UpdateFactLessonCounts(LearningPlan record)
        {
            record.LectionsFactCount = 0;
            record.Group1PracticeFactCount = 0;
            record.Group2PracticeFactCount = 0;
            record.SeparatlesPracticeFactCount = 0;
            record.UnspecifiedFactCount = 0;
            if (record.Schedule.FirstDay > record.Schedule.LastDay)
            {
                return;
            }

            var day = record.Schedule.FirstDay;
            var holidaysDates = DataContext.Holidays.Select(x => x.Date).ToList();
            var scheduleDays = _lessonHoursService.GetWeekDaysLessons(record.Schedule, record.Group);
            var weekStart = day.DayOfWeek;
            var currentWeekType = WeekType.UpLine;
            while (day <= record.Schedule.LastDay)
            {
                if (!holidaysDates.Any(x => x.Date.Month == day.Month && x.Date.Day == day.Day))
                {
                    var dayHours = scheduleDays.ContainsKey(day.DayOfWeek)
                        ? scheduleDays[day.DayOfWeek]
                        : new List<LessonHour>();
                    foreach (var dayHour in dayHours)
                    {
                        var weekTypeLessons = dayHour.Lessons
                            .Where(x => x.Subject.Id == record.Subject.Id &&
                                (x.WeekType == currentWeekType || x.WeekType == WeekType.Simple))
                            .ToList();
                        record.SeparatlesPracticeFactCount += GetPlannedLessonsCount(weekTypeLessons, LessonType.Practice, SubGroup.Separateless);
                        record.Group1PracticeFactCount += GetPlannedLessonsCount(weekTypeLessons, LessonType.Practice, SubGroup.Group1);
                        record.Group2PracticeFactCount += GetPlannedLessonsCount(weekTypeLessons, LessonType.Practice, SubGroup.Group2);
                        record.LectionsFactCount += GetPlannedLessonsCount(weekTypeLessons, LessonType.Lection);
                        record.UnspecifiedFactCount += GetPlannedLessonsCount(weekTypeLessons, LessonType.Unspecified);
                    }
                }

                day = day.AddDays(1);
                if (day.DayOfWeek == weekStart)
                {
                    currentWeekType = currentWeekType == WeekType.UpLine
                        ? WeekType.UnderLine
                        : WeekType.UpLine;
                }
            }
        }

        public IEnumerable<LearningPlan> GetForScheduleId(Guid scheduleId)
        {
            var records = base.GetAll()
                .Include(x => x.Subject).Include(x => x.Schedule)
                .Where(x => x.Schedule.Id == scheduleId)
                .ToList();
            foreach (var record in records)
            {
                UpdateFactLessonCounts(record);
            }

            return records;
        }

        public double GetUnassignedLessonsTotal(Schedule schedule)
        {
            var unassignedForGroups = GetUnassignedLessonsCounts(schedule);
            var result = unassignedForGroups.Sum(x => x.Value);
            return result;
        }

        private double GetPlannedLessonsCount(List<Lesson> lessons, LessonType type, SubGroup? subGroup = null)
        {
            var query = lessons.Where(x => x.LessonType == type);
            if (subGroup != null)
            {
                query = query.Where(x => x.SubGroup == subGroup);
                return query.Select(x => 1).Sum();
            }

            return query.Select(x => x.SubGroup == SubGroup.Separateless ? 1 : 0.5).Sum();
        }

        /// <summary>
        /// Получение общего запланированного количества занятий для расписания
        /// </summary>
        /// <param name="schedule">Расписание</param>
        /// <returns>Количество занятий</returns>
        public Dictionary<Group, double> GetPlannedLessonsCounts(Schedule schedule)
        {
            var result = new Dictionary<Group, double>();
            if (schedule == null)
            {
                return result;
            }

            var plannedPracticeCounts = DataContext.LearningPlans
                .Where(x => x.Schedule.Id == schedule.Id)
                .GroupBy(x => x.Group)
                .Select(x => new
                {
                    Group = x.Key,
                    PlannedLessons = x.Select(y => y.PracticeCount)
                        .DefaultIfEmpty(0)
                        .Sum()
                }).ToList();
            var plannedLectionsCounts = DataContext.LearningPlans
                .Where(x => x.Schedule.Id == schedule.Id)
                .GroupBy(x => x.Group)
                .Select(x => new
                {
                    Group = x.Key,
                    PlannedLessons = x.Select(y => y.LectionsCount)
                        .DefaultIfEmpty(0)
                        .Sum()
                }).ToList();
            foreach (var plannedPracticeCount in plannedPracticeCounts)
            {
                AppendToGroupsDictionary(result, plannedPracticeCount.Group, plannedPracticeCount.PlannedLessons);
            }

            foreach (var plannedLectionsCount in plannedLectionsCounts)
            {
                AppendToGroupsDictionary(result, plannedLectionsCount.Group, plannedLectionsCount.PlannedLessons);
            }

            return result;
        }

        public double GetMaxPlannedLessonsCount(Schedule schedule)
        {
            var plannedLessonsCounts = GetPlannedLessonsCounts(Schedule.CurrentSchedule);
            var maxPlannedLessonsCount = plannedLessonsCounts.Select(x => x.Value).DefaultIfEmpty(0).Max();
            return maxPlannedLessonsCount;
        }

        public Dictionary<Group, double> GetUnassignedLessonsCounts(Schedule schedule)
        {
            var plannedLessonsCounts = GetPlannedLessonsCounts(schedule);
            var factLessonsCounts = _lessonsService.GetFactLessonsCounts(schedule);
            var result = new Dictionary<Group, double>();
            foreach (var plannedLessonsCount in plannedLessonsCounts)
            {
                var unassignedCount = plannedLessonsCount.Value;
                if (factLessonsCounts.ContainsKey(plannedLessonsCount.Key.Id))
                {
                    if (factLessonsCounts[plannedLessonsCount.Key.Id] < plannedLessonsCounts[plannedLessonsCount.Key])
                    {
                        unassignedCount = plannedLessonsCounts[plannedLessonsCount.Key] -
                                          factLessonsCounts[plannedLessonsCount.Key.Id];
                    }
                    else
                    {
                        unassignedCount = 0;
                    }
                }

                if (unassignedCount > 0)
                {
                    result[plannedLessonsCount.Key] = unassignedCount;
                }
            }

            return result;
        }

        private void AppendToGroupsDictionary(Dictionary<Group, double> result, Group group, int plannedLessons)
        {
            var existingkey = result.Keys.FirstOrDefault(x => x.Id == group.Id);
            if (existingkey == null)
            {
                result.Add(group, plannedLessons);
            }
            else
            {
                result[existingkey] += plannedLessons;
            }
        }

        public IEnumerable<LearningPlan> GetForGroup(Guid scheduleId, Guid groupId)
        {
            var records = base.GetAll()
                .Include(x => x.Subject).Include(x => x.Schedule)
                .Where(x => x.Schedule.Id == scheduleId && x.Group.Id == groupId)
                .ToList();
            foreach (var record in records)
            {
                UpdateFactLessonCounts(record);
            }

            return records;
        }
    }
}
