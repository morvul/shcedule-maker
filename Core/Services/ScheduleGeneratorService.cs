﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.ReadModels;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис ренерации расписания
    /// </summary>
    public class ScheduleGeneratorService
    {
        private readonly LearningPlansService _learningPlansService;
        private readonly SchedulesService _scheduleService;
        private readonly LessonHoursService _lessonHoursService;
        private readonly LessonsService _lessonsService;
        private readonly RoomsService _roomsService;
        private readonly HolidayService _holidayService;

        public ScheduleGeneratorService(
            LearningPlansService learningPlansService,
            SchedulesService scheduleService,
            LessonHoursService lessonHoursService,
            LessonsService lessonsService,
            RoomsService roomsService,
            HolidayService holidayService)
        {
            _learningPlansService = learningPlansService;
            _scheduleService = scheduleService;
            _lessonHoursService = lessonHoursService;
            _lessonsService = lessonsService;
            _roomsService = roomsService;
            _holidayService = holidayService;
        }

        /// <summary>
        /// Метод инициализации процесса генерации расписания
        /// </summary>
        /// <param name="scheduleId">Идентификатор текущего расписания</param>
        /// <param name="isNewCopy">Новая копия</param>
        /// <param name="isUseExistingHours">Использовать существующие учебные часы</param>
        /// <param name="isUseRaiting">Использовать рейтинги групп и преподавателей</param>
        /// <param name="isStreamLections">Создавать лекции потоков</param>
        /// <param name="errorMessages">Список ошибок</param>
        /// <returns></returns>
        public bool GenerateSchedule(
            ref Guid scheduleId,
            bool isNewCopy,
            bool isUseExistingHours,
            bool isUseRaiting,
            bool isStreamLections,
            out string[] errorMessages)
        {
            var schedule = _scheduleService.GetById(scheduleId);
            var isDataValid = ValidateSchedule(schedule, isUseExistingHours, out var errors);
            if (!isDataValid)
            {
                errorMessages = errors.ToArray();
                return false;
            }

            var weekSchedule = _lessonHoursService.GetWeekDaysLessons(schedule);
            if (!isUseExistingHours)
            {
                ClearLessons(weekSchedule);
            }

            var isGenerationSuccess = PerformGeneration(
                scheduleId,
                isUseRaiting,
                isStreamLections,
                out errors,
                weekSchedule);
            if (isGenerationSuccess)
            {
                if (isNewCopy)
                {
                    schedule = _scheduleService.AddCopy(schedule);
                    Schedule.CurrentSchedule = schedule;
                    scheduleId = schedule.Id;
                }
                else if (!isUseExistingHours)
                {
                    _lessonsService.Delete(schedule);
                }

                AddNewLessons(schedule, weekSchedule);
            }

            errorMessages = errors.ToArray();
            return isGenerationSuccess;
        }

        /// <summary>
        /// Метод генерации расписания
        /// </summary>
        /// <param name="scheduleId">Идентификатор текущего расписания</param>
        /// <param name="isNewCopy">Новая копия</param>
        /// <param name="isUseExistingHours">Использовать существующие учебные часы</param>
        /// <param name="isUseRaiting">Использовать рейтинги групп и преподавателей</param>
        /// <param name="isStreamLections">Создавать лекции потоков</param>
        /// <param name="errorMessages">Список ошибок</param>
        /// <returns>Флаг успешного завершения генерации</returns>
        private bool PerformGeneration(
            Guid scheduleId,
            bool isUseRaiting,
            bool isStreamLections,
            out List<string> errors,
            Dictionary<DayOfWeek, IEnumerable<LessonHour>> resultScheduleLessons)
        {
            errors = new List<string>();
            var planItems = _learningPlansService.GetForScheduleId(scheduleId);
            if(isStreamLections)
            {
                ApplyStreamSize(planItems);
            }

            if (isUseRaiting)
            {
                ApplyRaitingOrder(planItems);
            }
            else
            {
                ApplyBaseOrder(planItems);
            }

            UpdatePlanItemsFact(planItems, resultScheduleLessons);
            ApplyLessons(planItems, resultScheduleLessons, errors, true, isStreamLections);
            ApplyLessons(planItems, resultScheduleLessons, errors, false);
            return !errors.Any();
        }

        /// <summary>
        /// Применение учебных занятий
        /// </summary>
        /// <param name="planItems">Запись учебного плана</param>
        /// <param name="resultScheduleLessons">Список созданных учебных занятий</param>
        /// <param name="errors">Список ошибок генерации</param>
        /// <param name="isLectionsMode">Режим лекций / практик</param>
        /// <param name="isStreamLections">Режим применения потоков</param>
        /// <returns>Флаг успешного завершения генерации</returns>
        private bool ApplyLessons(IEnumerable<LearningPlan> planItems,
            Dictionary<DayOfWeek, IEnumerable<LessonHour>> resultScheduleLessons,
            List<string> errors,
            bool isLectionsMode,
            bool isStreamLections = false)
        {
            foreach (var planItem in planItems.OrderByDescending(x => x.StreamSize))
            {
                var planItemResult = ApplyResult.Success;
                foreach (var day in resultScheduleLessons)
                {
                    var dayResult = isLectionsMode
                        ? ApplyLections(planItem, day, isStreamLections)
                        : ApplyPractices(planItem, day);
                    if (dayResult == ApplyResult.FullSuccess)
                    {
                        planItemResult = ApplyResult.FullSuccess;
                        break;
                    }

                    if (dayResult > planItemResult)
                    {
                        planItemResult = dayResult;
                    }
                }

                switch (planItemResult)
                {
                    case ApplyResult.Success:
                    case ApplyResult.NoFreeHours:
                        errors.Add("Недостаточно учебных часов");
                        return false;
                    case ApplyResult.NoFitRoom:
                        errors.Add("Не хватает аудиторий подходящего размера");
                        return false;
                    case ApplyResult.NoFreeRooms:
                        errors.Add("Не хватает аудиторий");
                        return false;
                    case ApplyResult.FullSuccess:
                        continue;
                }
            }

            return !errors.Any();
        }

        /// <summary>
        /// Обновление размеров потоков
        /// </summary>
        /// <param name="planItems">Записи учебного плана</param>
        private void ApplyStreamSize(IEnumerable<LearningPlan> planItems)
        {
            var streams = planItems.GroupBy(x => new { LectionTeachersList = x.LectionTeachersList.Trim(), x.Subject.Id });
            foreach (var stream in streams)
            {
                var streamSize = stream.Sum(x => x.Group.MembersCount);
                foreach (var planItem in stream)
                {
                    planItem.StreamSize = streamSize;
                }
            }
        }

        /// <summary>
        /// Создание практических занятий
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="day">Учебные часы дня недели</param>
        /// <returns>Результат генерации</returns>
        private ApplyResult ApplyPractices(LearningPlan planItem, KeyValuePair<DayOfWeek, IEnumerable<LessonHour>> day)
        {
            var freeLessonHours = GetFreePracticeHours(planItem, day.Value);
            var applyResult = ApplyResult.NoFreeHours;
            foreach (var lessonHour in freeLessonHours)
            {
                if (planItem.PracticeCount <= planItem.PracticeFactCount
                    && planItem.Group1PracticeFactCount == planItem.Group2PracticeFactCount)
                {
                    return ApplyResult.FullSuccess;
                }

                var freeRooms = GetFreePracticeRooms(planItem, lessonHour);
                if (!freeRooms.Any())
                {
                    applyResult = ApplyResult.NoFreeRooms;
                    continue;
                }

                var fitRoomWithWeekType = GetFitPracticeRoom(planItem, freeRooms, day.Key);
                if (fitRoomWithWeekType == null)
                {
                    applyResult = ApplyResult.NoFitRoom;
                    continue;
                }

                var newLesson = new Lesson
                {
                    Group = planItem.Group,
                    LessonHour = lessonHour,
                    Room = fitRoomWithWeekType.Room,
                    Teachers = planItem.PractTeachers,
                    Subject = planItem.Subject,
                    WeekType = fitRoomWithWeekType.WeekType,
                    SubGroup = fitRoomWithWeekType.SubGroup,
                    LessonType = LessonType.Practice
                };
                lessonHour.Lessons.Add(newLesson);
                applyResult = planItem.PracticeCount <= planItem.PracticeFactCount
                    && planItem.Group1PracticeFactCount == planItem.Group2PracticeFactCount
                    ? ApplyResult.FullSuccess : ApplyResult.Success;
                if (fitRoomWithWeekType.SubGroup == SubGroup.Group1)
                {
                    applyResult =  ApplyPractices(planItem, day);
                }
            }

            return applyResult;
        }

        /// <summary>
        /// Поиск подходящих по размеру аудиторий практики
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="freeRooms">Список доступных аудиторий</param>
        /// <param name="day">День недели</param>
        /// <returns>Список доступных аудиторий с информацией о типах подгруппы и недели </returns>
        private OccupiedInfo GetFitPracticeRoom(LearningPlan planItem, IEnumerable<OccupiedInfo> freeRooms, DayOfWeek day)
        {
            OccupiedInfo fitRoom = null;
            LearningPlan currentPlan = null;
            var subGroup = SubGroup.Separateless;
            var fitRoomWithWeekTypes = new List<OccupiedInfo>();
            var firstRoom = freeRooms.OrderBy(x => x.Room.SeatsCount)
                .FirstOrDefault(x => x.Room.SeatsCount >= planItem.Group.MembersCount);
            fitRoomWithWeekTypes = freeRooms.Where(x => x.Room.Id == firstRoom?.Room?.Id).ToList();
            if (!fitRoomWithWeekTypes.Any())
            {
                fitRoomWithWeekTypes = freeRooms
                    .OrderBy(x => x.Room.SeatsCount)
                    .Where(x => x.Room.SeatsCount >= planItem.Group.MembersCount / 2.0)
                    .ToList();
                subGroup = planItem.Group1PracticeFactCount > planItem.Group2PracticeFactCount
                            ? SubGroup.Group2 : SubGroup.Group1;
            }

            if (fitRoomWithWeekTypes.Any())
            {
                double minPosDiff = double.MaxValue;
                var upLineRoom = fitRoomWithWeekTypes.FirstOrDefault(x => x.WeekType == WeekType.UpLine);
                if (upLineRoom != null)
                {
                    var uplinePlanCase = planItem.Clone();
                    UpdatePlanItemFact(uplinePlanCase, planItem.Subject, day,
                        LessonType.Practice, subGroup, WeekType.UpLine);
                    var uplinePlanDif = planItem.PracticeCount - uplinePlanCase.PracticeFactCount;
                    if (Math.Abs(uplinePlanDif) < minPosDiff)
                    {
                        minPosDiff = uplinePlanDif;
                        fitRoom = upLineRoom;
                        currentPlan = uplinePlanCase;
                    }
                }

                var underlineLineRoom = fitRoomWithWeekTypes.FirstOrDefault(x => x.WeekType == WeekType.UnderLine);
                if (underlineLineRoom != null)
                {
                    var underlinePlanCase = planItem.Clone();
                    UpdatePlanItemFact(underlinePlanCase, planItem.Subject, day,
                        LessonType.Practice, subGroup, WeekType.UnderLine);
                    var underlinePlanDif = planItem.PracticeCount - underlinePlanCase.PracticeFactCount;
                    if (Math.Abs(underlinePlanDif) < minPosDiff)
                    {
                        minPosDiff = underlinePlanDif;
                        fitRoom = underlineLineRoom;
                        currentPlan = underlinePlanCase;
                    }
                }

                var simpleRoom = fitRoomWithWeekTypes.FirstOrDefault(x => x.WeekType == WeekType.Simple);
                if (simpleRoom != null)
                {
                    var simpleRoomCase = planItem.Clone();
                    UpdatePlanItemFact(simpleRoomCase, planItem.Subject, day,
                        LessonType.Practice, subGroup, WeekType.Simple);
                    var simplePlanDif = planItem.PracticeCount - simpleRoomCase.PracticeFactCount;
                    if (Math.Abs(simplePlanDif) < minPosDiff)
                    {
                        fitRoom = simpleRoom;
                        currentPlan = simpleRoomCase;
                    }
                }

                if (fitRoom != null)
                {
                    fitRoom.Room.Occupied += planItem.Group.MembersCount;
                    fitRoom.SubGroup = subGroup;
                    currentPlan.CloneTo(planItem);
                }
            }

            return fitRoom;
        }

        /// <summary>
        /// Поиск доступных аудиторий для практики
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="lessonHour">Учебный час</param>
        /// <returns>Список доступных аудиторий с указанием типов подгруппы и недели</returns>
        private IEnumerable<OccupiedInfo> GetFreePracticeRooms(LearningPlan planItem, LessonHour lessonHour)
        {
            var occupiedRooms = lessonHour.Lessons.Select(x => new OccupiedInfo(x.Room, x.WeekType)).ToList();
            var rooms = _roomsService.GetAll().ToList();
            if (planItem.PractRooms.Any())
            {
                rooms = rooms.Where(x => planItem.PractRooms.Any(y => y.Id == x.Id)).ToList();
            }

            var freeRooms = new List<OccupiedInfo>();
            foreach (var room in rooms)
            {
                if (!occupiedRooms.Any(x => x.Room.Id == room.Id && x.WeekType == WeekType.Simple))
                {
                    if (!occupiedRooms.Any(x => x.Room.Id == room.Id && x.WeekType == WeekType.UpLine))
                    {
                        freeRooms.Add(new OccupiedInfo(room, WeekType.UpLine));
                    }

                    if (!occupiedRooms.Any(x => x.Room.Id == room.Id && x.WeekType == WeekType.UnderLine))
                    {
                        freeRooms.Add(new OccupiedInfo(room, WeekType.UnderLine));
                    }

                    if (!occupiedRooms.Any(x => x.Room.Id == room.Id &&
                        (x.WeekType == WeekType.UpLine || x.WeekType == WeekType.UnderLine)))
                    {
                        freeRooms.Add(new OccupiedInfo(room, WeekType.Simple));
                    }
                }
            }

            return freeRooms;
        }

        /// <summary>
        /// Поиск учебных часов доступных и для группы и для преподавателя
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="lessonHours">Список учебных часов дня</param>
        /// <returns>Список доступных учебных часов</returns>
        private List<LessonHour> GetFreePracticeHours(LearningPlan planItem, IEnumerable<LessonHour> lessonHours)
        {
            var freePracticeHours = new List<LessonHour>();
            var lessonLoursFreeForGroup = lessonHours
                .Where(lh => lh.Lessons.All(l => l.Group.Id != planItem.Group.Id))
                .ToList();
            var teachersIds = planItem.PractTeachers.Select(x => x.Id).ToArray();
            foreach (var lessonHour in lessonLoursFreeForGroup)
            {
                var teachersLessons = lessonHour.Lessons.Where(lh => lh.Teachers.Any(t => teachersIds.Contains(t.Id)));
                if (!teachersLessons.Any())
                {
                    freePracticeHours.Add(lessonHour);
                }
            }

            return freePracticeHours;
        }

        /// <summary>
        /// Метод создания лекционных занятий
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="day">Учебные часы дня недели</param>
        /// <param name="isStreamLections">Режим создания потоковых лекций</param>
        /// <returns>Результат генерации</returns>
        private ApplyResult ApplyLections(
            LearningPlan planItem,
            KeyValuePair<DayOfWeek,
            IEnumerable<LessonHour>> day,
            bool isStreamLections)
        {
            var freeLessonHours = GetFreeLectionHours(planItem, day.Value, isStreamLections);
            var applyResult = ApplyResult.NoFreeHours;
            foreach (var lessonHour in  freeLessonHours)
            {
                if (planItem.LectionsCount <= planItem.LectionsFactCount)
                {
                    return ApplyResult.FullSuccess;
                }

                var freeRooms = GetFreeLectionRooms(planItem, lessonHour, isStreamLections);
                if (!freeRooms.Any())
                {
                    applyResult = ApplyResult.NoFreeRooms;
                    continue;
                }

                var fitRoomWithWeekType = GetFitLectionRoom(planItem, freeRooms, day.Key);
                if (fitRoomWithWeekType == null)
                {
                    applyResult = ApplyResult.NoFitRoom;
                    continue;
                }

                var newLesson = new Lesson
                {
                    Group = planItem.Group,
                    LessonHour = lessonHour,
                    Room = fitRoomWithWeekType.Room,
                    Teachers = planItem.LectionTeachers,
                    Subject = planItem.Subject,
                    WeekType = fitRoomWithWeekType.WeekType,
                    SubGroup = SubGroup.Separateless,
                    LessonType = LessonType.Lection
                };
                lessonHour.Lessons.Add(newLesson);
                applyResult = planItem.LectionsCount <= planItem.LectionsFactCount
                    ? ApplyResult.FullSuccess : ApplyResult.Success;
            }

            return applyResult;
        }

        /// <summary>
        /// Поиск подходящих по размеру лекционных аудиторий
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="freeRooms">Доступные аудитории</param>
        /// <param name="day">День недели</param>
        /// <returns>Найденный день</returns>
        private OccupiedInfo GetFitLectionRoom(LearningPlan planItem, IEnumerable<OccupiedInfo> freeRooms, DayOfWeek day)
        {
            OccupiedInfo fitRoom = null;
            LearningPlan currentPlan = null;
            var fitRoomWithWeekTypes = new List<OccupiedInfo>();
            if (planItem.StreamSize > 0)
            {
                var firstRoom = freeRooms.OrderBy(x => x.Room.SeatsCount)
                    .FirstOrDefault(x => x.Room.SeatsCount >= planItem.StreamSize);
                fitRoomWithWeekTypes = freeRooms.Where(x => x.Room.Id == firstRoom?.Room?.Id).ToList();
            }
            if (!fitRoomWithWeekTypes.Any())
            {
                var firstRoom = freeRooms.OrderBy(x => x.Room.SeatsCount)
                    .FirstOrDefault(x => x.Room.SeatsCount >= planItem.Group.MembersCount);
                fitRoomWithWeekTypes = freeRooms.Where(x => x.Room.Id == firstRoom?.Room?.Id).ToList();
            }

            if (fitRoomWithWeekTypes.Any())
            {
                double minPosDiff = planItem.LectionsCount;
                var upLineRoom = fitRoomWithWeekTypes.FirstOrDefault(x => x.WeekType == WeekType.UpLine);
                if (upLineRoom != null)
                {
                    var uplinePlanCase = planItem.Clone();
                    UpdatePlanItemFact(uplinePlanCase, planItem.Subject, day,
                        LessonType.Lection, SubGroup.Separateless, WeekType.UpLine);
                    var uplinePlanDif = planItem.LectionsCount - uplinePlanCase.LectionsFactCount;
                    if (Math.Abs(uplinePlanDif) <= minPosDiff)
                    {
                        minPosDiff = uplinePlanDif;
                        fitRoom = upLineRoom;
                        currentPlan = uplinePlanCase;
                    }
                }

                var underlineLineRoom = fitRoomWithWeekTypes.FirstOrDefault(x => x.WeekType == WeekType.UnderLine);
                if (underlineLineRoom != null)
                {
                    var underlinePlanCase = planItem.Clone();
                    UpdatePlanItemFact(underlinePlanCase, planItem.Subject, day,
                        LessonType.Lection, SubGroup.Separateless, WeekType.UnderLine);
                    var underlinePlanDif = planItem.LectionsCount - underlinePlanCase.LectionsFactCount;
                    if (Math.Abs(underlinePlanDif) < minPosDiff)
                    {
                        minPosDiff = underlinePlanDif;
                        fitRoom = underlineLineRoom;
                        currentPlan = underlinePlanCase;
                    }
                }

                var simpleRoom = fitRoomWithWeekTypes.FirstOrDefault(x => x.WeekType == WeekType.Simple);
                if (simpleRoom != null)
                {
                    var simpleRoomCase = planItem.Clone();
                    UpdatePlanItemFact(simpleRoomCase, planItem.Subject, day,
                        LessonType.Lection, SubGroup.Separateless, WeekType.Simple);
                    var simplePlanDif = planItem.LectionsCount - simpleRoomCase.LectionsFactCount;
                    if (Math.Abs(simplePlanDif) <= minPosDiff)
                    {
                        fitRoom = simpleRoom;
                        currentPlan = simpleRoomCase;
                    }
                }

                if (fitRoom != null)
                {
                    fitRoom.Room.Occupied += planItem.Group.MembersCount;
                    currentPlan.CloneTo(planItem);
                }
            }

            return fitRoom;
        }

        /// <summary>
        /// Поиск доступных лекционных аудиторий
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="lessonHour">Учебный час</param>
        /// <param name="isStreamLections">Режим потоковых лекций</param>
        /// <returns>Список доступных аудиторий</returns>
        private IEnumerable<OccupiedInfo> GetFreeLectionRooms(
            LearningPlan planItem,
            LessonHour lessonHour,
            bool isStreamLections)
        {
            var occupiedRooms = lessonHour.Lessons
                    .Select(x => new OccupiedInfo(x.Room, x.WeekType))
                    .ToList();
            var rooms = _roomsService.GetAll();
            var freeRooms = new List<OccupiedInfo>();
            if (isStreamLections)
            {

                var streamRooms = lessonHour.Lessons
                .Where(x => x.Subject.Id == planItem.Subject.Id
                    && planItem.StreamSize <= x.Room.SeatsCount
                    && x.TeachersList.Trim() == planItem.LectionTeachersList.Trim()
                    && x.LessonType == LessonType.Lection)
                .Select(x => new OccupiedInfo(x.Room, x.WeekType))
                .ToList();
                freeRooms.AddRange(streamRooms);
            }

            foreach (var room in rooms)
            {
                if (!occupiedRooms.Any(x => x.Room.Id == room.Id && x.WeekType == WeekType.Simple))
                {
                    if (!occupiedRooms.Any(x => x.Room.Id == room.Id && x.WeekType == WeekType.UpLine))
                    {
                        freeRooms.Add(new OccupiedInfo(room, WeekType.UpLine));
                    }

                    if (!occupiedRooms.Any(x => x.Room.Id == room.Id && x.WeekType == WeekType.UnderLine))
                    {
                        freeRooms.Add(new OccupiedInfo(room, WeekType.UnderLine));
                    }

                    if (!occupiedRooms.Any(x => x.Room.Id == room.Id &&
                        (x.WeekType == WeekType.UpLine || x.WeekType == WeekType.UnderLine)))
                    {
                        freeRooms.Add(new OccupiedInfo(room, WeekType.Simple));
                    }
                }
            }

            if (isStreamLections)
            {
                foreach (var roomInfo in freeRooms)
                {
                    roomInfo.Room.Occupied = lessonHour.Lessons
                        .Where(x => x.Room.Id == roomInfo.Room.Id)
                        .Sum(x => x.SubGroup == SubGroup.Separateless
                            ? x.Group.MembersCount : x.Group.MembersCount / 2.0);
                }
            }

            return freeRooms;
        }

        /// <summary>
        /// Поиск учебных часов, свободных как для групп, так и для преподавателя
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="lessonHours">Список учебных часов дня недели</param>
        /// <param name="isStreamLections">Режим потоковых лекций</param>
        /// <returns>Список доступных учебных часов</returns>
        private IEnumerable<LessonHour> GetFreeLectionHours(LearningPlan planItem, IEnumerable<LessonHour> lessonHours,
            bool isStreamLections)
        {
            var freeLectionHours = new List<LessonHour>();
            var lessonLoursFreeForGroup = lessonHours
                .Where(lh => !lh.Lessons.Any(l => l.Group.Id == planItem.Group.Id))
                .ToList();
            var teachersIds = planItem.LectionTeachers.Select(x => x.Id).ToArray();
            foreach (var lessonHour in lessonLoursFreeForGroup)
            {
                var teachersLessons = lessonHour.Lessons.Where(lh =>
                    lh.Teachers.Any(t => teachersIds.Contains(t.Id)));
                if (isStreamLections)
                {
                    if (!teachersLessons.Any(x =>
                        x.Subject.Id != planItem.Subject.Id || x.LessonType != LessonType.Lection))
                    {
                        freeLectionHours.Add(lessonHour);
                    }
                }
                else
                {
                    if (!teachersLessons.Any())
                    {
                        freeLectionHours.Add(lessonHour);
                    }
                }
            }

            return freeLectionHours;
        }

        /// <summary>
        /// Обновление фактических счетчиков учебного плана
        /// </summary>
        /// <param name="planItems">Записи учебного плана</param>
        /// <param name="resultScheduleLessons">Список назначенных учебных занятий</param>
        private void UpdatePlanItemsFact(
            IEnumerable<LearningPlan> planItems,
            Dictionary<DayOfWeek, IEnumerable<LessonHour>> resultScheduleLessons)
        {
            var factLessons = resultScheduleLessons
                .SelectMany(x => x.Value.SelectMany(y => y.Lessons))
                .ToList();
            foreach (var planItem in planItems)
            {
                planItem.LectionsFactCount = 0;
                planItem.SeparatlesPracticeFactCount = 0;
                planItem.UnspecifiedFactCount = 0;
                planItem.Group1PracticeFactCount = 0;
                planItem.Group2PracticeFactCount = 0;

            }

            foreach (var factLesson in factLessons)
            {
                var planItem = planItems.FirstOrDefault(x =>
                    x.Group.Id == factLesson.Group.Id
                 && x.Subject.Id == factLesson.Subject.Id);
                if (planItem == null)
                {
                    continue;
                }

                UpdatePlanItemFact(planItem, factLesson.Subject, factLesson.LessonHour.Day,
                    factLesson.LessonType, factLesson.SubGroup, factLesson.WeekType);
            }
        }

        /// <summary>
        /// Обновление фактических счетчиков учебного плана
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="subject">Предмет</param>
        /// <param name="weekDay">День недели</param>
        /// <param name="lessonType">Тип занятия (лекция / практика)</param>
        /// <param name="subGroup">Тип подгруппы (1ая, 2ая, без разделения)</param>
        /// <param name="weekType">Тип недели</param>
        private void UpdatePlanItemFact(
            LearningPlan planItem,
            Subject subject,
            DayOfWeek weekDay,
            LessonType lessonType,
            SubGroup subGroup,
            WeekType weekType)
        {
            if (planItem.Schedule.FirstDay > planItem.Schedule.LastDay)
            {
                return;
            }

            var day = planItem.Schedule.FirstDay;
            var holidaysDates = _holidayService.GetAll().Select(x => x.Date).ToList();
            var scheduleDays = _lessonHoursService.GetWeekDaysLessons(planItem.Schedule, planItem.Group);
            var weekStart = day.DayOfWeek;
            var currentWeekType = WeekType.UpLine;
            while (day <= planItem.Schedule.LastDay)
            {
                if (day.DayOfWeek == weekDay
                    && !holidaysDates.Any(x => x.Date.Month == day.Month && x.Date.Day == day.Day))
                {
                    AddFactHours(planItem, subject, weekType, currentWeekType, lessonType, subGroup);
                }

                day = day.AddDays(1);
                if (day.DayOfWeek == weekStart)
                {
                    currentWeekType = currentWeekType == WeekType.UpLine
                        ? WeekType.UnderLine
                        : WeekType.UpLine;
                }
            }
        }
        /// <summary>
        /// Логика увеличения счетчиков фактических учебных занятий
        /// </summary>
        /// <param name="planItem">Запись учебного плана</param>
        /// <param name="subject">Предмет</param>
        /// <param name="weekType">Тип недели</param>
        /// <param name="currentWeekType">Тип текущей недели</param>
        /// <param name="lessonType">Тип занятий</param>
        /// <param name="subGroup">Тип подгруппы</param>
        private void AddFactHours(LearningPlan planItem, Subject subject, WeekType weekType,
            WeekType currentWeekType, LessonType lessonType, SubGroup subGroup)
        {
            if (subject.Id == planItem.Subject.Id &&
                                (weekType == currentWeekType || weekType == WeekType.Simple))
            {
                planItem.LectionsFactCount += GetPlannedLessonsCount(lessonType, subGroup,
                    LessonType.Lection);
                planItem.UnspecifiedFactCount += GetPlannedLessonsCount(lessonType, subGroup,
                    LessonType.Unspecified);
                if (lessonType == LessonType.Practice)
                {
                    if (subGroup == SubGroup.Group1)
                    {
                        planItem.Group1PracticeFactCount += GetPlannedLessonsCount(
                            lessonType, SubGroup.Group1, LessonType.Practice);
                    }

                    if (subGroup == SubGroup.Group2)
                    {
                        planItem.Group2PracticeFactCount += GetPlannedLessonsCount(
                            lessonType, SubGroup.Group2, LessonType.Practice);
                    }

                    if (subGroup == SubGroup.Separateless)
                    {
                        planItem.SeparatlesPracticeFactCount += GetPlannedLessonsCount(
                            lessonType, SubGroup.Separateless, LessonType.Practice);
                    }
                }
            }
        }

        /// <summary>
        /// Логика определения числа занятий на основе типов недели и подгруппы
        /// </summary>
        /// <param name="lessonType">Тип недели</param>
        /// <param name="groupType">Тип подгруппы</param>
        /// <returns>Число добавленных часов</returns>
        private double GetPlannedLessonsCount(LessonType lessonType, SubGroup groupType, LessonType targetType)
        {
            var result = 0.0;
            if (lessonType == targetType)
            {
                if (lessonType == LessonType.Practice)
                {
                    result = 1;
                }
                else
                {
                    result = groupType == SubGroup.Separateless ? 1 : 0.5;
                }
            }

            return result;
        }

        /// <summary>
        /// Применения порядка рейтингов
        /// </summary>
        /// <param name="planItems">Записи учебного плана</param>
        private void ApplyRaitingOrder(IEnumerable<LearningPlan> planItems)
        {
            planItems = planItems
                    .OrderBy(x => x.StreamSize)
                    .ThenBy(x => x.LectionTeachers.Sum(y => y.Raiting))
                    .ThenBy(x => x.PractTeachers.Sum(y => y.Raiting))
                    .ThenBy(x => x.LectionTeachers.Select(y => y.Id))
                    .ThenBy(x => x.PractTeachers.Select(y => y.Id))
                    .ThenBy(x => x.Group.Raiting)
                    .ThenBy(x => x.Group.Id);
        }

        /// <summary>
        /// Применение порядка записей учебного плана без учета рейтинга
        /// </summary>
        /// <param name="planItems">Записи учебного плана</param>
        private void ApplyBaseOrder(IEnumerable<LearningPlan> planItems)
        {
            planItems = planItems
                    .OrderBy(x => x.StreamSize)
                    .ThenBy(x => x.LectionTeachers.Select(y => y.Id))
                    .ThenBy(x => x.PractTeachers.Select(y => y.Id))
                    .ThenBy(x => x.Group.Id);
        }

        /// <summary>
        /// Добавление новых учебных занятий к существующему расписанию
        /// </summary>
        /// <param name="schedule">Расписание</param>
        /// <param name="weekSchedule">Список новых учебных занятий</param>
        private void AddNewLessons(
            Schedule schedule,
            Dictionary<DayOfWeek, IEnumerable<LessonHour>> weekSchedule)
        {
            var resultScheduleLessons = weekSchedule.SelectMany(x => x.Value.SelectMany(y => y.Lessons)).ToList();
            foreach (var lesson in resultScheduleLessons)
            {
                if (lesson.Id != Guid.Empty && lesson.LessonHour.Schedule.Id == schedule.Id)
                {
                    continue;
                }

                var newLesson = lesson.Id != Guid.Empty ? lesson.Clone() : lesson;
                var lessonHour = schedule.LessonHours.First(x =>
                    x.Day == lesson.LessonHour.Day &&
                    x.StartTime == lesson.LessonHour.StartTime);
                newLesson.LessonHour = lessonHour;
                newLesson.Id = Guid.NewGuid();
                _lessonsService.Add(newLesson);
            }
        }

        /// <summary>
        /// Очитска расписания от существующих учебных часов
        /// </summary>
        /// <param name="weekSchedule"></param>
        private void ClearLessons(Dictionary<DayOfWeek, IEnumerable<LessonHour>> weekSchedule)
        {
            foreach (var learingHours in weekSchedule.SelectMany(x => x.Value))
            {
                learingHours.Lessons.Clear();
            }
        }

        /// <summary>
        /// Проверка корректности расписания и учебного плана
        /// </summary>
        /// <param name="schedule">Расписание</param>
        /// <param name="isUseExistingHours">Флаг использования существующих учебных занятий</param>
        /// <param name="errors">Список ошибок</param>
        /// <returns></returns>
        private bool ValidateSchedule(Schedule schedule, bool isUseExistingHours, out List<string> errors)
        {
            string errorMessage;
            var errorsList = new List<string>();
            var plannedLessonsCounts = _learningPlansService.GetPlannedLessonsCounts(schedule);
            var unassignedLessonsCounts = isUseExistingHours
                ? _learningPlansService.GetUnassignedLessonsCounts(schedule)
                : plannedLessonsCounts;
            if (unassignedLessonsCounts.Count == 0)
            {
                errorMessage = "Учебный план расписания должен содержать как минимум одну запись c неназначенным занятием";
                errorsList.Add(errorMessage);
            }

            var scheduleSize = _lessonHoursService.GetScheduleSize(schedule);
            var exceedScheduleSize = plannedLessonsCounts.Where(x => scheduleSize < x.Value).ToList();
            if (exceedScheduleSize.Any())
            {
                var groupsDescription = GetGroupsDescription(exceedScheduleSize);
                errorMessage =
                    $"Учебный план следующих групп превысил размер расписания ({scheduleSize} доступно):\n"
                    + $"{groupsDescription}";

                errorsList.Add(errorMessage);
            }

            errors = errorsList;
            return !errors.Any();
        }

        /// <summary>
        /// Получение списка групп с информацией о числе записей учебного плана
        /// </summary>
        /// <param name="exceedScheduleSize">Размер расписания</param>
        /// <returns>Строка со списком групп, учебный план которых превышает размер расписания</returns>
        private string GetGroupsDescription(List<KeyValuePair<Group, double>> exceedScheduleSize)
        {
            var result = new StringBuilder();
            var lastGroupRecord = exceedScheduleSize.LastOrDefault();
            foreach (var groupRecord in exceedScheduleSize)
            {
                result.Append($"{groupRecord.Key.FullName} ({groupRecord.Value} запланировано)");
                if (groupRecord.Key != lastGroupRecord.Key)
                {
                    result.Append(", ");
                }
            }

            return result.ToString();
        }
    }
}
