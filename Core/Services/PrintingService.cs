﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Novacode;
using ScheduleMaker.Logic.Helpers;
using ScheduleMaker.Logic.Model;


namespace ScheduleMaker.Logic.Services
{
    public class PrintingService
    {
        private readonly GroupsService _groupsService;
        private readonly SchedulesService _schedulesService;
        private readonly LessonsService _lessonsService;

        public PrintingService(GroupsService groupsService, SchedulesService schedulesService,
            LessonsService lessonsService)
        {
            _groupsService = groupsService;
            _schedulesService = schedulesService;
            _lessonsService = lessonsService;
        }

        public void GenereteCommonSchedule(string templatePath, string resultDir, Schedule schedule)
        {
            if (schedule == null) return;
            CommonScheduleGenerator generator = new CommonScheduleGenerator(templatePath, _lessonsService);
            if (!generator.IsReady) return;
            var fullPath = resultDir;
            if (fullPath == null)
            {
                MessageBox.Show(string.Format("Директория для сохранения файла не найдена\n\"{0}\"", resultDir),
                    "Не верный путь", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var resultFile = schedule.Name;
            foreach (var invalidFileNameChar in Path.GetInvalidFileNameChars())
            {
                resultFile = resultFile.Replace(invalidFileNameChar, '_');
            }

            resultFile = fullPath + resultFile + "_Общее расписание.docx";
            using (DocX document = DocX.Load(templatePath))
            {
                generator.CleanDocument(document);
                var weekDays = _schedulesService.GetWeekDays(schedule).ToList();
                var groups = _groupsService.GetAvailableInSchedule(schedule)
                    .OrderBy(x => x.CurrentCourse).ThenBy(x => x.Name).ToList();
                var firstGroup = groups.FirstOrDefault();
                var lastGroup = groups.LastOrDefault();
                foreach (var group in groups)
                {
                    if (group == firstGroup)
                    {
                        generator.AddLSchedule(document, group, weekDays);
                    }
                    else if (group == lastGroup)
                    {
                        generator.AddRSchedule(document, group, weekDays);
                    }
                    else
                    {
                        generator.AddMSchedule(document, group, weekDays);
                    }

                    if (group != lastGroup)
                    {
                        document.InsertSectionPageBreak();
                    }
                }

                generator.SetEqualsRowsHeight();
                try
                {
                    document.SaveAs(resultFile);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(),
                    "Ошибка записи", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            WordHelper.OpenDoc(resultFile);
        }

        public void GenereteAuditorySchedule(string resultDir, Schedule schedule, Room room)
        {
            if (schedule == null || room == null) return;
            AuditoryScheduleGenerator generator = new AuditoryScheduleGenerator(_lessonsService);
            if (!generator.IsReady) return;
            var fullPath = resultDir;
            if (fullPath == null)
            {
                MessageBox.Show(string.Format("Директория для сохранения файла не найдена\n\"{0}\"", resultDir),
                    "Не верный путь", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var resultFile = "Расписание для аудитории "+ room.Name +" (" + schedule.Name + ")";
            foreach (var invalidFileNameChar in Path.GetInvalidFileNameChars())
            {
                resultFile = resultFile.Replace(invalidFileNameChar, '_');
            }

            resultFile = fullPath + resultFile + "_.docx";
            using (DocX document = DocX.Create(resultFile))
            {
                var weekDays = _schedulesService.GetWeekDays(schedule).ToList();
                var lessonHours = new List<LessonHour>();
                foreach (var weekDay in weekDays)
                {
                    foreach (var lessonHour in weekDay.LessonHours)
                    {
                        if (lessonHours.All(x => x.StartTime != lessonHour.StartTime))
                        {
                            lessonHours.Add(lessonHour);
                        }
                    }
                }

                lessonHours = lessonHours.OrderBy(x => x.StartTime).ToList();
                generator.Generate(document, room, schedule, weekDays, lessonHours);
                try
                {
                    document.SaveAs(resultFile);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(),
                    "Ошибка записи", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            WordHelper.OpenDoc(resultFile);
        }
    }
}
