﻿using System.Linq;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис "Аудитории"
    /// </summary>
    public class RoomsService : BaseReferenceService<Room>
    {
        public RoomsService(DataContext dataContext)
            : base(dataContext)
        {         
        }

        public override IQueryable<Room> GetAll()
        {
            return base.GetAll().OrderBy(x=>x.Name);
        }
    }
}
