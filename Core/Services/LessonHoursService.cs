﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Properties;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис "Учебные часы"
    /// </summary>
    public class LessonHoursService : BaseReferenceService<LessonHour>
    {
        public LessonHoursService(DataContext dataContext)
            : base(dataContext)
        {
        }

        /// <summary>
        /// Смещение учебных часов расписания на заданный промежуток времени
        /// </summary>
        /// <param name="oldSchedule">Расписание с исходными учебными часами</param>
        /// <param name="def">Значение смещения</param>
        public void ShiftLessons(Schedule oldSchedule, TimeSpan def)
        {
            var lessonHoursForSchedule = GetAll().Where(x => x.Schedule.Id == oldSchedule.Id).GroupBy(x => x.Day);

            foreach (var lessonHoursForDay in lessonHoursForSchedule)
            {
                LessonHour prewLessonHour = null;
                foreach (var lessonHour in lessonHoursForDay.OrderBy(x => x.StartTime))
                {
                    if (prewLessonHour == null)
                    {
                        if (lessonHour.StartTime.TimeOfDay == oldSchedule.LessonsStart.TimeOfDay)
                        {
                            lessonHour.StartTime = lessonHour.Schedule.LessonsStart;
                        }

                        prewLessonHour = lessonHour;
                        continue;
                    }

                    if (lessonHour.StartTime.TimeOfDay + def <=
                        prewLessonHour.StartTime.TimeOfDay + lessonHour.Schedule.LessonDuration)
                    {
                        lessonHour.StartTime = prewLessonHour.StartTime + lessonHour.Schedule.LessonDuration
                            + Settings.Default.LessonBreak;
                    }
                    else
                    {
                        lessonHour.StartTime += def;
                    }

                    prewLessonHour = lessonHour;
                }
            }

            DataContext.SaveChanges();
        }

        /// <summary>
        /// Копирование учебных часов в пределах недели, между днями
        /// </summary>
        /// <param name="schedule">Текущее расписание</param>
        /// <param name="destDay">День-приемник часов</param>
        /// <param name="sourceDay">День-источник часов</param>
        public void CopyHours(Schedule schedule, DayOfWeek destDay, DayOfWeek sourceDay)
        {
            var sourceHours = schedule.LessonHours.Where(x => x.Day == sourceDay).ToList();
            foreach (var sourceHour in sourceHours)
            {
                var newHour = new LessonHour(sourceHour)
                {
                    Day = destDay,
                    Lessons =  new List<Lesson>()
                };
                Add(newHour);
            }
        }

        /// <summary>
        /// Получение учебных занятий для расписания и, опционально, группы, 
        /// сгруппированных по дням недели и учебным часам
        /// </summary>
        /// <param name="schedule">Расписание, для которого делается выборка</param>
        /// <param name="group">Группа, для которой делается выборка</param>
        /// <returns></returns>
        public Dictionary<DayOfWeek, IEnumerable<LessonHour>> GetWeekDaysLessons(
            Schedule schedule, Group group = null)
        {
            var result = new Dictionary<DayOfWeek, IEnumerable<LessonHour>>();
            if (schedule != null)
            {
                foreach (DayOfWeek weekDay in Enum.GetValues(typeof(DayOfWeek)))
                {
                    var dayLessons = DataContext.LessonHours
                        .Where(x => x.Schedule.Id == schedule.Id && x.Day == weekDay)
                        .OrderBy(x => x.StartTime)
                        .ToList()
                        .Select(x=>x.Clone())
                        .ToList();
                    foreach (var lessonHour in dayLessons)
                    {
                        var lessons = new List<Lesson>();
                        foreach (var lesson in lessonHour.Lessons)
                        {
                            var query = DataContext.Lessons
                                .Include(x => x.Subject).Include(x => x.Teachers).Include(x => x.Room)
                                .Where(l => l.Id == lesson.Id);
                            if (group != null)
                            {
                                query = query.Where(l => l.Group.Id == group.Id);
                            }

                            lessons.AddRange(query);
                        }

                        lessonHour.Lessons = lessons;
                    }

                    result[weekDay] = dayLessons;
                }

                // удаление первых пустых дней
                var days = result.Keys.ToList();
                foreach (var weekDay in days)
                {
                    if (!result[weekDay].Any())
                    {
                        result.Remove(weekDay);
                    }
                    else
                    {
                       break; 
                    }
                }

                // удаление последних пустых дней
                days = result.Keys.Reverse().ToList();
                foreach (var weekDay in days)
                {
                    if (!result[weekDay].Any())
                    {
                        result.Remove(weekDay);
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return result.OrderBy(x=>x.Key == DayOfWeek.Sunday ? (DayOfWeek)7 : x.Key)
                .ToDictionary(x => x.Key, x => x.Value); 
        }

        public double GetScheduleSize(Schedule schedule)
        {
            var scheduleSize = 0;
            if (schedule == null || schedule.FirstDay > schedule.LastDay)
            {
                return scheduleSize;
            }

            var day = schedule.FirstDay;
            var holidaysDates = DataContext.Holidays.Select(x => x.Date).ToList();
            var scheduleDays = GetWeekDaysLessons(schedule);
            while (day <= schedule.LastDay)
            {
                if (!holidaysDates.Any(x => x.Date.Month == day.Month && x.Date.Day == day.Day))
                {
                    var dayHours = scheduleDays.ContainsKey(day.DayOfWeek)
                        ? scheduleDays[day.DayOfWeek]
                        : new List<LessonHour>();
                    foreach (var unused in dayHours)
                    {
                        scheduleSize++;
                    }
                }

                day = day.AddDays(1);
            }

            return scheduleSize;
        }
    }
}
