﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис "Занятия"
    /// </summary>
    public class LessonsService : BaseReferenceService<Lesson>
    {
        public LessonsService(DataContext dataContext)
            : base(dataContext)
        {
        }

        public override Lesson GetById(Guid id)
        {
            return DataContext.Set<Lesson>().Where(rec => rec.Id == id).
                Include(x => x.Subject).Include(y => y.Room).Include(x => x.Group).FirstOrDefault();
        }

        public IEnumerable<Lesson> GetLessonsForHour(LessonHour currentLessonHour)
        {
            return DataContext.Lessons.Where(x => x.LessonHour.Id == currentLessonHour.Id);
        }

        /// <summary>
        /// Проверяет, есть ли на это время другое занятие (под чертой / над чертой)
        /// </summary>
        public bool ExistsOpposite(Lesson lesson)
        {
            if (lesson.LessonHour != null)
            {
                var existsLessons = GetGetLessonsWithPreconditions(lesson, lesson.SubGroup, lesson.WeekType).ToList();
                return existsLessons.Any();
            }

            return false;
        }

        /// <summary>
        /// Возвращает другие занятия этой группы, назначенные на это же время
        /// </summary>
        public IEnumerable<Lesson> GetGetLessonsWithPreconditions(Lesson lesson, SubGroup subGroup, WeekType weekType)
        {
            return GetAll().Where(x =>
                x.LessonHour.Id == lesson.LessonHour.Id && x.Id != lesson.Id &&
                x.Group.Id == lesson.Group.Id &&
                (
                    x.WeekType == weekType && x.SubGroup == subGroup || x.WeekType == WeekType.Simple &&
                    (
                        x.SubGroup == subGroup ||
                        x.SubGroup == SubGroup.Separateless ||
                        subGroup == SubGroup.Separateless
                        )
                    || x.SubGroup == SubGroup.Separateless &&
                    (
                        x.WeekType == weekType ||
                        x.WeekType == WeekType.Simple ||
                        weekType == WeekType.Simple
                        )
                    || weekType == WeekType.Simple &&
                    (
                        x.SubGroup == subGroup ||
                        x.SubGroup == SubGroup.Separateless
                        )
                    || subGroup == SubGroup.Separateless &&
                    (
                        x.WeekType == weekType ||
                        x.WeekType == WeekType.Simple
                        )
                    || weekType == WeekType.Simple && subGroup == SubGroup.Separateless
                    )).ToList();

        }

        public void Delete(Schedule schedule)
        {
            var recordsForDelete = DataContext.Lessons.Where(x => x.LessonHour.Schedule.Id == schedule.Id);
            DataContext.Lessons.RemoveRange(recordsForDelete);
            DataContext.SaveChanges();
        }

        private double GetLessonHourScheduleCount(Lesson lesson)
        {
            var day = lesson.LessonHour.Schedule.FirstDay;
            var weekStart = day.DayOfWeek;
            var currentWeekType = WeekType.UpLine;
            var holidaysDates = DataContext.Holidays.Select(x => x.Date).ToList();
            var lessonHourCount = 0.0;
            var group1PracticeCount = 0.0;
            var group2PracticeCount = 0.0;
            while (day <= lesson.LessonHour.Schedule.LastDay)
            {
                if (lesson.LessonHour.Day == day.DayOfWeek && (lesson.WeekType == currentWeekType || lesson.WeekType == WeekType.Simple)
                    && !holidaysDates.Any(x => x.Date.Month == day.Month && x.Date.Day == day.Day))
                {
                    if (lesson.LessonType == LessonType.Practice)
                    {
                        if (lesson.SubGroup == SubGroup.Group1)
                        {
                            group1PracticeCount += 1;
                        }

                        if (lesson.SubGroup == SubGroup.Group2)
                        {
                            group2PracticeCount += 1;
                        }

                        if (lesson.SubGroup == SubGroup.Separateless)
                        {
                            lessonHourCount += 1;
                        }
                    }
                    else
                    {
                        lessonHourCount += lesson.SubGroup == SubGroup.Separateless ? 1 : 0.5;
                    }
                }

                day = day.AddDays(1);
                if (day.DayOfWeek == weekStart)
                {
                    currentWeekType = currentWeekType == WeekType.UpLine
                        ? WeekType.UnderLine
                        : WeekType.UpLine;
                }
            }

            return lessonHourCount + Math.Max(group1PracticeCount, group2PracticeCount);
        }

        public Dictionary<Guid, double> GetFactLessonsCounts(Schedule schedule)
        {
            var result = new Dictionary<Guid, double>();
            if (schedule == null)
            {
                return result;
            }

            var scheduleLessons = DataContext.Lessons
                .Include(x => x.Group)
                .Where(x => x.LessonHour.Schedule.Id == schedule.Id);
            foreach (var scheduleLesson in scheduleLessons)
            {
                var lessonCount = GetLessonHourScheduleCount(scheduleLesson);
                if (result.ContainsKey(scheduleLesson.Group.Id))
                {
                    result[scheduleLesson.Group.Id] += lessonCount;
                }
                else
                {
                    result.Add(scheduleLesson.Group.Id, lessonCount);
                }
            }

            return result;
        }
    }
}
