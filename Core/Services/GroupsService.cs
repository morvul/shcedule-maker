﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис "Группы"
    /// </summary>
    public class GroupsService : BaseReferenceService<Group>
    {
        public GroupsService(DataContext dataContext)
            : base(dataContext)
        {
        }

        public IEnumerable<Group> GetAvailableInSchedule(LessonHour lessonHour)
        {
            return lessonHour == null
                ? new List<Group>() : GetAvailableInSchedule(lessonHour.Schedule);
        }

        public IEnumerable<Group> GetAvailableInSchedule(Schedule schedule)
        {
            if (schedule == null)
            {
                return new List<Group>();
            }

            var query = from groups in DataContext.Groups
                        where groups.StartLern.Year <= schedule.FirstDay.Year &&
                             groups.EndLern.Year >= schedule.FirstDay.Year
                        orderby groups.Name, groups.StartLern
                        select groups;
            return query.Include(x => x.LearningPlans).ToList();
        }
    }
}
