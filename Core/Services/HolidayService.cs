﻿using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Logic.Services
{
    /// <summary>
    /// Сервис "Праздники"
    /// </summary>
    public class HolidayService : BaseReferenceService<Holiday>
    {
        public HolidayService(DataContext dataContext)
            : base(dataContext)
        {
        }
    }
}
