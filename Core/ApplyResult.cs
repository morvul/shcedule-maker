﻿namespace ScheduleMaker.Logic
{
    public enum ApplyResult
    {
        FullSuccess = 0,
        Success = 1,
        NoFreeHours = 2,
        NoFreeRooms = 3,
        NoFitRoom = 4
    }
}
