﻿using System;
using System.Collections.Generic;

namespace ScheduleMaker.Logic.Helpers
{
    public static class EnumHelpers
    {
        public static string Description(this Enum value)
        {
            return ReflectionHelpers.GetCustomDescription(value);
        }

        /// <summary>
        /// Gets all items for an enum type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static IEnumerable<T> GetAllItems<T>() where T : struct
        {
            foreach (object item in Enum.GetValues(typeof(T)))
            {
                yield return (T)item;
            }
        }

        public static IEnumerable<KeyValuePair<T, string>> GetAllItemsWithDescription<T>() where T : struct
        {
            foreach (object item in Enum.GetValues(typeof(T)))
            {
                T enumItem = (T) item;
                yield return  new KeyValuePair<T, string>(enumItem, (enumItem as Enum).Description());
            }
        }
        
    }
}
