﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Novacode;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;

namespace ScheduleMaker.Logic.Helpers
{
    /// <summary>
    /// Расположение столбцов с временем занятий
    /// </summary>
    public enum HoursTypes
    {
        None,
        Left,
        Right
    }

    /// <summary>
    /// Генератор docx-файлов "Общего" расписания
    /// </summary>
    public class CommonScheduleGenerator
    {
        private readonly string[] _templateStrings = new string[]
        {
            "ПредметНЧ", "АудиторияНЧ", "ПреподавательНЧ", "ПредметПЧ", "АудиторияПЧ", "ПреподавательПЧ", "Предмет1П",
            "Аудитория1П", "Преподаватель1П", "Предмет2П", "Аудитория2П", "Преподаватель2П", "ПредметНЧП1",
            "АудиторияНЧП1", "ПреподавательНЧП1", "ПредметНЧП2", "АудиторияНЧП2", "ПреподавательНЧП2", "ПредметПЧП1",
            "АудиторияПЧП1", "ПреподавательПЧП1", "ПредметПЧП2", "АудиторияПЧП2", "ПреподавательПЧП2", "Преподаватель",
            "Аудитория"
        };

        private readonly Row _lSimple;
        private readonly Row _lSubGr;
        private readonly Row _lSubWeekUpline;
        private readonly Row _lSubWeekUnderline;
        private readonly Row _lSubGrWeekUpline;
        private readonly Row _lSubGrWeekUnderline;
        private readonly Row _lEmptyHour;
        private readonly Row _lSeparator;
        private readonly Row _mSimple;
        private readonly Row _mSubGr;
        private readonly Row _mSubWeekUpline;
        private readonly Row _mSubWeekUnderline;
        private readonly Row _mSubGrWeekUpline;
        private readonly Row _mSubGrWeekUnderline;
        private readonly Row _mSeparator;
        private readonly Row _rSimple;
        private readonly Row _rSubGr;
        private readonly Row _rSubWeekUpline;
        private readonly Row _rSubWeekUnderline;
        private readonly Row _rSubGrWeekUpline;
        private readonly Row _rSubGrWeekUnderline;
        private readonly Row _rSeparator;
        private readonly Table _leftTemplate;
        private readonly Table _middleTemplate;
        private readonly Table _rightTemplate;
        private readonly LessonsService _lessonsService;
        private readonly Row _rEmptyHour;
        private readonly Row _mEmptyHour;
        private readonly Row _lSimpleSubGrWeekUpline;
        private readonly Row _lSimpleSubGrWeekUnderLineline;
        private readonly Row _lSubGrWeekUnderLinelineSimple;
        private readonly Row _lSubGrWeekUplineSimple;
        private readonly Row _mSimpleSubGrWeekUpline;
        private readonly Row _mSimpleSubGrWeekUnderLineline;
        private readonly Row _mSubGrWeekUplineSimple;
        private readonly Row _mSubGrWeekUnderLinelineSimple;
        private readonly Row _rSimpleSubGrWeekUpline;
        private readonly Row _rSimpleSubGrWeekUnderLineline;
        private readonly Row _rSubGrWeekUnderLinelineSimple;
        private readonly Row _rSubGrWeekUplineSimple;


        private double _maxRowHeight;
        private readonly List<Table> _tables;

        /// <summary>
        /// Флаг успешной загрузки шаблона расписания
        /// </summary>
        public bool IsReady { get; set; }

        /// <summary>
        /// Создание объекта генератора на основе шаблона 
        /// </summary>
        /// <param name="templatePath">Путь к файлу шаблона</param>
        /// <param name="lessonsService">Объект сервиса занятий</param>
        public CommonScheduleGenerator(string templatePath, LessonsService lessonsService)
        {
            _lessonsService = lessonsService;
            _maxRowHeight = 0;
            _tables = new List<Table>();
            IsReady = false;
            using (DocX template = WordHelper.OpenTemplate(templatePath, "Шаблон не найден\n\"{0}\""))
            {
                if (template == null) return;
                _leftTemplate = template.Tables[0];
                if (_leftTemplate == null)
                {
                    MessageBox.Show("Таблица шаблона расписания не найдена\n",
                        "Ошибка анализа шаблона", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                try
                {
                    _lSimple = _leftTemplate.Rows[1];
                    _lSubGr = _leftTemplate.Rows[2];
                    _lSubWeekUpline = _leftTemplate.Rows[3];
                    _lSubWeekUnderline = _leftTemplate.Rows[4];
                    _lSubGrWeekUpline = _leftTemplate.Rows[5];
                    _lSubGrWeekUnderline = _leftTemplate.Rows[6];
                    _lSimpleSubGrWeekUpline = _leftTemplate.Rows[7];
                    _lSimpleSubGrWeekUnderLineline = _leftTemplate.Rows[8];
                    _lSubGrWeekUplineSimple = _leftTemplate.Rows[9];
                    _lSubGrWeekUnderLinelineSimple = _leftTemplate.Rows[10];
                    _lEmptyHour = _leftTemplate.Rows[11];
                    _lSeparator = _leftTemplate.Rows[12];
                    _leftTemplate.Rows.ForEach(x => SetMaxRowHeight(x.Height));
                    SetMaxRowHeight(_lSubWeekUpline.Height * 2);
                    SetMaxRowHeight(_lSubWeekUnderline.Height * 2);
                    SetMaxRowHeight(_lSubGrWeekUpline.Height * 2);
                    SetMaxRowHeight(_lSubGrWeekUnderline.Height * 2);
                    SetMaxRowHeight(_lSimpleSubGrWeekUpline.Height * 2);
                    SetMaxRowHeight(_lSimpleSubGrWeekUnderLineline.Height * 2);
                    SetMaxRowHeight(_lSubGrWeekUplineSimple.Height * 2);
                    SetMaxRowHeight(_lSubGrWeekUnderLinelineSimple.Height * 2);
                }
                catch (Exception)
                {
                    MessageBox.Show("Строка шаблона не найдена\n",
                        "Ошибка анализа шаблона", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                _middleTemplate = template.Tables[2];
                if (_middleTemplate == null)
                {
                    MessageBox.Show("Таблица шаблона расписания не найдена\n",
                        "Ошибка анализа шаблона", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                try
                {
                    _mSimple = _middleTemplate.Rows[1];
                    _mSubGr = _middleTemplate.Rows[2];
                    _mSubWeekUpline = _middleTemplate.Rows[3];
                    _mSubWeekUnderline = _middleTemplate.Rows[4];
                    _mSubGrWeekUpline = _middleTemplate.Rows[5];
                    _mSubGrWeekUnderline = _middleTemplate.Rows[6];
                    _mSimpleSubGrWeekUpline = _middleTemplate.Rows[7];
                    _mSimpleSubGrWeekUnderLineline = _middleTemplate.Rows[8];
                    _mSubGrWeekUplineSimple = _middleTemplate.Rows[9];
                    _mSubGrWeekUnderLinelineSimple = _middleTemplate.Rows[10];
                    _mEmptyHour = _middleTemplate.Rows[11];
                    _mSeparator = _middleTemplate.Rows[12];
                    _middleTemplate.Rows.ForEach(x => SetMaxRowHeight(x.Height));
                    SetMaxRowHeight(_mSubWeekUpline.Height * 2);
                    SetMaxRowHeight(_mSubWeekUnderline.Height * 2);
                    SetMaxRowHeight(_mSubGrWeekUpline.Height * 2);
                    SetMaxRowHeight(_mSubGrWeekUnderline.Height * 2);
                    SetMaxRowHeight(_mSimpleSubGrWeekUpline.Height * 2);
                    SetMaxRowHeight(_mSimpleSubGrWeekUnderLineline.Height * 2);
                    SetMaxRowHeight(_mSubGrWeekUplineSimple.Height * 2);
                    SetMaxRowHeight(_mSubGrWeekUnderLinelineSimple.Height * 2);
                }
                catch (Exception)
                {
                    MessageBox.Show("Строка шаблона не найдена\n",
                        "Ошибка анализа шаблона", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                _rightTemplate = template.Tables[1];
                if (_rightTemplate == null)
                {
                    MessageBox.Show("Таблица шаблона расписания не найдена\n",
                        "Ошибка анализа шаблона", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                try
                {
                    _rSimple = _rightTemplate.Rows[1];
                    _rSubGr = _rightTemplate.Rows[2];
                    _rSubWeekUpline = _rightTemplate.Rows[3];
                    _rSubWeekUnderline = _rightTemplate.Rows[4];
                    _rSubGrWeekUpline = _rightTemplate.Rows[5];
                    _rSubGrWeekUnderline = _rightTemplate.Rows[6];
                    _rSimpleSubGrWeekUpline = _rightTemplate.Rows[7];
                    _rSimpleSubGrWeekUnderLineline = _rightTemplate.Rows[8];
                    _rSubGrWeekUplineSimple = _rightTemplate.Rows[9];
                    _rSubGrWeekUnderLinelineSimple = _rightTemplate.Rows[10];
                    _rEmptyHour = _rightTemplate.Rows[11];
                    _rSeparator = _rightTemplate.Rows[12];
                    _rightTemplate.Rows.ForEach(x => SetMaxRowHeight(x.Height));
                    SetMaxRowHeight(_rSubWeekUpline.Height * 2);
                    SetMaxRowHeight(_rSubWeekUnderline.Height * 2);
                    SetMaxRowHeight(_rSubGrWeekUpline.Height * 2);
                    SetMaxRowHeight(_rSubGrWeekUnderline.Height * 2);
                    SetMaxRowHeight(_rSimpleSubGrWeekUpline.Height * 2);
                    SetMaxRowHeight(_rSimpleSubGrWeekUnderLineline.Height * 2);
                    SetMaxRowHeight(_rSubGrWeekUplineSimple.Height * 2);
                    SetMaxRowHeight(_rSubGrWeekUnderLinelineSimple.Height * 2);
                }
                catch (Exception)
                {
                    MessageBox.Show("Строка шаблона не найдена\n",
                        "Ошибка анализа шаблона", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                IsReady = true;
            }
        }

        /// <summary>
        /// Генерация таблиц расписания крайней левой колонки
        /// </summary>
        /// <param name="document">Объект результирующего документа</param>
        /// <param name="group">Группа, для которой генерируется отчет</param>
        /// <param name="weekDays">Коллекция дней недели с заполненными учебными часами</param>
        public void AddLSchedule(DocX document, Group group, List<WeekDay> weekDays)
        {
            AddSchedule(document, group, weekDays, HoursTypes.Left, _lEmptyHour, _lSimple, _lSubGr, _lSubGrWeekUpline,
                _lSubWeekUpline,
                _lSubGrWeekUnderline, _lSubWeekUnderline, _lSimpleSubGrWeekUpline, _lSimpleSubGrWeekUnderLineline,
                _lSubGrWeekUnderLinelineSimple, _lSubGrWeekUplineSimple, _lSeparator, _leftTemplate);
        }

        /// <summary>
        /// Генерация таблиц расписания средней колонки колонки (без колонок дня недели и времени)
        /// </summary>
        /// <param name="document">Объект результирующего документа</param>
        /// <param name="group">Группа, для которой генерируется отчет</param>
        /// <param name="weekDays">Коллекция дней недели с заполненными учебными часами</param>
        internal void AddMSchedule(DocX document, Group group, List<WeekDay> weekDays)
        {
            AddSchedule(document, group, weekDays, HoursTypes.None, _mEmptyHour, _mSimple, _mSubGr, _mSubGrWeekUpline,
                _mSubWeekUpline,
                _mSubGrWeekUnderline, _mSubWeekUnderline, _mSimpleSubGrWeekUpline, _mSimpleSubGrWeekUnderLineline,
                _mSubGrWeekUnderLinelineSimple, _mSubGrWeekUplineSimple, _mSeparator, _middleTemplate);
        }

        /// <summary>
        /// Генерация таблиц расписания крайней правой колонки
        /// </summary>
        /// <param name="document">Объект результирующего документа</param>
        /// <param name="group">Группа, для которой генерируется отчет</param>
        /// <param name="weekDays">Коллекция дней недели с заполненными учебными часами</param>
        internal void AddRSchedule(DocX document, Group group, List<WeekDay> weekDays)
        {
            AddSchedule(document, group, weekDays, HoursTypes.Right, _rEmptyHour, _rSimple, _rSubGr, _rSubGrWeekUpline,
                _rSubWeekUpline,
                _rSubGrWeekUnderline, _rSubWeekUnderline, _rSimpleSubGrWeekUpline, _rSimpleSubGrWeekUnderLineline,
                _rSubGrWeekUnderLinelineSimple, _rSubGrWeekUplineSimple, _rSeparator, _rightTemplate);
        }

        /// <summary>
        /// Генерация таблиц расписания для группы
        /// </summary>
        public void AddSchedule(DocX document, Group group, List<WeekDay> weekDays, HoursTypes hoursType,
            Row emptyHourRow, Row simpleRow, Row subGrRow, Row subGrWeekUplineRow, Row subWeekUplineRow,
            Row subGrWeekUnderlineRow, Row subWeekUnderlineRow, Row simpleSubGrWeekUpline, Row simpleSubGrWeekUnderline,
            Row subGrWeekUnderlineSimple, Row subGrWeekUplineSimple,
            Row separatorRow, Table template)
        {
            var firstDay = weekDays.FirstOrDefault();
            foreach (var day in weekDays)
            {
                if (!day.LessonHours.Any()) return;
                var table = AddEmptyWeekTable(document, template, group.FullName);
                if (hoursType != HoursTypes.None)
                {
                    table.InsertParagraphAfterSelf("");
                }

                _tables.Add(table);
                int rowId = 0;

                int rowsPerDay = 0;
                foreach (var lessonHour in day.LessonHours)
                {
                    var groupHourLessons = lessonHour.Lessons.Where(x => x.Group.Id == group.Id).ToList();
                    if (!groupHourLessons.Any())
                    {
                        rowId++;
                        rowsPerDay++;
                        var row = table.InsertRow(emptyHourRow);
                        row.BreakAcrossPages = false;
                        if (hoursType != HoursTypes.None)
                        {
                            row.ReplaceText("ДЕНЬ", lessonHour.DayName.ToUpper());
                            row.ReplaceText("ЧЧ", lessonHour.StartTime.Hour.ToString());
                            row.ReplaceText("мм", lessonHour.StartTime.Minute.ToString("00"));
                        }

                        continue;
                    }

                    Row upLine = null;
                    Row underLine = null;
                    Row simple;

                    #region Определение строк 

                    var simpleHourLessons = groupHourLessons.Where(x => x.WeekType == WeekType.Simple).ToList();
                    var uplineHourLessons = groupHourLessons.Where(x => x.WeekType == WeekType.UpLine).ToList();
                    var underlineHourLessons = groupHourLessons.Where(x => x.WeekType == WeekType.UnderLine).ToList();
                    if (simpleHourLessons.Any())
                    {
                        var firstSimple = simpleHourLessons.First();
                        if (!uplineHourLessons.Any() && !underlineHourLessons.Any())
                        {
                            simple = table.InsertRow(firstSimple.SubGroup == SubGroup.Separateless
                                ? simpleRow
                                : subGrRow);
                            rowId++;
                            rowsPerDay++;
                            SetNewRowValues(simple, lessonHour, hoursType);
                        }
                        else
                        {
                            if (firstSimple.SubGroup == SubGroup.Group1)
                            {
                                upLine = table.InsertRow(subGrWeekUplineSimple);
                                underLine = table.InsertRow(subGrWeekUnderlineSimple);
                            }
                            else
                            {
                                upLine = table.InsertRow(simpleSubGrWeekUpline);
                                underLine = table.InsertRow(simpleSubGrWeekUnderline);
                            }

                            SetNewRowValues(upLine, lessonHour, hoursType);
                            SetNewRowValues(underLine, lessonHour, hoursType);
                            upLine.MinHeight = -1;
                            underLine.MinHeight = -1;
                            simple = upLine;
                            rowId += 2;
                            rowsPerDay += 2;
                        }
                    }
                    else
                    {
                        upLine = table.InsertRow(uplineHourLessons.Any(x => x.SubGroup != SubGroup.Separateless)
                            ? subGrWeekUplineRow
                            : subWeekUplineRow);
                        underLine = table.InsertRow(underlineHourLessons.Any(x => x.SubGroup != SubGroup.Separateless)
                            ? subGrWeekUnderlineRow
                            : subWeekUnderlineRow);
                        SetNewRowValues(upLine, lessonHour, hoursType);
                        SetNewRowValues(underLine, lessonHour, hoursType);
                        upLine.MinHeight = -1;
                        underLine.MinHeight = -1;
                        simple = upLine;
                        rowId += 2;
                        rowsPerDay += 2;
                    }

                    #endregion


                    foreach (var lessonRecord in groupHourLessons)
                    {
                        var lesson = _lessonsService.GetById(lessonRecord.Id);
                        switch (lesson.WeekType)
                        {
                            case WeekType.Simple:
                                if (lesson.SubGroup == SubGroup.Separateless)
                                {
                                    simple.ReplaceText("Предмет", lesson.Subject.Name);
                                    if (lesson.Room != null)
                                    {
                                        simple.ReplaceText("Аудитория", lesson.Room.Name);
                                    }

                                    simple.ReplaceText("Преподаватель", lesson.TeachersList);
                                }
                                else
                                {
                                    if (lesson.SubGroup == SubGroup.Group1)
                                    {
                                        simple.ReplaceText("Предмет1П", lesson.Subject.Name);
                                        if (lesson.Room != null)
                                        {
                                            simple.ReplaceText("Аудитория1П", lesson.Room.Name);
                                        }

                                        simple.ReplaceText("Преподаватель1П", lesson.TeachersList);
                                    }
                                    else if (lesson.SubGroup == SubGroup.Group2)
                                    {
                                        simple.ReplaceText("Предмет2П", lesson.Subject.Name);
                                        if (lesson.Room != null)
                                        {
                                            simple.ReplaceText("Аудитория2П", lesson.Room.Name);
                                        }

                                        simple.ReplaceText("Преподаватель2П", lesson.TeachersList);
                                    }
                                }

                                SetMaxRowHeight(simple.Height);
                                break;
                            case WeekType.UpLine:
                                if (lesson.SubGroup == SubGroup.Separateless)
                                {
                                    if (upLine != null)
                                    {
                                        upLine.ReplaceText("ПредметНЧ", lesson.Subject.Name);
                                        if (lesson.Room != null)
                                        {
                                            upLine.ReplaceText("АудиторияНЧ", lesson.Room.Name);
                                        }

                                        upLine.ReplaceText("ПреподавательНЧ", lesson.TeachersList);
                                    }
                                }
                                else
                                {
                                    if (lesson.SubGroup == SubGroup.Group1)
                                    {
                                        if (upLine != null)
                                        {
                                            upLine.ReplaceText("ПредметНЧП1", lesson.Subject.Name);
                                            if (lesson.Room != null)
                                            {
                                                upLine.ReplaceText("АудиторияНЧП1", lesson.Room.Name);
                                            }

                                            upLine.ReplaceText("ПреподавательНЧП1", lesson.TeachersList);
                                        }
                                    }
                                    else if (lesson.SubGroup == SubGroup.Group2)
                                    {
                                        if (upLine != null)
                                        {
                                            upLine.ReplaceText("ПредметНЧП2", lesson.Subject.Name);
                                            if (lesson.Room != null)
                                            {
                                                upLine.ReplaceText("АудиторияНЧП2", lesson.Room.Name);
                                            }

                                            upLine.ReplaceText("ПреподавательНЧП2", lesson.TeachersList);
                                        }
                                    }
                                }
                                break;

                            case WeekType.UnderLine:
                                if (lesson.SubGroup == SubGroup.Separateless)
                                {
                                    if (underLine != null)
                                    {
                                        underLine.ReplaceText("ПредметПЧ", lesson.Subject.Name);
                                        if (lesson.Room == null)
                                        {
                                            underLine.RemoveParagraph(
                                                underLine.Paragraphs.FirstOrDefault(x => x.Text == "АудиторияПЧ"));
                                        }
                                        else
                                        {
                                            underLine.ReplaceText("АудиторияПЧ", lesson.Room.Name);
                                        }

                                        underLine.ReplaceText("ПреподавательПЧ", lesson.TeachersList);
                                    }
                                }
                                else
                                {
                                    if (lesson.SubGroup == SubGroup.Group1)
                                    {
                                        if (underLine != null)
                                        {
                                            underLine.ReplaceText("ПредметПЧП1", lesson.Subject.Name);
                                            if (lesson.Room != null)
                                            {
                                                underLine.ReplaceText("АудиторияПЧП1", lesson.Room.Name);
                                            }

                                            underLine.ReplaceText("ПреподавательПЧП1", lesson.TeachersList);
                                        }
                                    }
                                    else if (lesson.SubGroup == SubGroup.Group2)
                                    {
                                        if (underLine != null)
                                        {
                                            underLine.ReplaceText("ПредметПЧП2", lesson.Subject.Name);
                                            if (lesson.Room != null)
                                            {
                                                underLine.ReplaceText("АудиторияПЧП2", lesson.Room.Name);
                                            }

                                            underLine.ReplaceText("ПреподавательПЧП2", lesson.TeachersList);
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }

                CleanNotFilledCells(table);
                if (hoursType == HoursTypes.Left)
                {
                    table.MergeCellsInColumn(0, rowId - rowsPerDay + 1, rowId);
                }
                else if (hoursType == HoursTypes.Right)
                {
                    table.MergeCellsInColumn(2, rowId - rowsPerDay + 1, rowId);
                }

                if (firstDay?.Day != day.Day)
                {
                    table.RemoveRow(0);
                    var row = table.InsertRow(separatorRow, 0);
                    row.Height = separatorRow.Height;
                }
            }
        }


        /// <summary>
        /// Установка общих значений для только что созданной строки таблицы
        /// </summary>
        /// <param name="row">Строка</param>
        /// <param name="lessonHour">Учебный час</param>
        /// <param name="hoursType">Тип таблицы (с указанием времени занятия или без)</param>
        private void SetNewRowValues(Row row, LessonHour lessonHour, HoursTypes hoursType)
        {
            row.BreakAcrossPages = false;
            if (hoursType != HoursTypes.None)
            {
                row.ReplaceText("ДЕНЬ", lessonHour.DayName.ToUpper());
                row.ReplaceText("ЧЧ", lessonHour.StartTime.Hour.ToString());
                row.ReplaceText("мм", lessonHour.StartTime.Minute.ToString("00"));
            }
        }

        /// <summary>
        /// Установка одинаковой высоты строк для всех таблиц документа
        /// </summary>
        public void SetEqualsRowsHeight()
        {
            foreach (var table in _tables)
            {
                for (var i = table.Rows.Count - 1; i > 0; i--)
                {
                    if (table.Rows[i].MinHeight < 0)
                    {
                        table.Rows[i].Height = _maxRowHeight / 2;
                    }
                    else
                    {
                        table.Rows[i].Height = _maxRowHeight;
                    }
                }
            }
        }

        /// <summary>
        /// Определение максимального значения
        /// </summary>
        /// <param name="value"></param>
        private void SetMaxRowHeight(double value)
        {
            if (value > _maxRowHeight)
            {
                _maxRowHeight = value;
            }
        }

        /// <summary>
        /// Очистка шаблонных надписей
        /// </summary>
        /// <param name="table"></param>
        private void CleanNotFilledCells(Table table)
        {
            foreach (var row in table.Rows)
            {
                foreach (var cell in row.Cells)
                {
                    foreach (var paragraph in cell.Paragraphs)
                    {
                        DeleteAllWards(paragraph, cell);
                    }

                    if (!cell.Paragraphs.Any())
                    {
                        cell.InsertParagraph("");
                    }
                }
            }
        }

        private void DeleteAllWards(Paragraph paragraph, Cell cell)
        {
            string maxLenthWord = "";
            foreach (var templateString in _templateStrings)
            {
                if (paragraph.Text.Equals(templateString))
                {
                    cell.RemoveParagraph(paragraph);
                }
                else if (paragraph.Text.Contains(templateString))
                {
                    if (maxLenthWord.Length < templateString.Length)
                    {
                        maxLenthWord = templateString;
                    }
                }
            }

            if (maxLenthWord.Any())
            {
                paragraph.ReplaceText(maxLenthWord, "");
                if (string.IsNullOrEmpty(paragraph.Text))
                {
                    cell.RemoveParagraph(paragraph);
                }
                else
                {
                    DeleteAllWards(paragraph, cell);
                }
            }
        }

        /// <summary>
        /// Создание пустой таблицы
        /// </summary>
        private Table AddEmptyWeekTable(DocX document, Table templateTable, string groupName)
        {
            var addedTable = document.InsertTable(templateTable);
            for (int i = addedTable.RowCount - 1; i > 0; i--)
            {
                addedTable.RemoveRow(i);
            }

            addedTable.Rows[0].ReplaceText("Группа", groupName);
            return addedTable;
        }

        /// <summary>
        /// Подготовка пустого документа на основе шаблонного
        /// </summary>
        /// <param name="document"></param>
        public void CleanDocument(DocX document)
        {
            for (var i = document.Paragraphs.Count - 1; i > 50; i--)
            {
                var par = document.Paragraphs[i];
                if (par.FollowingTable != null && !par.Text.Any())
                {
                    document.RemoveParagraphAt(i);
                }
            }

            for (var i = document.Tables.Count - 1; i >= 0; i--)
            {
                document.Tables[i].Remove();
            }
        }
    }
}