﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Novacode;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;

namespace ScheduleMaker.Logic.Helpers
{
    public class AuditoryScheduleGenerator
    {
        private readonly LessonsService _lessonsService;
        public bool IsReady { get; set; }

        public AuditoryScheduleGenerator(LessonsService lessonsService)
        {
            IsReady = true;
            _lessonsService = lessonsService;
        }

        internal void Generate(DocX document, Room room, Schedule schedule,
            List<WeekDay> weekDays, List<LessonHour> lessonHours)
        {
            document.PageLayout.Orientation = Orientation.Landscape;
            var title = document.InsertParagraph("Расписание для аудитории " + room.Name +
                " (" + schedule.Name + ")");
            title.Alignment = Alignment.center;
            title.FontSize(16);
            title.Bold();
            title.Font(FontFamily.GenericSansSerif);
            document.InsertParagraph();
            var table = document.AddTable(weekDays.Count * 2 + 1, lessonHours.Count + 1);
            table.Alignment = Alignment.center;
            int columnId = 1;
            SetHeaderText(table.Rows[0].Cells[0], "Время");
            foreach (var lessonHour in lessonHours)
            {
                SetHeaderText(table.Rows[0].Cells[columnId++], lessonHour.DisplayName);
            }

            int rowId = 1;
            foreach (var weekDay in weekDays)
            {
                SetHeaderText(table.Rows[rowId].Cells[0], weekDay.Name);
                SetHeaderText(table.Rows[rowId + 1].Cells[0], weekDay.Name);
                table.MergeCellsInColumn(0, rowId, rowId + 1);
                columnId = 1;
                foreach (var time in lessonHours)
                {
                    var lessonHour = weekDay.LessonHours.FirstOrDefault(x =>
                        x.StartTime == time.StartTime);
                    var lessons = new List<Lesson>();
                    if (lessonHour != null)
                    {
                        lessons = lessonHour.Lessons.Where(x =>
                            x.Room != null && x.Room.Id == room.Id).ToList();
                    }

                    bool merged = false;
                    if (!lessons.Any())
                    {
                        table.MergeCellsInColumn(columnId, rowId, rowId + 1);
                        merged = true;
                    }


                    foreach (var lessonId in lessons)
                    {
                        var lesson = _lessonsService.GetById(lessonId.Id);
                        switch (lesson.WeekType)
                        {
                            case WeekType.Simple:
                                if (!merged)
                                {
                                    table.MergeCellsInColumn(columnId, rowId, rowId + 1);
                                    merged = true;
                                }

                                AddLesson(table.Rows[rowId].Cells[columnId], lesson);
                                break;
                            case WeekType.UpLine:
                                AddLesson(table.Rows[rowId].Cells[columnId], lesson);
                                break;
                            case WeekType.UnderLine:
                                AddLesson(table.Rows[rowId+1].Cells[columnId], lesson);
                                break;
                        }
                    }

                    columnId++;
                }

                rowId += 2;
            }

            document.InsertTable(table);
        }

        private void AddLesson(Cell cell, Lesson lesson)
        {
            if (cell.Paragraphs.All(x => x.Text != lesson.Subject.Name))
            {
                cell.InsertParagraph(lesson.Subject.Name).Alignment = Alignment.center;
            }

            if (cell.Paragraphs.All(x => x.Text != lesson.Group.Name))
            {
                cell.InsertParagraph(lesson.Group.Name).Alignment = Alignment.center;
            }

            if (cell.Paragraphs.All(x => x.Text != lesson.TeachersList))
            {
                cell.InsertParagraph(lesson.TeachersList).Alignment = Alignment.center;
            }
        }

        private void SetHeaderText(Cell cell, string text)
        {
            var paragraph = cell.Paragraphs.First().Append(text);
            paragraph.Bold();
            paragraph.Alignment = Alignment.center;
            cell.VerticalAlignment = VerticalAlignment.Center;
        }
    }
}
