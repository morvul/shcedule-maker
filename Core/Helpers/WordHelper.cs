﻿using System;
using System.Windows.Forms;
using Novacode;
using Application = Microsoft.Office.Interop.Word.Application;

namespace ScheduleMaker.Logic.Helpers
{
    public static class WordHelper
    {
        public static void SetCell(Table table, Cell cell, int row, int column)
        {
            Cell destCell = table.Rows[row].Cells[column];
            destCell.RemoveParagraphAt(0);
            destCell.Width = cell.Width;
            foreach (var p in cell.Paragraphs)
            {
                destCell.InsertParagraph(p);
            }
        }

        public static DocX OpenTemplate(string documentPath, string errorMsg)
        {
            string fullPath = documentPath;
            if (fullPath == null)
            {
                MessageBox.Show(string.Format(errorMsg, documentPath),
                    "Ошибка чтения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            DocX file = null;
            try
            {
                file = DocX.Load(documentPath);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Ошибка чтения", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return file;
        }

        public static void OpenDoc(string documentPath)
        {
            string fullPath = documentPath;
            if (fullPath == null)
            {
                MessageBox.Show(string.Format("Файл не найден\n\"{0}\"", documentPath),
                    "Ошибка чтения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //OBJECT OF MISSING "NULL VALUE"
            Object oMissing = System.Reflection.Missing.Value;

            //CREATING OBJECTS OF WORD AND DOCUMENT
            Application oWord = new Application { Visible = true };

            //MAKING THE APPLICATION VISIBLE

            object oTemplatePath = documentPath;
            oWord.Documents.Add(ref oTemplatePath, ref oMissing, ref oMissing, ref oMissing);
        }
    }
}
