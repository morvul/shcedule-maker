﻿using ScheduleMaker.Logic.Model;
using System;

namespace ScheduleMaker.Logic.ReadModels
{
    public class OccupiedInfo
    {
        public OccupiedInfo(Room room, WeekType weekType)
        {
            Room = room;
            WeekType = weekType;
        }

        public Room Room { get; set; }

        public WeekType WeekType { get; set; }

        public SubGroup SubGroup { get; set; }
    }
}
