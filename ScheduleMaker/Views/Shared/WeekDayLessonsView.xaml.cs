﻿using System.Windows;
using System.Windows.Controls;

namespace ScheduleMaker.Views.Shared
{
    /// <summary>
    /// Логика взаимодействия для WeekDayLessonsView.xaml
    /// </summary>
    public partial class WeekDayLessonsView
    {
        public WeekDayLessonsView()
        {
            InitializeComponent();
        }

        private void ListBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ((ListBox) sender).SelectedItem = null;
        }
    }
}
