﻿using System.Windows.Input;
using ScheduleMaker.ViewModels.Shared;

namespace ScheduleMaker.Views.Shared
{
    /// <summary>
    /// Interaction logic for FindPanelView.xaml
    /// </summary>
    public partial class FindPanelView
    {
        public FindPanelView()
        {
            InitializeComponent();
        }

        private FindPanelViewModel ViewModel
        {
            get { return (FindPanelViewModel)DataContext; }
        }

        private void OnEditFieldsKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    ViewModel.Cancel();
                    break;
                case Key.Enter:
                    ViewModel.Search();
                    Keyboard.Focus(FilterText);
                    break;
                default:
                    if (((int)e.Key > 33 && (int)e.Key < 70)||((int)e.Key >72 && (int)e.Key <90) || (int)e.Key == 32 || (int)e.Key == 31 || (int)e.Key == 2)
                    {
                        ViewModel.IsFilterActive = false;
                        Keyboard.Focus(FilterText);
                    }
                    break;
            }
            
        }
    }
}
