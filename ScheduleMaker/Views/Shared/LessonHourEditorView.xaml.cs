﻿using System.Windows.Input;
using ScheduleMaker.ViewModels.Shared;

namespace ScheduleMaker.Views.Shared
{
    /// <summary>
    /// Interaction logic for LessonHourEditor.xaml
    /// </summary>
    public partial class LessonHourEditorView
    {
        public LessonHourEditorView()
        {
            InitializeComponent();
        }

        private LessonHourEditorViewModel ViewModel
        {
            get { return (LessonHourEditorViewModel)DataContext; }
        }

        private void OnEditFieldsKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    ViewModel.Cancel();
                    break;
                case Key.Enter:
                    ViewModel.Save();
                    break;
            }
        }
    }
}
