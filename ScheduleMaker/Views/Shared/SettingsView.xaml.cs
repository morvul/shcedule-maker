﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using ScheduleMaker.ViewModels.Shared;

namespace ScheduleMaker.Views.Shared
{
    /// <summary>
    /// Логика взаимодействия для SettingsView.xaml
    /// </summary>
    public partial class SettingsView
    {
        public SettingsView()
        {
            InitializeComponent();
            Loaded += SchedulesView_OnLoaded;
        }

        private SettingsViewModel ViewModel
        {
            get { return (SettingsViewModel)DataContext; }
        }

        private void OnEditFieldsKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    ViewModel.Cancel();
                    break;
                case Key.Enter:
                    ViewModel.Save();
                    break;
            }
        }

        private void SchedulesView_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (ViewModel.IsEditMode)
            {
                Keyboard.Focus(StartElement);
            }
        }

        private void OnNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
            e.Handled = true;
        }
    }
}
