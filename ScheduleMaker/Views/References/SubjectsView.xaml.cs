﻿using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ScheduleMaker.ViewModels.References;

namespace ScheduleMaker.Views.References
{
    /// <summary>
    /// Interaction logic for SubjectsView.xaml
    /// </summary>
    public partial class SubjectsView
    {
        public SubjectsView()
        {
            InitializeComponent();
            CollectionView myCollectionView = (CollectionView)CollectionViewSource.GetDefaultView(DataGrid.Items);
            ((INotifyCollectionChanged)myCollectionView).CollectionChanged += DataGrid_CollectionChanged;
            Loaded += SchedulesView_OnLoaded;
        }

        private SubjectsViewModel ViewModel
        {
            get { return (SubjectsViewModel)DataContext; }
        }

        private void DataGrid_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                var view = CollectionViewSource.GetDefaultView(DataGrid.ItemsSource);
                if (view != null && view.SortDescriptions.Count > 0)
                {
                    view.SortDescriptions.Clear();
                    foreach (var column in DataGrid.Columns)
                    {
                        column.SortDirection = null;
                    }
                }
            }
        }

        private void OnDataGridKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Delete:
                    if (ViewModel.CanDelete)
                    {
                        ViewModel.Delete();
                    }
                    break;
                case Key.Insert:
                    if (ViewModel.CanNew)
                    {
                        ViewModel.New();
                    }
                    break;
                case Key.F2:
                    if (ViewModel.CanEdit)
                    {
                        ViewModel.Edit();
                    }
                    break;
            }
        }

        private void OnEditFieldsKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    ViewModel.Cancel();
                    break;
                case Key.Enter:
                    ViewModel.Save();
                    break;
            }
        }

        private void OnEditPanelVisibilityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Update();
        }

        private void OnSelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            Update();
        }

        private void Update()
        {
            if (DataGrid.SelectedIndex > -1 && !ViewModel.IsEditMode && ViewModel.SelectedIndex < DataGrid.Items.Count)
            {
                DataGrid.SelectedItem = DataGrid.Items[ViewModel.SelectedIndex];
                DataGrid.ScrollIntoView(DataGrid.Items[ViewModel.SelectedIndex]);
                DataGridRow dgrow =
                    (DataGridRow)DataGrid.ItemContainerGenerator.ContainerFromItem(DataGrid.Items[ViewModel.SelectedIndex]);
                if (dgrow != null)
                {
                    dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                }
            }
        }

        private void SchedulesView_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (ViewModel.IsEditMode)
            {
                Keyboard.Focus(StartElement);
            }
        }
    }
}
