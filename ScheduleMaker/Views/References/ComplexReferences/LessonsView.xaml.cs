﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ScheduleMaker.ViewModels;
using ScheduleMaker.ViewModels.References.ComplexReferences;

namespace ScheduleMaker.Views.References.ComplexReferences
{
    /// <summary>
    /// Логика взаимодействия для LessonsView.xaml
    /// </summary>
    public partial class LessonsView
    {
        public LessonsView()
        {
            InitializeComponent();
            Loaded += SchedulesView_OnLoaded;
        }

        private LessonsViewModel ViewModel
        {
            get { return (LessonsViewModel)DataContext; }
        }

        private void OnDataGridKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Delete:
                    if (ViewModel.CanDelete)
                    {
                        ViewModel.Delete();
                    }
                    break;
                case Key.Insert:
                    if (ViewModel.CanNew)
                    {
                        ViewModel.New();
                    }
                    break;
                case Key.F2:
                    if (ViewModel.CanEdit)
                    {
                        ViewModel.Edit();
                    }
                    break;
            }
        }

        private void OnEditFieldsKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    ViewModel.Cancel();
                    break;
                case Key.Enter:
                    ViewModel.Save();
                    break;
            }
        }

        private void OnEditPanelVisibilityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Update();
        }

        private void OnSelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            Update();
        }

        private void Update()
        {
            if (DataGrid.SelectedIndex > -1 && !ViewModel.IsEditMode && ViewModel.SelectedIndex < DataGrid.Items.Count)
            {
                DataGrid.SelectedItem = DataGrid.Items[ViewModel.SelectedIndex];
                DataGrid.ScrollIntoView(DataGrid.Items[ViewModel.SelectedIndex]);
                DataGridRow dgrow =
                    (DataGridRow)DataGrid.ItemContainerGenerator.ContainerFromItem(DataGrid.Items[ViewModel.SelectedIndex]);
                dgrow?.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
        }

        private void Groups_OnIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Groups.IsEnabled)
            {
                Keyboard.Focus(Groups);
            }
        }

        private void SchedulesView_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (ViewModel.IsEditMode)
            {
                switch (ViewModel.CurrentField)
                {
                    case Screens.GroupsReference:
                        Keyboard.Focus(Groups);
                        break;
                    case Screens.TeachersReference:
                        Keyboard.Focus(Teachers);
                        break;
                    case Screens.SubjectsReference:
                        Keyboard.Focus(Subjects);
                        break;
                    case Screens.RoomsReference:
                        Keyboard.Focus(Auditories);
                        break;
                }

                ViewModel.CurrentField = Screens.None;
            }
        }
    }
}
