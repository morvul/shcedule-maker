﻿using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.ViewModels;
using ScheduleMaker.ViewModels.References.ComplexReferences;

namespace ScheduleMaker.Views.References.ComplexReferences
{
    /// <summary>
    /// Interaction logic for GroupScheduleView.xaml
    /// </summary>
    public partial class GroupScheduleView : IHandle<ReferncePanelsEvent>
    {
        public GroupScheduleView()
        {
            InitializeComponent();
            Loaded += SchedulesView_OnLoaded;
            IEventAggregator events = IoC.Get<IEventAggregator>();
            events.Subscribe(this);
        }

        private GroupScheduleViewModel ViewModel
        {
            get { return (GroupScheduleViewModel)DataContext; }
        }


        private void SchedulesView_OnLoaded(object sender, RoutedEventArgs e)
        {

            switch (ViewModel.CurrentField)
            {
                case Screens.SchedulesReference:
                    Keyboard.Focus(Schedules);
                    break;
            }

            ViewModel.CurrentField = Screens.None;
        }

        private void OnEditFieldsKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    ViewModel.Cancel();
                    break;
                case Key.Enter:
                    ViewModel.Save();
                    break;
            }
        }

        public void Handle(ReferncePanelsEvent message)
        {
            if (ViewModel.IsEditMode)
            {
                Subjects.Focus();
                Keyboard.Focus(Subjects);
            }
        }
    }
}
