﻿using System.Windows;
using System.Windows.Input;
using ScheduleMaker.ViewModels;
using ScheduleMaker.ViewModels.References.ComplexReferences;

namespace ScheduleMaker.Views.References.ComplexReferences
{
    /// <summary>
    /// Interaction logic for CommonScheduleView.xaml
    /// </summary>
    public partial class CommonScheduleView
    {
        public CommonScheduleView()
        {
            InitializeComponent();
            Loaded += SchedulesView_OnLoaded;
        }

        private CommonScheduleViewModel ViewModel
        {
            get { return (CommonScheduleViewModel)DataContext; }
        }


        private void SchedulesView_OnLoaded(object sender, RoutedEventArgs e)
        {

            switch (ViewModel.CurrentField)
            {
                case Screens.SchedulesReference:
                    Keyboard.Focus(Schedules);
                    break;
            }

            ViewModel.CurrentField = Screens.None;
        }
    }
}
