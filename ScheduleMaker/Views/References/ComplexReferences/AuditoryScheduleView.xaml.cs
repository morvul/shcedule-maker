﻿using System.Windows;
using System.Windows.Input;
using ScheduleMaker.ViewModels;
using ScheduleMaker.ViewModels.References.ComplexReferences;

namespace ScheduleMaker.Views.References.ComplexReferences
{
    /// <summary>
    /// Interaction logic for CommonScheduleView.xaml
    /// </summary>
    public partial class AuditoryScheduleView
    {
        public AuditoryScheduleView()
        {
            InitializeComponent();
            Loaded += SchedulesView_OnLoaded;
        }

        private AuditoryScheduleViewModel ViewModel
        {
            get { return (AuditoryScheduleViewModel)DataContext; }
        }


        private void SchedulesView_OnLoaded(object sender, RoutedEventArgs e)
        {

            switch (ViewModel.CurrentField)
            {
                case Screens.SchedulesReference:
                    Keyboard.Focus(Schedules);
                    break;
                case Screens.TeachersReference:
                    Keyboard.Focus(Auditories);
                    break;
            }

            ViewModel.CurrentField = Screens.None;
        }
    }
}
