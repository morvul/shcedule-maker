﻿using System;
using System.IO;
using System.Windows.Forms;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Services;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace ScheduleMaker.ViewModels.Shared
{
    public class SettingsViewModel : BaseViewModel
    {
        private readonly ISettingsService _settingsService;
        private readonly IEventAggregator _eventAggregator;
        private string _commonScheduleTemplatePath;
        private bool _hasChanges;
        private string _resultDir;
        private TimeSpan _breakSize;

        public SettingsViewModel(ISettingsService settingsService,
            IEventAggregator eventAggregator)
        {
            _settingsService = settingsService;
            _eventAggregator = eventAggregator;
        }

        public override bool CanSave
        {
            get { return Error == null && HasChanges; }
        }

        public bool HasChanges
        {
            get { return _hasChanges; }
            set
            {
                _hasChanges = value;
                NotifyOfPropertyChange(() => CanSave);
            }
        }

        public string CommonScheduleTemplatePath
        {
            get { return _commonScheduleTemplatePath; }
            set
            {
                if (_commonScheduleTemplatePath != value)
                {
                    _commonScheduleTemplatePath = value;
                    HasChanges = true;
                    NotifyOfPropertyChange(() => CommonScheduleTemplatePath);
                }
            }
        }

        public string ResultDir
        {
            get { return _resultDir; }
            set
            {
                if (_resultDir != value)
                {
                    _resultDir = value;
                    HasChanges = true;
                    NotifyOfPropertyChange(() => ResultDir);
                }
            }
        }

        public TimeSpan BreakSize
        {
            get { return _breakSize; }
            set
            {
                if (_breakSize != value)
                {
                    _breakSize = value;
                    HasChanges = true;
                    NotifyOfPropertyChange(() => BreakSize);
                }
            }
        }

        public override void UpdateData()
        {
            IsEditMode = true;
            CommonScheduleTemplatePath = _settingsService.CommonScheduleTemplatePath;
            ResultDir = _settingsService.ResultDir;
            BreakSize = _settingsService.BreakSize;
            HasChanges = false;
        }

        public void Save()
        {
            _settingsService.CommonScheduleTemplatePath = CommonScheduleTemplatePath;
            _settingsService.ResultDir = ResultDir;
            _settingsService.BreakSize = BreakSize;
            Cancel();
        }

        public void Cancel()
        {
            IsEditMode = false;
            HasChanges = false;
            _eventAggregator.Publish(new ScreenEvent(ScreenOperations.GoToPrevious));
        }

        public void SelectComSchedTmpl()
        {
            var newPath = GetNewFilePath(CommonScheduleTemplatePath);
            if (newPath != null)
            {
                CommonScheduleTemplatePath = newPath;
            }
        }

        public void SelectResultFolder()
        {
            var newPath = GetNewDirPath(ResultDir);
            if (newPath != null)
            {
                ResultDir = newPath;
            }
        }

        public override string GetErrors(string propertyName)
        {
            switch (propertyName)
            {
                case "CommonScheduleTemplatePath":
                    if (!File.Exists(CommonScheduleTemplatePath))
                        return "Указан неверный путь к файлу шаблона общего расписания";
                    break;
                case "ResultDir":
                    if (!Directory.Exists(ResultDir))
                        return "Указан неверный путь к результирующей директории";
                    break;
            }

            return null;
        }

        private string GetNewFilePath(string filePath)
        {

            OpenFileDialog dlg = new OpenFileDialog
            {
                Filter = "Документы Word (*.doc;*.docx;*.dot;*.dotx)|*.doc;*.docx;*.dot;*.dotx|Все файлы|*.*"
            };

            var path = !string.IsNullOrEmpty(filePath)
                ? Path.GetDirectoryName(filePath) : "";
            if (path != null)
            {
                dlg.InitialDirectory = path;
                var file = Path.GetFileName(filePath);
                dlg.FileName = file;
            }

            if (dlg.ShowDialog() == true)
            {
                filePath = dlg.FileName;
                return filePath;
            }

            return null;
        }

        private string GetNewDirPath(string filePath)
        {
            var dlg = new FolderBrowserDialog();
            var path = !string.IsNullOrEmpty(filePath)
                ? Path.GetDirectoryName(filePath) : "";
            if (path != null)
            {
                dlg.SelectedPath = path;
            }

            if (dlg.ShowDialog()==DialogResult.OK)
            {
                return dlg.SelectedPath;
            }

            return null;
        }
    }
}
