﻿using System;
using Caliburn.Micro;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.ViewModels.Shared
{
    public class LessonViewModel : PropertyChangedBase
    {
        private LessonHour _lessonHour;
        private Lesson _simpleLesson;
        private Lesson _underlineLesson;
        private Lesson _uplineLesson;
        private Lesson _simpleSubG1Lesson;
        private Lesson _simpleSubG2Lesson;
        private Lesson _underlineSubG2Lesson;
        private Lesson _underlineSubG1Lesson;
        private Lesson _uplineSubG1Lesson;
        private Lesson _uplineSubG2Lesson;
        private WeekType _weekType;

        public LessonViewModel(LessonHour lessonHour)
        {
            LessonHour = lessonHour;
            UpdateWeekTypeAndLessons();
        }

        public LessonHour LessonHour
        {
            private set
            {
                _lessonHour = value;
                NotifyOfPropertyChange(() => LessonHour);
                NotifyOfPropertyChange(() => IsCurrentHour);
            }
            get { return _lessonHour; }
        }

        public WeekType WeekType
        {
            private set
            {
                _weekType = value;
                NotifyOfPropertyChange(() => WeekType);
            }
            get { return _weekType; }
        }

        public string LessonHourDisplayName
        {
            get { return _lessonHour.DisplayName; }
        }

        public Lesson SimpleLesson
        {
            set
            {
                _simpleLesson = value;
                NotifyOfPropertyChange(() => SimpleLesson);
            }
            get { return _simpleLesson; }
        }

        public Lesson SimpleSubG1Lesson
        {
            set
            {
                _simpleSubG1Lesson = value;
                NotifyOfPropertyChange(() => SimpleSubG1Lesson);
            }
            get { return _simpleSubG1Lesson; }
        }

        public Lesson SimpleSubG2Lesson
        {
            set
            {
                _simpleSubG2Lesson = value;
                NotifyOfPropertyChange(() => SimpleSubG2Lesson);
            }
            get { return _simpleSubG2Lesson; }
        }

        public Lesson UnderlineLesson
        {
            set
            {
                _underlineLesson = value;
                NotifyOfPropertyChange(() => UnderlineLesson);
            }
            get { return _underlineLesson; }
        }

        public Lesson UnderlineSubG1Lesson
        {
            set
            {
                _underlineSubG1Lesson = value;
                NotifyOfPropertyChange(() => UnderlineSubG1Lesson);
            }
            get { return _underlineSubG1Lesson; }
        }

        public Lesson UnderlineSubG2Lesson
        {
            set
            {
                _underlineSubG2Lesson = value;
                NotifyOfPropertyChange(() => UnderlineSubG2Lesson);
            }
            get { return _underlineSubG2Lesson; }
        }

        public Lesson UplineLesson
        {
            set
            {
                _uplineLesson = value;
                NotifyOfPropertyChange(() => UplineLesson);
            }
            get { return _uplineLesson; }
        }

        public Lesson UplineSubG1Lesson
        {
            set
            {
                _uplineSubG1Lesson = value;
                NotifyOfPropertyChange(() => UplineSubG1Lesson);
            }
            get { return _uplineSubG1Lesson; }
        }

        public Lesson UplineSubG2Lesson
        {
            set
            {
                _uplineSubG2Lesson = value;
                NotifyOfPropertyChange(() => UplineSubG2Lesson);
            }
            get { return _uplineSubG2Lesson; }
        }

        public bool IsCurrentHour
        {
            get
            {
                return DateTime.Now.DayOfWeek == LessonHour.Day
                    && DateTime.Now.TimeOfDay >= LessonHour.StartTime.TimeOfDay
                    && DateTime.Now.TimeOfDay <= LessonHour.EndTime.TimeOfDay;
            }
        }


        private void UpdateWeekTypeAndLessons()
        {
            foreach (var lesson in _lessonHour.Lessons)
            {
                switch (lesson.WeekType)
                {
                    case WeekType.Simple:
                        WeekType = WeekType.Simple;
                        switch (lesson.SubGroup)
                        {
                            case SubGroup.Separateless:
                                SimpleLesson = lesson;
                                break;
                            case SubGroup.Group1:
                                SimpleSubG1Lesson = lesson;
                                break;
                            case SubGroup.Group2:
                                SimpleSubG2Lesson = lesson;
                                break;
                        }
                        break;
                    case WeekType.UnderLine:
                        switch (lesson.SubGroup)
                        {
                            case SubGroup.Separateless:
                                UnderlineLesson = lesson;
                                break;
                            case SubGroup.Group1:
                                UnderlineSubG1Lesson = lesson;
                                break;
                            case SubGroup.Group2:
                                UnderlineSubG2Lesson = lesson;
                                break;
                        }
                        
                        break;
                    case WeekType.UpLine:
                        switch (lesson.SubGroup)
                        {
                            case SubGroup.Separateless:
                                UplineLesson = lesson;
                                break;
                            case SubGroup.Group1:
                                UplineSubG1Lesson = lesson;
                                break;
                            case SubGroup.Group2:
                                UplineSubG2Lesson = lesson;
                                break;
                        }

                        break;
                }
            }
        }
    }
}
