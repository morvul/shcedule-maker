using System.Windows.Controls;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.ViewModels.References;

namespace ScheduleMaker.ViewModels.Shared
{
    public class EditBarViewModel : PropertyChangedBase, IHandle<ReferenceBarsActivityEvent>, IHandle<ReferncePanelsEvent>
    {
        private bool _isPanelShow;
        private bool _isPanelActive;

        public EditBarViewModel(IEventAggregator events)
        {
            events.Subscribe(this);
            IsPanelActive = false;
            IsPanelVisible = false;
            Reference = null;
        }

        #region Properties

        public bool IsPanelActive
        {
            get { return _isPanelActive; }
            set
            {
                _isPanelActive = value;
                NotifyOfPropertyChange(() => IsPanelVisible);
            }
        }

        public bool IsPanelVisible
        {
            get { return _isPanelShow && IsPanelActive; }
            set
            {
                _isPanelShow = value;
                NotifyOfPropertyChange(() => IsPanelVisible);
            }
        }

        public Orientation Orientation { set; get; }

        private IReference Reference { set; get; }

        public bool CanDelete
        {
            get { return (Reference != null && Reference.CanDelete); }
        }

        public bool CanEdit
        {
            get
            {
                return (Reference != null && Reference.CanEdit);
            }
        }

        public bool CanNew
        {
            get { return (Reference != null && Reference.CanNew); }
        }

        #endregion


        #region Methods

        public void Delete()
        {
            Reference.Delete();
        }

        public void Edit()
        {
            Reference.Edit();
        }

        public void New()
        {
            Reference.New();
        }

        public void Handle(ReferenceBarsActivityEvent message)
        {
            IsPanelActive = message.IsActive;
            Reference = message.Reference;
            CheckStates();
        }

        #endregion

        public void Handle(ReferncePanelsEvent message)
        {
            CheckStates();
        }

        private void CheckStates()
        {
            NotifyOfPropertyChange(() => CanNew);
            NotifyOfPropertyChange(() => CanDelete);
            NotifyOfPropertyChange(() => CanEdit);
        }
    }
}