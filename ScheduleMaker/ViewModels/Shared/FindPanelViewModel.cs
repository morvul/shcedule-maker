using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.ViewModels.References;

namespace ScheduleMaker.ViewModels.Shared
{
    public class FindPanelViewModel : PropertyChangedBase, IHandle<ReferenceBarsActivityEvent>, IHandle<ReferncePanelsEvent>
    {
        private bool _isPanelShow;
        private bool _isPanelActive;
        private string _filterText;
        private bool _isFilterActive;

        public FindPanelViewModel(IEventAggregator events)
        {
            events.Subscribe(this);
            IsPanelActive = false;
            IsPanelVisible = false;
            Reference = null;
        }

        #region Properties

        public string FilterText
        {
            set
            {
                _filterText = value;
                NotifyOfPropertyChange(() => FilterText);
            }
            get { return _filterText; }
        }

        public bool IsPanelActive
        {
            get { return _isPanelActive; }
            set
            {
                _isPanelActive = value;
                NotifyOfPropertyChange(() => IsPanelVisible);
            }
        }

        public bool IsPanelVisible
        {
            get { return _isPanelShow && IsPanelActive; }
            set
            {
                _isPanelShow = value;
                NotifyOfPropertyChange(() => IsPanelVisible);
            }
        }

        public bool IsFilterActive
        {
            get { return _isFilterActive; }
            set
            {
                if (_isFilterActive == value)
                {
                    return;
                }

                _isFilterActive = value;
                if (!value)
                {
                    Reference.Filter("");
                }

                NotifyOfPropertyChange(() => IsFilterActive);
            }
        }

        private IReference Reference { set; get; }

        public bool CanFilter
        {
            get { return (Reference != null && Reference.CanFilter); }
        }

        #endregion


        #region Methods

        public void Handle(ReferenceBarsActivityEvent message)
        {
            IsPanelActive = message.IsActive;
            Reference = message.Reference;
            if (Reference != null)
            {
                FilterText = Reference.FilterText;
                IsFilterActive = !string.IsNullOrEmpty(FilterText);
            }

            CheckStates();
        }

        #endregion

        public void Handle(ReferncePanelsEvent message)
        {
            CheckStates();
        }

        private void CheckStates()
        {
            NotifyOfPropertyChange(() => CanFilter);
        }

        public void Cancel()
        {
            FilterText = "";
            IsFilterActive = false;
        }

        public void Search()
        {
            if (!string.IsNullOrEmpty(FilterText))
            {
                Reference.Filter(FilterText);
                IsFilterActive = true;
            }
        }
    }
}