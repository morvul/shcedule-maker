using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.ViewModels.References;

namespace ScheduleMaker.ViewModels.Shared
{
    public class LessonHourEditorViewModel : BaseReferenceViewModel<LessonHour>
    {
        private readonly LessonHoursService _lessonHoursService;
        private readonly SchedulesService _schedulesService;
        private bool _isCopying;
        private DayOfWeek _day;
        private Schedule _schedule;
        private BindableCollection<WeekDay> _weekDays;
        private WeekDay _selectedWeekDay;

        public LessonHourEditorViewModel(LessonHoursService lessonHoursService,
            SchedulesService schedulesService, IEventAggregator events)
            : base(lessonHoursService, events)
        {
            IsCopying = false;
            _schedulesService = schedulesService;
            _lessonHoursService = lessonHoursService;
        }

        public DateTime StartTime
        {
            set
            {
                CurrentItem.StartTime = value;
                UpdateFields();
            }
            get { return CurrentItem.StartTime; }
        }

        public DateTime EndTime
        {
            get { return CurrentItem.EndTime; }
        }

        public Schedule Schedule
        {
            set
            {
                _schedule = value;
                UpdateData();
            }
            get { return _schedule; }
        }

        public DayOfWeek Day
        {
            set
            {
                _day = value;
                UpdateData();
            }
            get { return _day; }
        }

        public bool IsCanCopy
        {
            get { return (!IsCopying && Records != null && Records.Count == 0 && Schedule.LessonHours.Any()); }
        }

        public bool IsCopying
        {
            set
            {
                _isCopying = value;
                if (value) IsEditMode = false;
                NotifyOfPropertyChange(() => IsCopying);
                NotifyOfPropertyChange(() => IsCanCopy);
            }
            get { return _isCopying; }
        }



        public BindableCollection<WeekDay> WeekDays
        {
            set
            {
                _weekDays = value;
                NotifyOfPropertyChange(() => WeekDays);
            }
            get { return _weekDays; }
        }

        public WeekDay SelectedWeekDay
        {
            set
            {
                _selectedWeekDay = value;
                NotifyOfPropertyChange(() => SelectedWeekDay);
                NotifyOfPropertyChange(() => CanStartCopy);
            }
            get { return _selectedWeekDay; }
        }

        public void Copy()
        {
            IsCopying = true;
            WeekDays = new BindableCollection<WeekDay>(
               _schedulesService.GetNotFreeWeekDays(Schedule, Day));
        }

        public void CancelCopy()
        {
            SelectedWeekDay = null;
            Cancel();
        }

        public override void Cancel()
        {
            base.Cancel();
            IsCopying = false;
        }

        public void StartCopy()
        {
            _lessonHoursService.CopyHours(Schedule, Day, SelectedWeekDay.Day);
            Cancel();
            Events.Publish(new UpdateChildsEvent(Screens.LessonHours));
        }

        public bool CanStartCopy
        {
            get { return SelectedWeekDay != null; }
        }

        protected override Screens CurrentScreen { get { return Screens.CommonSchedule; } }

        public override IEnumerable<LessonHour> AlternativeSourceOfRecords
        {
            get
            {
                return ReferenceService.GetAll().Where(x =>
                    x.Schedule.Id == Schedule.Id && x.Day == Day);
            }
        }

        public override bool IsEditMode
        {
            get { return base.IsEditMode; }
            protected set
            {
                base.IsEditMode = value;
                if (value) IsCopying = false;
            }
        }

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => StartTime);
            NotifyOfPropertyChange(() => EndTime);
            NotifyOfPropertyChange(() => IsCanCopy);
            NotifyOfPropertyChange(() => IsCopying);
        }

        public override void New(Screens newRecordCreator = Screens.None,
            object newRecordCreatorDetails = null)
        {
            NewRecordCreator = newRecordCreator;
            UpdateData();
            CurrentItem = new LessonHour(Schedule, Day);
            State = ReferenceStates.NewRecord;
            UpdateFields();
            NotifyOfPropertyChange(() => CanSave);
        }

        public override LessonHour CurrentItem
        {
            set
            {
                base.CurrentItem = value;
                IsEditMode = false;
            }
            get { return base.CurrentItem; }
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case "StartTime":
                    if (StartTime.TimeOfDay < Schedule.LessonsStart.TimeOfDay)
                    {
                        return String.Format(
                            "�������� ������ ���� >= ������� ������ ������� ({0:HH:mm})",
                            Schedule.LessonsStart);
                    }

                    var lessonHoursForDay = ReferenceService.GetAll().Where(x =>
                        x.Id != CurrentItem.Id && x.Schedule.Id == Schedule.Id &&
                        x.Day == Day).ToList();

                    foreach (var lessonHour in lessonHoursForDay)
                    {
                        if ((lessonHour.StartTime.TimeOfDay <= StartTime.TimeOfDay
                            && lessonHour.StartTime.TimeOfDay + lessonHour.Schedule.LessonDuration > StartTime.TimeOfDay)
                            || (StartTime.TimeOfDay + Schedule.LessonDuration > lessonHour.StartTime.TimeOfDay
                            && StartTime.TimeOfDay + Schedule.LessonDuration < lessonHour.StartTime.TimeOfDay + lessonHour.Schedule.LessonDuration)
                           )
                        {
                            return "�������� �� ������ �������� � ������� ������� �������";
                        }
                    }

                    break;
            }

            return null;
        }
    }
}