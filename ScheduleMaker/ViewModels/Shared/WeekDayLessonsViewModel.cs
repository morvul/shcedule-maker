﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.ViewModels.Shared
{
    public class WeekDayLessonsViewModel : PropertyChangedBase
    {
        private readonly IEventAggregator _events;
        private BindableCollection<LessonViewModel> _lessonHours;
        private DayOfWeek _day;

        public WeekDayLessonsViewModel(IEnumerable<LessonHour> lessonHours, DayOfWeek day,
            IEventAggregator events)
        {
            LessonHours = new BindableCollection<LessonViewModel>(lessonHours.Select(x=>
                new LessonViewModel(x)));
            Day = day;
            _events = events;
        }

        public BindableCollection<LessonViewModel> LessonHours
        {
            set
            {
                _lessonHours = value; 
                NotifyOfPropertyChange(()=> LessonHours);
            }
            get { return _lessonHours; }
        }

        public DayOfWeek Day
        {
            set
            {
                _day = value;
                NotifyOfPropertyChange(() => Day);
                NotifyOfPropertyChange(() => DayName);
                NotifyOfPropertyChange(() => IsCurrentDay);
            }
            get { return _day; }
        }

        public string DayName
        {
            get
            {
                var dayName = Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetDayName(Day);
                dayName = dayName.First().ToString().ToUpper() + dayName.Substring(1);
                return dayName;
            }
        }

        public bool IsCurrentDay
        {
            get { return DateTime.Now.DayOfWeek == Day; }
        }

        public void EditLesson(Lesson lesson)
        {
            _events.Publish(new GroupScheduleEvent(GroupScheduleOperations.EditLesson,
               lesson));
        }

        public void DeleteLesson(Lesson lesson)
        {
            _events.Publish(new GroupScheduleEvent(GroupScheduleOperations.DeleteLesson,
               lesson));
        }

        public void NewLesson(LessonHour lessonHour, WeekType weekType, SubGroup subGroup)
        {
            _events.Publish(new GroupScheduleEvent(GroupScheduleOperations.NewLesson, 
                new Lesson { LessonHour = lessonHour, WeekType = weekType, SubGroup = subGroup }));
        }
    }
}
