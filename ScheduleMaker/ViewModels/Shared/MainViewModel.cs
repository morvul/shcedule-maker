using System;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Helpers;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.Services;
using ScheduleMaker.ViewModels.References;
using ScheduleMaker.ViewModels.References.ComplexReferences;
using MessageBox = System.Windows.Forms.MessageBox;
using MessageBoxButton = System.Windows.Forms.MessageBoxButtons;
using Orientation = System.Windows.Controls.Orientation;

namespace ScheduleMaker.ViewModels.Shared
{
    public class MainViewModel : PropertyChangedBase,
        IHandle<ScreenEvent>, IHandle<VisibilityEvent>
    {
        private const string DefaultTitle = "����������";
        private const string AddRecordForScreen = " [���������� ������ ��� ������ \"{0}\"]";
        private const int DefaultTitleHeight = 50;
        private const int MinTitleHeight = 0;
        private readonly ISettingsService _settings;
        private readonly PrintingService _printingService;
        private readonly IEventAggregator _events;
        private BaseViewModel _currentScreenModel;
        private int _left;
        private int _top;
        private int _height;
        private int _width;
        private WindowState _state;
        private string _title;
        private int _titleHeight;
        private string _screenName;

        public MainViewModel(
            PanelViewModel panelVModel,
            TeachersViewModel teachersVModel,
            RoomsViewModel roomsVModel,
            SubjectsViewModel subjectsVModel,
            LearningPlanViewModel learningPlanVModel,
            ScheduleGeneratorViewModel scheduleGeneratorVModel,
            EditBarViewModel editBarVModel,
            GroupsViewModel groupsVModel,
            CommonScheduleViewModel commonScheduleVModel,
            SchedulesViewModel schedulesVModel, 
            FindPanelViewModel findPanelVModel,
            GroupScheduleViewModel groupScheduleVModel,
            AuditoryScheduleViewModel auditoryScheduleVModel,
            TeacherScheduleViewModel teacherScheduleVModel,
            ISettingsService settings,
            IEventAggregator events,
            SchedulesService schedulesService,
            GroupsService groupsService,
            TeachersService teachersService,
            RoomsService roomsService,
            PrintingService printingService,
            SettingsViewModel settingsVModel,
            HolidaysViewModel holidaysVModel)
        {
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.GetCultureInfoByIetfLanguageTag("ru");
            events.Subscribe(this);
            _events = events;
            _settings = settings;
            _printingService = printingService;
            HolidaysVModel = holidaysVModel;
            CommonScheduleVModel = commonScheduleVModel;
            TeacherScheduleVModel = teacherScheduleVModel;
            GroupScheduleVModel = groupScheduleVModel;
            AuditoryScheduleVModel = auditoryScheduleVModel;
            SchedulesVModel = schedulesVModel;
            LearningPlanVModel = learningPlanVModel;
            ScheduleGeneratorVModel = scheduleGeneratorVModel;
            SubjectsVModel = subjectsVModel;
            RoomsVModel = roomsVModel;
            TeachersVModel = teachersVModel;
            GroupsVModel = groupsVModel;
            PanelVModel = panelVModel;
            EditBarVModel = editBarVModel;
            EditBarVModel.Orientation = Orientation.Horizontal;
            EditBarVModel.IsPanelVisible = true;
            FindPanelVModel = findPanelVModel;
            FindPanelVModel.IsPanelVisible = true;
            SettingsVModel = settingsVModel;
            _settings.Load();
            Left = _settings.WindowLeft;
            Top = _settings.WindowTop;
            Height = _settings.WindowHeight;
            Width = _settings.WindowWidth;
            State = _settings.WindowMax ? WindowState.Maximized : WindowState.Normal;
            Schedule.CurrentSchedule = schedulesService.GetById(_settings.CurrentScheduleId);
            Group.CurrentGroup = groupsService.GetById(_settings.CurrentGroupId);
            Teacher.CurrentTeacher = teachersService.GetById(_settings.CurrentTeacherId);
            Room.CurrentRoom = roomsService.GetById(_settings.CurrentAuditoryId);
            PrevScreenEnumValue = Screens.None;
            Handle(new ScreenEvent(_settings.CurrentScreen));
        }

        ~MainViewModel()
        {
            _settings.WindowMax = State == WindowState.Maximized;
            _settings.WindowLeft = Left;
            _settings.WindowTop = Top;
            _settings.WindowWidth = Width;
            _settings.WindowHeight = Height;
            _settings.CurrentScreen = CurrentScreenEnumValue;
            _settings.CurrentScheduleId = Schedule.CurrentSchedule != null
                ? Schedule.CurrentSchedule.Id : Guid.Empty;
            _settings.CurrentGroupId = Group.CurrentGroup != null
                ? Group.CurrentGroup.Id : Guid.Empty;
            _settings.CurrentTeacherId = Teacher.CurrentTeacher != null
                ? Teacher.CurrentTeacher.Id : Guid.Empty;
            _settings.CurrentAuditoryId = Room.CurrentRoom != null
               ? Room.CurrentRoom.Id : Guid.Empty;
            _settings.Save();
        }

        #region Properties

        #region Window settings

        /// <summary>
        /// Shift position of window from left.
        /// </summary>
        public int Left
        {
            get { return _left; }
            set
            {
                if (_left == value)
                    return;
                _left = value;
                NotifyOfPropertyChange(() => Left);
            }
        }

        /// <summary>
        /// Shift position of window from top.
        /// </summary>
        public int Top
        {
            get { return _top; }
            set
            {
                if (_top == value)
                    return;
                _top = value;
                NotifyOfPropertyChange(() => Top);
            }
        }

        /// <summary>
        /// Window hieght.
        /// </summary>
        public int Height
        {
            get { return _height; }
            set
            {
                if (_height == value)
                    return;
                _height = value;
                NotifyOfPropertyChange(() => Height);
            }
        }

        /// <summary>
        /// Window width.
        /// </summary>
        public int Width
        {
            get { return _width; }
            set
            {
                if (_width == value)
                    return;
                _width = value;
                NotifyOfPropertyChange(() => Width);
            }
        }

        /// <summary>
        /// Window state.
        /// </summary>
        public WindowState State
        {
            get { return _state; }
            set
            {
                if (_state == value)
                    return;
                _state = value;
                NotifyOfPropertyChange(() => State);
            }
        }

        /// <summary>
        /// Gets or sets the window title.
        /// </summary>
        public string Title
        {
            get
            {
                return DefaultTitle + (string.IsNullOrEmpty(ScreenName)
                  ? "" : " - " + ScreenName);
            }
            set
            {
                if (_title == value)
                    return;
                _title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }

        /// <summary>
        /// Gets or sets the name of the screen.
        /// </summary>
        public string ScreenName
        {
            get { return _screenName; }
            set
            {
                if (_screenName == value)
                    return;
                _screenName = value;
                NotifyOfPropertyChange(() => ScreenName);
                NotifyOfPropertyChange(() => Title);
            }
        }

        #endregion

        public CommonScheduleViewModel CommonScheduleVModel { get; private set; }

        public TeacherScheduleViewModel TeacherScheduleVModel { get; private set; }

        public GroupScheduleViewModel GroupScheduleVModel { get; private set; }

        public AuditoryScheduleViewModel AuditoryScheduleVModel { get; private set; }

        public LearningPlanViewModel LearningPlanVModel { get; private set; }

        public ScheduleGeneratorViewModel ScheduleGeneratorVModel { get; private set; }

        public SchedulesViewModel SchedulesVModel { get; private set; }

        public SubjectsViewModel SubjectsVModel { get; private set; }

        public RoomsViewModel RoomsVModel { get; private set; }

        public TeachersViewModel TeachersVModel { get; private set; }

        public GroupsViewModel GroupsVModel { get; private set; }

        public PanelViewModel PanelVModel { get; private set; }

        public EditBarViewModel EditBarVModel { get; private set; }

        public FindPanelViewModel FindPanelVModel { get; private set; }

        public SettingsViewModel SettingsVModel { get; private set; }

        public HolidaysViewModel HolidaysVModel { get; private set; }
        
        public Screens CurrentScreenEnumValue { get; private set; }

        public Screens PrevScreenEnumValue { get; private set; }

        public BaseViewModel CurrentScreenVModel
        {
            get { return _currentScreenModel; }
            private set
            {
                if (_currentScreenModel != value)
                {
                    _currentScreenModel = value;
                    TitleHeight = value != null ? DefaultTitleHeight : MinTitleHeight;
                    NotifyOfPropertyChange(() => CurrentScreenVModel);
                }
            }
        }



        public int TitleHeight
        {
            get { return _titleHeight; }
            set
            {
                _titleHeight = value;
                NotifyOfPropertyChange(() => TitleHeight);
            }
        }

        #endregion

        #region Methods

        public void Handle(ScreenEvent message)
        {

            if (message.Operation == ScreenOperations.GoToPrevious
                || CurrentScreenEnumValue == Screens.Settings && message.Screen == Screens.Settings)
            {
                if (PrevScreenEnumValue == Screens.Settings)
                {
                    PrevScreenEnumValue = Screens.None;
                }

                CurrentScreenEnumValue = PrevScreenEnumValue;
            }
            else
            {
                if (message.Operation == ScreenOperations.UpdateTitle)
                {
                    ScreenName = message.Screen.Description();
                    return;
                }

                if (message.Sender != Screens.None && message.Sender != CurrentScreenEnumValue)
                    return;

                PrevScreenEnumValue = CurrentScreenEnumValue;
                CurrentScreenEnumValue = message.Screen;
            }

            var prewScreenName = ScreenName;
            ScreenName = CurrentScreenEnumValue.Description();
            switch (CurrentScreenEnumValue)
            {
                case Screens.CommonSchedule:
                    CurrentScreenVModel = CommonScheduleVModel;
                    _events.Publish(
                        new ReferenceBarsActivityEvent(CommonScheduleVModel.LessonsVModel));
                    EditBarVModel.IsPanelActive = false;
                    FindPanelVModel.IsPanelActive = false;
                    break;
                case Screens.TeacherSchedule:
                    CurrentScreenVModel = TeacherScheduleVModel;
                    _events.Publish(
                        new ReferenceBarsActivityEvent(TeacherScheduleVModel.LessonsVModel));
                    EditBarVModel.IsPanelActive = false;
                    FindPanelVModel.IsPanelActive = false;
                    break;
                case Screens.GroupSchedule:
                    CurrentScreenVModel = GroupScheduleVModel;
                    _events.Publish(
                        new ReferenceBarsActivityEvent(null));
                    EditBarVModel.IsPanelActive = false;
                    FindPanelVModel.IsPanelActive = false;
                    break;
                case Screens.AuditorySchedule:
                    CurrentScreenVModel = AuditoryScheduleVModel;
                    _events.Publish(
                        new ReferenceBarsActivityEvent(AuditoryScheduleVModel.LessonsVModel));
                    EditBarVModel.IsPanelActive = false;
                    FindPanelVModel.IsPanelActive = false;
                    break;
                case Screens.SchedulesReference:
                    CurrentScreenVModel = SchedulesVModel;
                    if (message.Operation == ScreenOperations.NewRecord)
                    {
                        SchedulesVModel.New(PrevScreenEnumValue);
                    }

                    _events.Publish(new ReferenceBarsActivityEvent(SchedulesVModel));
                    break;
                case Screens.TeachersReference:
                    CurrentScreenVModel = TeachersVModel;
                    if (message.Operation == ScreenOperations.NewRecord)
                    {
                        TeachersVModel.New(PrevScreenEnumValue, message.Details);
                    }

                    _events.Publish(new ReferenceBarsActivityEvent(TeachersVModel));
                    break;
                case Screens.RoomsReference:
                    CurrentScreenVModel = RoomsVModel;
                    if (message.Operation == ScreenOperations.NewRecord)
                    {
                        RoomsVModel.New(PrevScreenEnumValue);
                    }

                    _events.Publish(new ReferenceBarsActivityEvent(RoomsVModel));
                    break;
                case Screens.SubjectsReference:
                    CurrentScreenVModel = SubjectsVModel;
                    if (message.Operation == ScreenOperations.NewRecord)
                    {
                        SubjectsVModel.New(PrevScreenEnumValue);
                    }

                    _events.Publish(new ReferenceBarsActivityEvent(SubjectsVModel));
                    break;
                case Screens.HolidayReference:
                    CurrentScreenVModel = HolidaysVModel;
                    if (message.Operation == ScreenOperations.NewRecord)
                    {
                        HolidaysVModel.New(PrevScreenEnumValue);
                    }

                    _events.Publish(new ReferenceBarsActivityEvent(HolidaysVModel));
                    break;
                case Screens.GroupsReference:
                    CurrentScreenVModel = GroupsVModel;
                    if (message.Operation == ScreenOperations.NewRecord)
                    {
                        GroupsVModel.New(PrevScreenEnumValue);
                    }

                    _events.Publish(new ReferenceBarsActivityEvent(GroupsVModel));
                    break;
                case Screens.LearningPlan:
                    CurrentScreenVModel = LearningPlanVModel;
                    _events.Publish(new ReferenceBarsActivityEvent(LearningPlanVModel));
                    EditBarVModel.IsPanelActive = false;
                    FindPanelVModel.IsPanelActive = false;
                    break;
                case Screens.ScheduleGenerator:
                    CurrentScreenVModel = ScheduleGeneratorVModel;
                    _events.Publish(new ReferenceBarsActivityEvent(null));
                    EditBarVModel.IsPanelActive = false;
                    FindPanelVModel.IsPanelActive = false;
                    break;
                case Screens.None:
                    if (message.Operation == ScreenOperations.None
                        || message.Operation == ScreenOperations.GoToPrevious)
                    {
                        CurrentScreenVModel = null;
                        _events.Publish(new ReferenceBarsActivityEvent(null));
                        ScreenName = "";
                    }

                    break;
                case Screens.Settings:
                    CurrentScreenVModel = SettingsVModel;
                    _events.Publish(new ReferenceBarsActivityEvent(null));
                    EditBarVModel.IsPanelActive = false;
                    FindPanelVModel.IsPanelActive = false;
                    break;
                case Screens.CommonSchedulePrinting:
                    CurrentScreenEnumValue = PrevScreenEnumValue;
                    ScreenName = prewScreenName;
                    if (Schedule.CurrentSchedule == null)
                    {
                        MessageBox.Show("���������� ������� ����������",
                            "������ ���������", MessageBoxButton.OK, MessageBoxIcon.Warning);
                        if (CurrentScreenEnumValue != Screens.CommonSchedule)
                        {
                            Handle(new ScreenEvent(Screens.CommonSchedule));
                        }
                    }
                    else
                    {
                        _printingService.GenereteCommonSchedule(_settings.CommonScheduleTemplatePath,
                            _settings.ResultDir,
                            Schedule.CurrentSchedule);
                    }
                    break;
                case Screens.GroupSchedulePrinting:
                    CurrentScreenEnumValue = PrevScreenEnumValue;
                    ScreenName = prewScreenName;
                    if (Schedule.CurrentSchedule == null)
                    {
                        MessageBox.Show("���������� ������� ����������",
                            "������ ���������", MessageBoxButton.OK, MessageBoxIcon.Warning);
                        if (CurrentScreenEnumValue != Screens.CommonSchedule)
                        {
                            Handle(new ScreenEvent(Screens.CommonSchedule));
                        }
                    }
                    else if (Room.CurrentRoom == null)
                    {
                        MessageBox.Show("���������� ������� ���������",
                            "������ ���������", MessageBoxButton.OK, MessageBoxIcon.Warning);
                        if (CurrentScreenEnumValue != Screens.AuditorySchedule)
                        {
                            Handle(new ScreenEvent(Screens.AuditorySchedule));
                        }
                    }
                    else
                    {
                        _printingService.GenereteAuditorySchedule(_settings.ResultDir,
                            Schedule.CurrentSchedule, Room.CurrentRoom);
                    }
                    break;
            }

            if (message.Operation == ScreenOperations.NewRecord)
            {
                ScreenName += string.Format(AddRecordForScreen,
                    PrevScreenEnumValue.Description());
            }

            if (CurrentScreenVModel != null &&
                message.Operation != ScreenOperations.DenyUpdateData)
            {
                CurrentScreenVModel.UpdateData();
            }
        }

        public void Handle(VisibilityEvent message)
        {
            if (message.Sender == typeof(PanelViewModel))
            {
                TitleHeight = message.IsVisible ? MinTitleHeight : DefaultTitleHeight;
                EditBarVModel.IsPanelVisible = !message.IsVisible;
                FindPanelVModel.IsPanelVisible = !message.IsVisible;
            }
        }

        #endregion

    }
}