using System.Windows.Controls;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Services;

namespace ScheduleMaker.ViewModels.Shared
{
    public class PanelViewModel : PropertyChangedBase
    {
        private const int DefaultWidth = 180;
        private const int MinWidth = 45;
        private readonly ISettingsService _settings;
        private readonly IEventAggregator _events;
        private bool _isPanelShow;
        private int _width;
        private bool _isReferencesExpanded;
        private bool _isActionsExpanded;
        private bool _isPrintingExpanded;
        private bool _isSchedulesExpanded;

        public PanelViewModel(IEventAggregator events, ISettingsService settings, 
            EditBarViewModel editBarVModel)
        {
            events.Subscribe(this);
            _events = events;
            _settings = settings;
            IsPanelVisible = true;
            IsActionsExpanded = _settings.IsActionsExpanded;
            IsReferencesExpanded = _settings.IsReferencesExpanded;
            IsPrintingExpanded = _settings.IsPrintingExpanded;
            IsSchedulesExpanded = _settings.IsSchedulesExpanded;
            EditBarVModel = editBarVModel;
            EditBarVModel.Orientation = Orientation.Vertical;
            EditBarVModel.IsPanelVisible = false;
        }

        ~PanelViewModel()
        {
            _settings.IsPrintingExpanded = IsPrintingExpanded;
            _settings.IsSchedulesExpanded = IsSchedulesExpanded;
            _settings.IsActionsExpanded = IsActionsExpanded;
            _settings.IsReferencesExpanded = IsReferencesExpanded;
            _settings.Save();
        }

        #region Properties

        public bool IsSchedulesExpanded
        {
            get { return _isSchedulesExpanded; }
            set
            {
                _isSchedulesExpanded = value;
                NotifyOfPropertyChange(() => IsSchedulesExpanded);
            }
        }

        public bool IsReferencesExpanded
        {
            get { return _isReferencesExpanded; }
            set
            {
                _isReferencesExpanded = value;
                NotifyOfPropertyChange(() => IsReferencesExpanded);
            }
        }

        public bool IsPrintingExpanded
        {
            get { return _isPrintingExpanded; }
            set
            {
                _isPrintingExpanded = value;
                NotifyOfPropertyChange(() => IsPrintingExpanded);
            }
        }

        public bool IsActionsExpanded
        {
            get { return _isActionsExpanded; }
            set
            {
                _isActionsExpanded = value;
                NotifyOfPropertyChange(() => IsActionsExpanded);
            }
        }

        public bool IsPanelVisible
        {
            get { return _isPanelShow; }
            set
            {
                _isPanelShow = value;
                Width = value ? DefaultWidth : MinWidth;
                NotifyOfPropertyChange(() => IsShowPanelButtonVisible);
                NotifyOfPropertyChange(() => IsPanelVisible);
            }
        }

        public bool IsShowPanelButtonVisible
        {
            get { return !_isPanelShow; }
            set
            {
                _isPanelShow = !value;
                Width = value ? DefaultWidth : MinWidth;
                NotifyOfPropertyChange(() => IsPanelVisible);
                NotifyOfPropertyChange(() => IsShowPanelButtonVisible);
            }
        }

        public int Width
        {
            get { return _width; }
            set
            {
                _width = value;
                NotifyOfPropertyChange(() => Width);
            }
        }

        public EditBarViewModel EditBarVModel { get; private set; }

        #endregion


        #region Methods

        public void CommonSchedule()
        {
            _events.Publish(new ScreenEvent(Screens.CommonSchedule));
        }

        public void TeacherSchedule()
        {
            _events.Publish(new ScreenEvent(Screens.TeacherSchedule));
        }

        public void GroupSchedule()
        {
            _events.Publish(new ScreenEvent(Screens.GroupSchedule));
        }

        public void AuditorySchedule()
        {
            _events.Publish(new ScreenEvent(Screens.AuditorySchedule));
        }

        public void Schedules()
        {
            _events.Publish(new ScreenEvent(Screens.SchedulesReference));
        }

        public void Teachers()
        {
            _events.Publish(new ScreenEvent(Screens.TeachersReference));
        }

        public void Rooms()
        {
            _events.Publish(new ScreenEvent(Screens.RoomsReference));
        }

        public void Subjects()
        {
            _events.Publish(new ScreenEvent(Screens.SubjectsReference));
        }

        public void Groups()
        {
            _events.Publish(new ScreenEvent(Screens.GroupsReference));
        }

        public void LearningPlan()
        {
            _events.Publish(new ScreenEvent(Screens.LearningPlan));
        }

        public void ScheduleGenerator()
        {
            _events.Publish(new ScreenEvent(Screens.ScheduleGenerator));
        }

        public void Holidays()
        {
            _events.Publish(new ScreenEvent(Screens.HolidayReference));
        }

        public void Settings()
        {
            _events.Publish(new ScreenEvent(Screens.Settings));
        }

        public void HidePanel()
        {
            IsPanelVisible = false;
            EditBarVModel.IsPanelVisible = true;
            _events.Publish(new VisibilityEvent(true, typeof(PanelViewModel)));
        }

        public void ShowPanel()
        {
            IsPanelVisible = true;
            EditBarVModel.IsPanelVisible = false;
            _events.Publish(new VisibilityEvent(false, typeof(PanelViewModel)));
        }

        public void CommonSchedulePrinting()
        {
            _events.Publish(new ScreenEvent(Screens.CommonSchedulePrinting));
        }

        public void GroupSchedulePrinting()
        {
            _events.Publish(new ScreenEvent(Screens.GroupSchedulePrinting));
        }
        
        #endregion
    }
}