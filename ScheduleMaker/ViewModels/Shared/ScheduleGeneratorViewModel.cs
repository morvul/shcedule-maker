﻿using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using ScheduleMaker.Models;

namespace ScheduleMaker.ViewModels.Shared
{
    public class ScheduleGeneratorViewModel : BaseViewModel, IHandle<UpdateChildsEvent>
    {
        private readonly SchedulesService _schedulesService;
        private readonly LessonHoursService _lessonHoursService;
        private readonly ScheduleGeneratorService _scheduleGeneratorService;
        private readonly LearningPlansService _learningPlansService;
        private readonly IEventAggregator _events;
        private ObservableCollection<Schedule> _schedules;
        private BindableCollection<WeekDay> _weekDays;
        private WeekDay _selectedWeekDay;
        private bool _isAnyLessonHour;
        private string _scheduleSize;
        private string _learningPlanSize;
        private string _learningPlanNotAssigned;

        public ScheduleGeneratorViewModel(
            SchedulesService schedulesService,
            LessonHoursService lessonHoursService,
            ScheduleGeneratorService scheduleGeneratorService,
            LessonHourEditorViewModel lessonHourVModel,
            LearningPlansService learningPlansService,
            IEventAggregator events)
        {
            _schedulesService = schedulesService;
            _lessonHoursService = lessonHoursService;
            _scheduleGeneratorService = scheduleGeneratorService;
            LessonHourVModel = lessonHourVModel;
            _learningPlansService = learningPlansService;
            _events = events;
            _events.Subscribe(this);
            IsNewCopy = true;
            IsUseExistingHours = true;
            IsUseRaiting = true;
            IsAnyLessonHour = true;
            IsStreamLections = true;
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            IsEditMode = true;
            Warnings.CollectionChanged += WarningsChanged;

            NotifyOfPropertyChange(nameof(WeekDays));
        }

        public override void UpdateData()
        {
            Schedules = new ObservableCollection<Schedule>(_schedulesService.GetAll()
                .OrderByDescending(x => x.Name));
            var newItem = new Schedule
            {
                Id = Guid.Empty,
                Name = "Создать новое"
            };
            Schedules.Insert(0, newItem);
            if (SelectedSchedule?.Id != Guid.Empty)
            {
                SelectedSchedule = _schedulesService.GetById(SelectedSchedule?.Id ?? Guid.Empty);
                UpdateWeekDays();
            }

            UpdateLearningPlanDetails();
        }

        private void UpdateLearningPlanDetails()
        {
            var maxLearningPlanSize = _learningPlansService.GetMaxPlannedLessonsCount(Schedule.CurrentSchedule);
            LearningPlanSize = $"Максимальное запланированное число часов для группы: {maxLearningPlanSize}";
            var notAssignedCount = _learningPlansService.GetUnassignedLessonsTotal(Schedule.CurrentSchedule);
            LearningPlanNotAssigned = $"Число неназначенных занятий: {notAssignedCount}";
        }

        public ObservableCollection<Schedule> Schedules
        {
            set
            {
                _schedules = value;
                NotifyOfPropertyChange(() => Schedules);
            }
            get { return _schedules; }
        }

        public Screens CurrentField { get; set; }

        public bool IsNewCopy { get; set; }

        public bool IsUseExistingHours { get; set; }

        public bool IsUseRaiting { get; set; }

        public bool IsStreamLections { get; set; }
        

        public string ScheduleSize
        {
            set
            {
                _scheduleSize = value;
                NotifyOfPropertyChange(() => ScheduleSize);
            }
            get { return _scheduleSize; }
        }

        public string LearningPlanSize
        {
            set
            {
                _learningPlanSize = value;
                NotifyOfPropertyChange(() => LearningPlanSize);
            }
            get { return _learningPlanSize; }
        }

        public string LearningPlanNotAssigned
        {
            set
            {
                _learningPlanNotAssigned = value;
                NotifyOfPropertyChange(() => LearningPlanNotAssigned);
            }
            get { return _learningPlanNotAssigned; }
        }

        public BindableCollection<WeekDay> WeekDays
        {
            set
            {
                _weekDays = value;
                NotifyOfPropertyChange(() => WeekDays);
            }
            get { return _weekDays; }
        }

        public LessonHourEditorViewModel LessonHourVModel { get; private set; }

        public bool IsAnyLessonHour
        {
            set
            {
                _isAnyLessonHour = value;
                NotifyOfPropertyChange(() => IsAnyLessonHour);
            }
            get { return _isAnyLessonHour; }
        }


        public LessonHour SelectedLessonHour
        {
            get { return LessonHourVModel.CurrentItem; }
            set
            {
                LessonHourVModel.CurrentItem = value;
                NotifyOfPropertyChange(() => SelectedLessonHour);
            }
        }

        public WeekDay SelectedWeekDay
        {
            get { return _selectedWeekDay; }
            set
            {
                _selectedWeekDay = value;
                if (value != null)
                {
                    var currentLesson = _selectedWeekDay.LessonHours.FirstOrDefault(x =>
                        x.StartTime.TimeOfDay == SelectedLessonHour.StartTime.TimeOfDay);
                    if (LessonHourVModel.CurrentItem.Id == Guid.Empty || currentLesson == null)
                    {
                        TimeSpan minTimeToStart = TimeSpan.MaxValue;
                        LessonHour mostPossibleLesson = null;
                        foreach (var hour in _selectedWeekDay.LessonHours)
                        {
                            if (hour.StartTime.TimeOfDay <= DateTime.Now.TimeOfDay &&
                                hour.StartTime.TimeOfDay + hour.Schedule.LessonDuration
                                >= DateTime.Now.TimeOfDay)
                            {
                                mostPossibleLesson = hour;
                                break;
                            }

                            var def = hour.StartTime.TimeOfDay - DateTime.Now.TimeOfDay;
                            if (def.Duration() < minTimeToStart)
                            {
                                mostPossibleLesson = hour;
                                minTimeToStart = def.Duration();
                            }
                        }

                        SelectedLessonHour = mostPossibleLesson;
                    }
                    else
                    {
                        SelectedLessonHour = currentLesson;
                    }

                    LessonHourVModel.Day = value.Day;
                }

                NotifyOfPropertyChange(() => SelectedWeekDay);
            }
        }

        public bool CanGenerateCommand => IsAnyLessonHour;

        public void DeleteLessonHour(Guid id)
        {
            LessonHour currentHour = SelectedLessonHour;
            LessonHourVModel.SelectRecord(id);
            if (LessonHourVModel.Delete())
            {
                if (id != currentHour.Id)
                {
                    SelectedLessonHour = currentHour;
                }

                UpdateWeekDays();
                UpdateLearningPlanDetails();
            }
        }

        public void EditLessonHour(Guid id)
        {
            LessonHourVModel.SelectRecord(id);
            LessonHourVModel.Edit();
        }

        public Schedule SelectedSchedule
        {
            get { return Schedule.CurrentSchedule; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.SchedulesReference;
                        _events.Publish(new ScreenEvent(Screens.SchedulesReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                Schedule.CurrentSchedule = value;
                UpdateWeekDays();
                UpdateLearningPlanDetails();
                NotifyOfPropertyChange(() => SelectedSchedule);
                NotifyOfPropertyChange(() => Schedules);
                NotifyOfPropertyChange(() => CanGenerateCommand);
            }
        }

        public void Handle(UpdateChildsEvent message)
        {
            switch (message.RecordCreator)
            {
                case Screens.SchedulesReference:
                    CurrentField = Screens.SchedulesReference;
                    UpdateSchedules();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedSchedule = Schedules.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    
                    break;
                case Screens.CommonSchedule:
                    UpdateWeekDays();
                    UpdateLearningPlanDetails();
                    NotifyOfPropertyChange(() => CanGenerateCommand);
                    break;
                case Screens.LessonHours:
                    UpdateData();
                    break;
            }
        }

        private void UpdateSchedules()
        {
            Guid selectedScheduleId = SelectedSchedule != null
                ? SelectedSchedule.Id : Guid.Empty;
            Schedules = new BindableCollection<Schedule>(_schedulesService.GetAll()
                .OrderByDescending(x => x.Name));
            var newItem = new Schedule
            {
                Id = Guid.Empty,
                Name = "Создать новое"
            };
            Schedules.Insert(0, newItem);
            if (selectedScheduleId != Guid.Empty)
            {
                SelectedSchedule = _schedulesService.GetById(selectedScheduleId);
            }
        }

        private void UpdateWeekDays()
        {
            IsEditMode = true;
            var selectedDay = SelectedWeekDay;
            LessonHour selectedLessonHour = null;
            if (SelectedSchedule != null)
            {
                selectedLessonHour = SelectedLessonHour;
                SelectedSchedule.LessonHours = _lessonHoursService.GetAll()
                    .Where(x => x.Schedule.Id == SelectedSchedule.Id).ToList();
                LessonHourVModel.Schedule = SelectedSchedule;
            }

            WeekDays = new BindableCollection<WeekDay>(
                _schedulesService.GetWeekDays(SelectedSchedule));
            SelectedWeekDay = selectedDay == null
                ? WeekDays.FirstOrDefault(weekDay => weekDay.Day == DateTime.Now.DayOfWeek)
                : WeekDays.FirstOrDefault(weekDay => weekDay.Day == selectedDay.Day);

            if (selectedLessonHour != null)
            {
                var currentLesson = SelectedSchedule.LessonHours.FirstOrDefault(x =>
                    x.Id == selectedLessonHour.Id);
                if (currentLesson != null)
                {
                    SelectedLessonHour = currentLesson;
                }
            }

            IsEditMode = false;
            var scheduleSize = _lessonHoursService.GetScheduleSize(Schedule.CurrentSchedule);
            ScheduleSize = $"Число доступных часов: {scheduleSize}";
        }

        private void WarningsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotifyOfPropertyChange(nameof(CanGenerateCommand));
        }

        public override string GetErrors(string propertyName)
        {
            switch (propertyName)
            {
                case nameof(WeekDays):
                    if (SelectedSchedule != null)
                    {
                        IsAnyLessonHour = WeekDays.Any(x => x.LessonHours.Any());
                        if (!IsAnyLessonHour)
                        {

                            return "Необходимо назначить как минимум один учебный час";
                        }
                    }

                    break;
            }

            return null;
        }

        public void GenerateCommand()
        {
            var currentScheduleId = Schedule.CurrentSchedule.Id;
            var isSuccess = _scheduleGeneratorService.GenerateSchedule(ref currentScheduleId,
                IsNewCopy, IsUseExistingHours, IsUseRaiting, IsStreamLections, out var erorrMessages);
            var notAssignedCount = _learningPlansService.GetUnassignedLessonsTotal(Schedule.CurrentSchedule);
            LearningPlanNotAssigned = $"Число неназначенных занятий: {notAssignedCount}";
            if (isSuccess)
            {
                Warnings.Clear();
                MessageBox.Show("Расписание сгенерировано успешно!", "Генерация расписания...",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
                Schedule.CurrentSchedule = _schedulesService.GetById(currentScheduleId);
                _events.Publish(new ScreenEvent(Screens.CommonSchedule));
            }
            else
            {
                SetWarnings(erorrMessages, MessageType.Error);
                MessageBox.Show("Генерация прервана.", "Генерация расписания...", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            }
        }
    }
}
