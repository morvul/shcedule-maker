﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Caliburn.Micro;
using ScheduleMaker.Models;

namespace ScheduleMaker.ViewModels.Shared
{
    public abstract class BaseViewModel : Screen, IDataErrorInfo
    {
        #region Fields

        private bool _isEditMode;

        #endregion

        #region Constructors

        public BaseViewModel()
        {
            IsEditMode = false;
            Warnings = new BindableCollection<Error>();
        }

        #endregion

        #region Properties

        public virtual bool IsEditMode
        {
            get { return _isEditMode; }
            protected set
            {
                if (IsEditMode == value) return;
                _isEditMode = value;
                NotifyOfPropertyChange(() => IsReadonly);
                NotifyOfPropertyChange(() => IsEditMode);
            }
        }

        public bool IsReadonly
        {
            get { return !IsEditMode; }
        }

        public virtual bool CanSave { get { return true; } }

        #endregion

        #region Methods

        /// <summary>
        /// Запрос предупреждений для свойства
        /// </summary>
        /// <param name="propertyName">Имя проверяемого свойства</param>
        /// <returns>Список предупреждений</returns>
        public virtual IEnumerable<Error> GetWarnings(string propertyName)
        {
            return new Error[0];
        }

        /// <summary>
        /// Запрос ошибок для свойства
        /// </summary>
        /// <param name="propertyName">Имя проверяемого свойства</param>
        /// <returns>Первая ошибка для поля</returns>
        public virtual string GetErrors(string propertyName)
        {
            return null;
        }

        public abstract void UpdateData();

        #endregion

        #region Errors handling

        public string this[string property]
        {
            get
            {
                string error = String.Empty;
                if (!IsEditMode)
                {
                    if (Warnings.Any())
                    {
                        Warnings.Clear();
                    }

                    return error;
                }

                #region Добавление/удаление сообщения об ошибке для поля

                error = GetErrors(property);
                var existsRrrorItem = Warnings.FirstOrDefault(x => 
                    x.Type == MessageType.Error && x.Fields.Contains(property));

                if (existsRrrorItem != null && (string.IsNullOrEmpty(error) || error != existsRrrorItem.Message))
                {
                    Warnings.Remove(existsRrrorItem);
                    existsRrrorItem = null;
                    NotifyOfPropertyChange(() => Error);
                }

                if (!string.IsNullOrEmpty(error) && existsRrrorItem == null)
                {
                    Warnings.Add(new Error(MessageType.Error, error, property));
                    NotifyOfPropertyChange(() => Error);
                }

                #endregion

                #region Добавление/удаление предупреждений для поля

                var warningsList = GetWarnings(property);
                var existWarnings = Warnings.Where(x => 
                    x.Type == MessageType.Warning && x.Fields.Contains(property)).ToList();
                var recordsForDelete = existWarnings.ToList();

                foreach (var warning in warningsList)
                {
                    if (existWarnings.All(x => x.Message != warning.Message ))
                    {
                        Warnings.Add(warning);
                        continue;
                    }

                    recordsForDelete.Remove(recordsForDelete.FirstOrDefault(x => x.Code == warning.Code));
                }

                foreach (var record in recordsForDelete)
                {
                    var warning = Warnings.FirstOrDefault(x => x.Code == record.Code);
                    Warnings.Remove(warning);
                }

                #endregion

                NotifyOfPropertyChange(() => CanSave);

                return error;
            }
        }

        protected void SetWarnings(string[] erorrMessages, MessageType type = MessageType.Warning)
        {
            Warnings.Clear();
            foreach (var erorrMessage in erorrMessages)
            {
                Warnings.Add(new Error(type, erorrMessage));
            }
        }

        public string Error
        {
            get
            {
                if (Warnings.All(x => x.Type != MessageType.Error)) return null;
                var errorList = new StringBuilder();
                using (var errorMessages = Warnings.GetEnumerator())
                {
                    while (errorMessages.MoveNext())
                    {
                        if (errorMessages.Current?.Type == MessageType.Error)
                        {
                            errorList.AppendLine("‣  " + errorMessages.Current.Message);
                        }
                    }
                }

                return errorList.ToString();
            }
        }

        public ObservableCollection<Error> Warnings { get; }

        #endregion

    }
}