﻿using System.ComponentModel;

namespace ScheduleMaker.ViewModels
{
    public enum Screens
    {
        [Description("")]
        None,
       
        [Description("Cправочник расписаний")]
        SchedulesReference,

        [Description("Cправочник преподавателей")]
        TeachersReference,

        [Description("Cправочник аудиторий")]
        RoomsReference,

        [Description("Cправочник предметов")]
        SubjectsReference,

        [Description("Справочник групп")]
        GroupsReference,

        [Description("Общее расписание")]
        CommonSchedule,

        [Description("Расписание для группы")]
        GroupSchedule,

        [Description("Расписание для преподавателя")]
        TeacherSchedule,

        [Description("Расписание для предмета")]
        SubjectSchedule,

        [Description("Учебный план")]
        LearningPlan,

        [Description("Генератор расписания")]
        ScheduleGenerator,

        [Description("Учебные часы")]
        LessonHours,

        [Description("Расписание для аудитории")]
        AuditorySchedule,

        CommonSchedulePrinting,

        [Description("Настройки")]
        Settings,

        [Description("Печать расписания для аудитории")]
        GroupSchedulePrinting,

        [Description("Праздничные дни")]
        HolidayReference
    }
}