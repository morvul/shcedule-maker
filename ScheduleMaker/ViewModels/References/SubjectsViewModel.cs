using Caliburn.Micro;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;

namespace ScheduleMaker.ViewModels.References
{
    public class SubjectsViewModel : BaseReferenceViewModel<Subject>
    {
        public SubjectsViewModel(SubjectsService subjectsService, IEventAggregator events)
            : base(subjectsService, events)
        {
        }

        public string Name
        {
            set
            {
                CurrentItem.Name = value;
                NotifyOfPropertyChange(() => Name);
            }
            get { return CurrentItem.Name; }
        }

        protected override Screens CurrentScreen { get { return Screens.SubjectsReference; } }

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => Name);
        }

        public override bool CanSave
        {
            get { return (!string.IsNullOrEmpty(Name) && base.CanSave); }
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case "Name":
                    if (Name.Trim().Length == 0)
                    {
                        return "���� \"�������\" ������ ���� ���������";
                    }

                    if (ReferenceService.Exists(x => x.Name == Name && x.Id != CurrentItem.Id))
                    {
                        return "������� c ����� ������ ��� ����������";
                    }

                    break;
            }

            return null;
        }
    }
}