﻿namespace ScheduleMaker.ViewModels.References
{
    public interface IReference
    {
        bool CanDelete { get; }

        bool CanEdit { get; }

        bool CanNew { get; }

        bool CanFilter { get; }

        string FilterText { get; }

        bool Delete();

        void Edit();

        void New(Screens newRecordCreator = Screens.None, 
            object newRecordCreatorDetails = null);

        void Filter(string filterText);
    }
}