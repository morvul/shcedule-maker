using System;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;

namespace ScheduleMaker.ViewModels.References
{
    public class SchedulesViewModel : BaseReferenceViewModel<Schedule>
    {
        public SchedulesViewModel(SchedulesService schedulesService, IEventAggregator events)
            : base(schedulesService, events)
        { }

        public string Name
        {
            set
            {
                CurrentItem.Name = value;
                NotifyOfPropertyChange(() => Name);
            }
            get { return CurrentItem.Name; }
        }

        public DateTime LessonsStart
        {
            set
            {
                CurrentItem.LessonsStart = value;
                NotifyOfPropertyChange(() => LessonsStart);
            }
            get { return CurrentItem.LessonsStart; }
        }

        public DateTime FirstDay
        {
            set
            {
                CurrentItem.FirstDay = value;
                NotifyOfPropertyChange(() => FirstDay);
                NotifyOfPropertyChange(() => LastDay);
            }
            get { return CurrentItem.FirstDay; }
        }

        public DateTime LastDay
        {
            set
            {
                CurrentItem.LastDay = value;
                NotifyOfPropertyChange(() => LastDay);
                NotifyOfPropertyChange(() => FirstDay);
            }
            get { return CurrentItem.LastDay; }
        }

        public TimeSpan LessonDuration
        {
            set
            {
                CurrentItem.LessonDuration = value;
                NotifyOfPropertyChange(() => LessonDuration);
            }
            get { return CurrentItem.LessonDuration; }
        }

        protected override Screens CurrentScreen
        {
            get { return Screens.SchedulesReference; }
        }

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => Name);
            NotifyOfPropertyChange(() => LessonsStart);
            NotifyOfPropertyChange(() => LessonDuration);
            NotifyOfPropertyChange(() => FirstDay);
            NotifyOfPropertyChange(() => LastDay);
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case nameof(Name):
                    if (Name != null && Name.Trim().Length == 0)
                    {
                        return "���� \"����������\" ������ ���� ���������";
                    }

                    if (ReferenceService.Exists(x => x.Name == Name && x.Id != CurrentItem.Id))
                    {
                        return "���������� c ����� ������ ��� ����������";
                    }

                    break;
                case nameof(LessonDuration):
                    if (LessonDuration <= TimeSpan.Zero)
                    {
                        return "������������ ������� ������ ���� ����������";
                    }
                    break;
                case nameof(FirstDay):
                    if (FirstDay > LastDay)
                    {
                        return "������ ���� �� ����� ���� ����� ����������";
                    }

                    break;
                case nameof(LastDay):
                    if (FirstDay > LastDay)
                    {
                        return "��������� ���� �� ����� ���� ������ �������";
                    }

                    break;
            }

            return null;
        }

        public void GoToShcedule()
        {
            if (CurrentItem.Id != Guid.Empty)
            {
                Schedule.CurrentSchedule = CurrentItem;
                Events.Publish(new ScreenEvent(Screens.CommonSchedule));
            }
        }
    }
}