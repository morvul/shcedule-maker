﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.Models;

namespace ScheduleMaker.ViewModels.References
{
    public class GroupsViewModel : BaseReferenceViewModel<Group>
    {
        public GroupsViewModel(GroupsService groupsService, IEventAggregator events)
            : base(groupsService, events)
        { }

        #region Properties

        public override bool CanSave
        {
            get { return (!string.IsNullOrEmpty(Name) && base.CanSave); }
        }

        public string Name
        {
            set
            {
                CurrentItem.Name = value;
                NotifyOfPropertyChange(() => Name);
            }
            get { return CurrentItem.Name; }
        }

        public int MembersCount
        {
            set
            {
                CurrentItem.MembersCount = value;
                NotifyOfPropertyChange(() => MembersCount);
            }
            get { return CurrentItem.MembersCount; }
        }

        public int? StartLern
        {
            set
            {
                if (value.HasValue)
                {
                    try
                    {
                        CurrentItem.StartLern =
                            new DateTime(value.Value, Group.CourseStartMonth, Group.CourseStartDay);
                        CurrentItem.EndLern =
                            CurrentItem.StartLern.AddYears(Group.DefaultCourseCount);
                    }
                    catch (Exception)
                    {
                        StartLern = null;
                    }
                }

                NotifyOfPropertyChange(() => Name);
                NotifyOfPropertyChange(() => StartLern);
                NotifyOfPropertyChange(() => EndLern);
            }
            get { return CurrentItem.StartLern.Year; }
        }

        public int? EndLern
        {
            set
            {
                if (value.HasValue)
                {
                    CurrentItem.EndLern = 
                        new DateTime(value.Value, Group.CourseStartMonth, Group.CourseStartDay);
                }

                NotifyOfPropertyChange(() => StartLern);
                NotifyOfPropertyChange(() => EndLern);
            }
            get { return CurrentItem.EndLern.Year; }
        }

        public int Raiting
        {
            set
            {
                CurrentItem.Raiting = value;
                NotifyOfPropertyChange(() => Raiting);
            }
            get { return CurrentItem.Raiting; }
        }

        #endregion

        #region Methods

        protected override Screens CurrentScreen { get { return Screens.GroupsReference; } }

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => Name);
            NotifyOfPropertyChange(() => StartLern);
            NotifyOfPropertyChange(() => EndLern);
            NotifyOfPropertyChange(() => MembersCount);
            NotifyOfPropertyChange(() => Raiting);
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case "Name":
                    if (Name.Trim().Length == 0)
                    {
                        return "Поле \"Группа\" должно быть заполнено";
                    }

                    if (!char.IsLetter(Name.First()))
                    {
                        return "Имя группы должно начинаться с буквы";
                    }

                    if (ReferenceService.Exists(x => x.Id != CurrentItem.Id &&
                        x.Name == CurrentItem.Name && 
                        x.StartLern.Year == CurrentItem.StartLern.Year))
                    {
                        return "Группа c таким именем уже существует";
                    }

                    break;
                case "MembersCount":

                    break;
                case "StartLern":
                    if (EndLern < StartLern)
                    {
                        return "Дата начала обучения не может быть больше даты окончания";
                    }

                    if (StartLern < 2000)
                    {
                        return "Дата начала обучения не может быть раньше 2000-го года";
                    }
                    break;
                case "EndLern":
                    if (EndLern < StartLern)
                    {
                        return "Дата окончания обучения не может быть меньше даты начала";
                    }
                    break;
            }

            return null;
        }

        public override IEnumerable<Error> GetWarnings(string propertyName)
        {
            var warnings = new List<Error>();
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return warnings;
            }

            
            if (Schedule.CurrentSchedule!=null &&
                (
                    (propertyName == "StartLern" || propertyName == "EndLern") &&
                    ((StartLern.HasValue && StartLern > Schedule.CurrentSchedule.FirstDay.Year) ||
                    (EndLern.HasValue && EndLern < Schedule.CurrentSchedule.FirstDay.Year))
                )
               )
            {
                warnings.Add(new Error(MessageType.Warning, "Данная группа не будет доступна при заполнении расписания, так как текущий год не входит в ее период обучения",
                    new[] {"StartLern", "EndLern" }, 1)
                    );
            }

            return warnings;
        }

        public void GoToLearningPlan()
        {
            if (CurrentItem.Id != Guid.Empty)
            {
                Group.CurrentGroup = CurrentItem;
                Events.Publish(new ScreenEvent(Screens.LearningPlan));
            }
        }

        #endregion

    }
}