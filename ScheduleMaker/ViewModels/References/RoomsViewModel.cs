﻿using Caliburn.Micro;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;

namespace ScheduleMaker.ViewModels.References
{
    public class RoomsViewModel : BaseReferenceViewModel<Room>
    {
        public RoomsViewModel(RoomsService roomsService, IEventAggregator events)
            : base(roomsService, events)
        {

        }

        #region Properties

        public override bool CanSave
        {
            get { return (!string.IsNullOrEmpty(Name) && base.CanSave); }
        }

        public string Name
        {
            set
            {
                CurrentItem.Name = value;
                NotifyOfPropertyChange(() => Name);
            }
            get { return CurrentItem.Name; }
        }

        public int? SeatsCount
        {
            set
            {
                CurrentItem.SeatsCount = value;
                NotifyOfPropertyChange(() => SeatsCount);
            }
            get { return CurrentItem.SeatsCount; }
        }

        public string Description
        {
            set
            {
                CurrentItem.Description = value;
                NotifyOfPropertyChange(() => Description);
            }
            get { return CurrentItem.Description; }
        }

        #endregion

        #region Methods

        protected override Screens CurrentScreen { get { return Screens.RoomsReference; } }

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => Name);
            NotifyOfPropertyChange(() => Description);
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case "Name":
                    if (Name.Trim().Length == 0)
                    {
                        return "Поле \"Аудитория\" должно быть заполнено";
                    }

                    if (ReferenceService.Exists(x => x.Name == Name && x.Id != CurrentItem.Id))
                    {
                        return "Аудитория c таким именем уже существует";
                    }

                    break;
                case "Description":

                    break;
            }

            return null;
        }

        #endregion

    }
}