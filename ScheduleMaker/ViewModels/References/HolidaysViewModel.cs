using System;
using Caliburn.Micro;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;

namespace ScheduleMaker.ViewModels.References
{
    public class HolidaysViewModel : BaseReferenceViewModel<Holiday>
    {
        public HolidaysViewModel(HolidayService holidayService, IEventAggregator events)
            : base(holidayService, events)
        { }

        public string Name
        {
            set
            {
                CurrentItem.Name = value;
                NotifyOfPropertyChange(() => Name);
            }
            get => CurrentItem.Name;
        }

        public DateTime Date
        {
            set
            {
                CurrentItem.Date = value;
                NotifyOfPropertyChange(() => Date);
            }
            get => CurrentItem.Date;
        }
        
        protected override Screens CurrentScreen => Screens.HolidayReference;

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => Name);
            NotifyOfPropertyChange(() => Date);
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case nameof(Name):
                    if (Name!=null && Name.Trim().Length == 0)
                    {
                        return "���� \"��������\" ������ ���� ���������";
                    }

                    if (ReferenceService.Exists(x => x.Name == Name && x.Id != CurrentItem.Id))
                    {
                        return "�������� c ����� ������ ��� ����������";
                    }

                    break;
                case nameof(Date):
                    if (Date == DateTime.MinValue)
                    {
                        return "���� \"����\" ������ ���� ���������";
                    }

                    break;
            }

            return null;
        }
    }
}