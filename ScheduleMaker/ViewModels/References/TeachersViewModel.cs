using Caliburn.Micro;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;

namespace ScheduleMaker.ViewModels.References
{
    public class TeachersViewModel : BaseReferenceViewModel<Teacher>
    {
        public TeachersViewModel(TeachersService teachersService, IEventAggregator events)
            : base(teachersService, events)
        {
        }

        public override bool CanSave
        {
            get { return (!string.IsNullOrEmpty(Name) && base.CanSave); }
        }

        public string Name
        {
            set
            {
                CurrentItem.Name = value;
                NotifyOfPropertyChange(() => Name);
            }
            get { return CurrentItem.Name; }
        }

        public string Rank
        {
            set
            {
                CurrentItem.Rank = value;
                NotifyOfPropertyChange(() => Rank);
            }
            get { return CurrentItem.Rank; }
        }

        public int Raiting
        {
            set
            {
                CurrentItem.Raiting = value;
                NotifyOfPropertyChange(() => Raiting);
            }
            get { return CurrentItem.Raiting; }
        }

        public string Description
        {
            set
            {
                CurrentItem.Description = value;
                NotifyOfPropertyChange(() => Description);
            }
            get { return CurrentItem.Description; }
        }

        protected override Screens CurrentScreen { get { return Screens.TeachersReference; } }

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => Name);
            NotifyOfPropertyChange(() => Rank);
            NotifyOfPropertyChange(() => Description);
            NotifyOfPropertyChange(() => Raiting);
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case "Name":
                    if (Name == null || Name.Trim().Length == 0)
                    {
                        return "���� \"���\" ������ ���� ���������";
                    }

                    if (ReferenceService.Exists(x => x.Name == Name && x.Id != CurrentItem.Id))
                    {
                        return "������������� c ����� ������ ��� ����������";
                    }

                    break;
            }

            return null;
        }
    }
}