using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Helpers;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.Models;
using ScheduleMaker.ViewModels.Shared;
using Xceed.Wpf.Toolkit.Primitives;

namespace ScheduleMaker.ViewModels.References.ComplexReferences
{
    public class LessonsViewModel : BaseReferenceViewModel<Lesson>,
        IHandle<VisibilityEvent>, IHandle<UpdateChildsEvent>
    {
        #region ����

        private LessonHour _currentLessonHour;
        private readonly GroupsService _groupsService;
        private readonly SubjectsService _subjectsService;
        private readonly TeachersService _teachersService;
        private readonly RoomsService _roomsService;
        private readonly LearningPlansService _learningPlansService;
        private readonly LessonsService _lessonsService;
        private BindableCollection<Group> _groups;
        private BindableCollection<Subject> _subjects;
        private BindableCollection<Teacher> _teachers;
        private BindableCollection<Room> _auditories;
        private BindableCollection<KeyValuePair<LessonType, string>> _lessonTypes;
        private BindableCollection<KeyValuePair<WeekType, string>> _weekTypes;
        private BindableCollection<Teacher> _selectedTeachers;
        private Teacher _presetTeacher;
        private Room _presetRoom;
        private BindableCollection<KeyValuePair<SubGroup, string>> _subGroups;

        #endregion

        #region ������������

        public LessonsViewModel(LessonsService lessonsService, IEventAggregator events,
            EditBarViewModel editBarViewModel, FindPanelViewModel findPanelVModel,
            GroupsService groupsService, SubjectsService subjectsService,
            TeachersService teachersService, RoomsService roomsService,
            LearningPlansService learningPlansService)
            : base(lessonsService, events)
        {
            FindPanelVModel = findPanelVModel;
            EditBarVModel = editBarViewModel;
            FindPanelVModel.IsPanelVisible = true;
            EditBarVModel.IsPanelVisible = true;
            events.Subscribe(this);
            _groupsService = groupsService;
            _subjectsService = subjectsService;
            _teachersService = teachersService;
            _roomsService = roomsService;
            _learningPlansService = learningPlansService;
            _lessonsService = lessonsService;
        }

        #endregion

        #region ��������

        public Screens CurrentField { get; set; }

        public EditBarViewModel EditBarVModel { get; private set; }

        public FindPanelViewModel FindPanelVModel { get; private set; }

        public LessonHour CurrentLessonHour
        {
            get { return _currentLessonHour; }
            set
            {
                _currentLessonHour = value;
                Events.Publish(new ReferncePanelsEvent());
                NotifyOfPropertyChange(() => CurrentLessonHour);
                UpdateData();
            }
        }

        #region ������

        public BindableCollection<Group> Groups
        {
            set
            {
                _groups = value;
                NotifyOfPropertyChange(() => Groups);
            }
            get { return _groups; }
        }

        public BindableCollection<Subject> Subjects
        {
            set
            {
                _subjects = value;
                NotifyOfPropertyChange(() => Subjects);
            }
            get { return _subjects; }
        }


        public BindableCollection<KeyValuePair<LessonType, string>> LessonTypes
        {
            set
            {
                _lessonTypes = value;
                NotifyOfPropertyChange(() => LessonTypes);
            }
            get { return _lessonTypes; }
        }

        public BindableCollection<Teacher> Teachers
        {
            set
            {
                _teachers = value;
                NotifyOfPropertyChange(() => Teachers);
            }
            get { return _teachers; }
        }

        public BindableCollection<Room> Auditories
        {
            set
            {
                _auditories = value;
                NotifyOfPropertyChange(() => Auditories);
            }
            get { return _auditories; }
        }

        public BindableCollection<KeyValuePair<WeekType, string>> WeekTypes
        {
            set
            {
                _weekTypes = value;
                NotifyOfPropertyChange(() => WeekTypes);
            }
            get { return _weekTypes; }
        }

        public BindableCollection<KeyValuePair<SubGroup, string>> SubGroups
        {
            set
            {
                _subGroups = value;
                NotifyOfPropertyChange(() => SubGroups);
            }
            get { return _subGroups; }
        }

        #endregion

        #region Selected items

        public Group SelectedGroup
        {
            get { return CurrentItem.Group; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.GroupsReference;
                        Events.Publish(new ScreenEvent(Screens.GroupsReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                CurrentItem.Group = value;
                NotifyOfPropertyChange(() => SelectedGroup);
            }
        }

        public Subject SelectedSubject
        {
            get { return CurrentItem.Subject; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.SubjectsReference;
                        Events.Publish(new ScreenEvent(Screens.SubjectsReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                CurrentItem.Subject = value;
                NotifyOfPropertyChange(() => SelectedSubject);
                SetTeachersFromPlan();
            }
        }

        public BindableCollection<Teacher> SelectedTeachers
        {
            set
            {
                _selectedTeachers = value;
                NotifyOfPropertyChange(() => SelectedTeachers);
            }
            get { return _selectedTeachers; }
        }

        public Room SelectedAuditory
        {
            get { return CurrentItem.Room; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.RoomsReference;
                        Events.Publish(new ScreenEvent(Screens.RoomsReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                CurrentItem.Room = value;
                NotifyOfPropertyChange(() => SelectedAuditory);
            }
        }

        public KeyValuePair<LessonType, string> SelectedLessonType
        {
            get
            {
                return new KeyValuePair<LessonType, string>(
              CurrentItem.LessonType, CurrentItem.LessonType.Description());
            }
            set
            {
                CurrentItem.LessonType = value.Key;
                NotifyOfPropertyChange(() => SelectedLessonType);
                SetTeachersFromPlan();
            }
        }

        public KeyValuePair<WeekType, string> SelectedWeekType
        {
            get
            {
                return new KeyValuePair<WeekType, string>(
              CurrentItem.WeekType, CurrentItem.WeekType.Description());
            }
            set
            {
                CurrentItem.WeekType = value.Key;
                NotifyOfPropertyChange(() => SelectedSubGroup);
                NotifyOfPropertyChange(() => SelectedWeekType);
                NotifyOfPropertyChange(() => SelectedGroup);
            }
        }

        public KeyValuePair<SubGroup, string> SelectedSubGroup
        {
            get
            {
                return new KeyValuePair<SubGroup, string>(
              CurrentItem.SubGroup, CurrentItem.SubGroup.Description());
            }
            set
            {
                CurrentItem.SubGroup = value.Key;
                NotifyOfPropertyChange(() => SelectedSubGroup);
                NotifyOfPropertyChange(() => SelectedWeekType);
                NotifyOfPropertyChange(() => SelectedGroup);
            }
        }

        public string Description
        {
            get { return CurrentItem.Description; }
            set
            {
                CurrentItem.Description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        #endregion

        public override IEnumerable<Lesson> AlternativeSourceOfRecords
        {
            get
            {
                return CurrentLessonHour == null
                    ? new List<Lesson>() : CurrentLessonHour.Lessons;
            }
        }


        public override bool CanNew
        {
            get
            {
                bool result = CurrentLessonHour != null && base.CanNew;
                if (PresetTeacher != null)
                {
                    result &= PresetTeacher.Id != Guid.Empty;
                }

                if (PresetRoom != null)
                {
                    result &= PresetRoom.Id != Guid.Empty;
                }

                return result;
            }
        }

        public override bool CanSave
        {
            get
            {
                return SelectedGroup != null && SelectedSubject != null && base.CanSave;
            }
        }

        protected override Screens CurrentScreen { get { return Screens.CommonSchedule; } }

        public Teacher PresetTeacher
        {
            get { return _presetTeacher; }
            set
            {
                _presetTeacher = value;
                NotifyOfPropertyChange(() => PresetTeacher);
            }
        }

        public Room PresetRoom
        {
            get { return _presetRoom; }
            set
            {
                _presetRoom = value;
                NotifyOfPropertyChange(() => PresetRoom);
            }
        }

        #endregion

        #region ������

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => SelectedGroup);
            NotifyOfPropertyChange(() => SelectedAuditory);
            NotifyOfPropertyChange(() => SelectedLessonType);
            NotifyOfPropertyChange(() => SelectedSubject);
            NotifyOfPropertyChange(() => SelectedWeekType);
            NotifyOfPropertyChange(() => SelectedSubGroup);
            NotifyOfPropertyChange(() => Description);
            UpdateSelectedTeachers();
        }

        private void UpdateSelectedTeachers()
        {
            SelectedTeachers = new BindableCollection<Teacher>(CurrentItem.Teachers);
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case "SelectedGroup":
                    if (SelectedGroup == null)
                    {
                        return "���������� ������� ������";
                    }

                    var existsLesson = _lessonsService.GetGetLessonsWithPreconditions(CurrentItem, 
                        SelectedSubGroup.Key, SelectedWeekType.Key).FirstOrDefault();
                    if (existsLesson != null)
                    {
                        return string.Format("��� ������ ������ �� ������� ����� ��� ��������� ������� ({0} - {1})", 
                            existsLesson.SubGroup.Description(),
                            existsLesson.Subject.Name);
                    }

                    break;
                case "SelectedSubject":
                    if (SelectedSubject == null)
                    {
                        return "���������� ������� �������";
                    }

                    break;
            }

            return null;
        }

        public override IEnumerable<Error> GetWarnings(string propertyName)
        {
            var warnings = new List<Error>();
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return warnings;
            }

            // �������� ����� � ���������
            if (SelectedGroup != null && SelectedAuditory != null &&
                (propertyName == "SelectedGroup" || propertyName == "SelectedAuditory"))
            {
                if (ReferenceService.GetAll().Any(x => x.Id != CurrentItem.Id &&
                    x.LessonHour.Id == CurrentLessonHour.Id &&
                    (x.Room != null && x.Room.Id == SelectedAuditory.Id) &&
                    x.Group.Id != SelectedGroup.Id && (x.WeekType == SelectedWeekType.Key || SelectedWeekType.Key == WeekType.Simple || x.WeekType == WeekType.Simple)))
                {
                    warnings.Add(new Error(MessageType.Warning, string.Format(
                        "�� ������ ����� � ��������� \"{0}\" ��� ��������� ������� � ������ ������", SelectedAuditory.Name),
                        new[] { "SelectedGroup", "SelectedAuditory" }, 1
                    ));
                }


                var totalStudentsCount = ReferenceService.GetAll().Where(x =>
                    x.LessonHour.Id == CurrentLessonHour.Id &&
                    x.Id != CurrentItem.Id && x.Room != null &&
                    x.Room.Id == SelectedAuditory.Id && 
                    (x.WeekType == SelectedWeekType.Key || SelectedWeekType.Key == WeekType.Simple || x.WeekType == WeekType.Simple))
                        .Select(x => (int?)x.Group.MembersCount).Sum() ?? 0;
                var overflow = totalStudentsCount +
                    CurrentItem.Group.MembersCount - SelectedAuditory.SeatsCount;
                if (overflow > 0)
                {
                    string seatWordEnd = "";
                    if (overflow % 10 == 1)
                    {
                        seatWordEnd = "�";
                    }
                    else if (overflow % 10 < 5 && overflow % 10 != 0)
                    {
                        seatWordEnd = "�";
                    }
                    warnings.Add(new Error(MessageType.Warning, string.Format(
                        "� ��������� ��������� ������������ ���� (���������� ������: {0} ����{1})", overflow, seatWordEnd),
                        new[] { "SelectedGroup", "SelectedAuditory" }, 2
                    ));
                }
            }

            // �������� �������������� � ���������
            if ((propertyName == "SelectedTeachers" || propertyName == "SelectedAuditory" || propertyName == "SelectedWeekType") &&
                SelectedTeachers.Any() && SelectedAuditory != null)
            {
                foreach (var selectedTeacher in SelectedTeachers)
                {
                    var busyman = ReferenceService.GetAll().Where(x =>
                        x.LessonHour.Id == CurrentLessonHour.Id && 
                        (x.WeekType == SelectedWeekType.Key || x.WeekType == WeekType.Simple || SelectedWeekType.Key == WeekType.Simple) && CurrentItem.Id != x.Id &&
                        x.Room != null && x.Room.Id != SelectedAuditory.Id && x.Teachers.Any(
                        y => y.Id == selectedTeacher.Id)).Select(x => new
                        {
                            Teacher = x.Teachers.FirstOrDefault(y => y.Id == selectedTeacher.Id),
                            x.Room
                        }).FirstOrDefault();
                    if (busyman != null)
                    {
                        warnings.Add(new Error(MessageType.Warning, string.Format(
                            "� ������ ����� � ������������� \"{0}\" ��� ���� ������� � ������ ��������� ({1})",
                            busyman.Teacher.Name, busyman.Room.Name),
                            new[] { "SelectedTeachers", "SelectedAuditory", "SelectedWeekType" }, 3)
                        );
                    }
                }
            }

            // �������� ����� � ���������
            if ((propertyName == "SelectedSubject" || propertyName == "SelectedLessonType") &&
                SelectedGroup != null && SelectedSubject != null)
            {
                var planItem = _learningPlansService.GetPlanItem(
                    SelectedSubject, Schedule.CurrentSchedule, SelectedGroup);
                if (planItem != null)
                {
                    double actualCount = 0;
                    switch (SelectedLessonType.Key)
                    {
                        case LessonType.Lection:
                            if (planItem.LectionsCount < actualCount)
                            {
                                warnings.Add(new Error(MessageType.Warning, string.Format(
                                    "�������� ����� ������ �� ������� �������� ��������� (�������� �� ����� - \"{0}\", ����������� �������� -\"{1}\")",
                                    planItem.LectionsCount, actualCount),
                                    new[] { "SelectedSubject", "SelectedLessonType" }, 4)
                                    );
                            }
                            break;
                        case LessonType.Practice:
                            if (planItem.PracticeCount < actualCount)
                            {
                                warnings.Add(new Error(MessageType.Warning, string.Format(
                                    "�������� ����� ������������ ������� �� ������� �������� ��������� (�������� �� ����� - \"{0}\", ����������� �������� -\"{1}\")",
                                    planItem.PracticeCount, actualCount),
                                    new[] { "SelectedSubject", "SelectedLessonType" }, 5)
                                    );
                            }
                            break;
                    }
                }
            }

            return warnings;
        }

        public override void Save()
        {
            CurrentItem.Teachers = SelectedTeachers;
            CurrentField = Screens.None;
            base.Save();
        }

        public override void Cancel()
        {
            base.Cancel();
            CurrentField = Screens.None;
        }

        public override void UpdateData()
        {
            base.UpdateData();
            UpdateGroups();
            UpdateSubjects();
            UpdateAuditories();
            UpdateTeachers();
            UpdateSelectedTeachers();
            LessonTypes = new BindableCollection<KeyValuePair<LessonType, string>>(
                EnumHelpers.GetAllItemsWithDescription<LessonType>());
            WeekTypes = new BindableCollection<KeyValuePair<WeekType, string>>(
                EnumHelpers.GetAllItemsWithDescription<WeekType>());
            SubGroups = new BindableCollection<KeyValuePair<SubGroup, string>>(
                EnumHelpers.GetAllItemsWithDescription<SubGroup>());
        }

        #region ����������� �������

        public void Handle(VisibilityEvent message)
        {
            if (message.Sender == typeof(PanelViewModel))
            {
                EditBarVModel.IsPanelVisible = !message.IsVisible;
                FindPanelVModel.IsPanelVisible = !message.IsVisible;
            }
        }

        public void Handle(UpdateChildsEvent message)
        {
            if (!IsEditMode)
            {
                return;
            }

            switch (message.RecordCreator)
            {
                case Screens.GroupsReference:
                    UpdateGroups();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedGroup = Groups.FirstOrDefault(x => x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.SubjectsReference:
                    UpdateSubjects();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedSubject = Subjects.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.RoomsReference:
                    UpdateAuditories();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedAuditory = Auditories.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.TeachersReference:
                    UpdateTeachers();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedTeachers.Add(Teachers.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId));
                    }

                    NotifyOfPropertyChange(() => SelectedTeachers);
                    break;
            }
        }

        #endregion


        #region ������ ���������� �����

        private void UpdateGroups()
        {
            Groups = new BindableCollection<Group>(
                _groupsService.GetAvailableInSchedule(CurrentLessonHour));
            var newGroup = new Group
            {
                Id = Guid.Empty,
                Name = " ������� ������"
            };
            Groups.Add(newGroup);
        }

        private void UpdateSubjects()
        {
            Subjects = new BindableCollection<Subject>(_subjectsService.GetAll());
            var newSubject = new Subject
            {
                Id = Guid.Empty,
                Name = " �������� �������"
            };
            Subjects.Add(newSubject);
        }

        private void UpdateAuditories()
        {
            Auditories = new BindableCollection<Room>(_roomsService.GetAll());
            var newRoom = new Room
            {
                Id = Guid.Empty,
                Name = " �������� ���������"
            };
            Auditories.Add(newRoom);
        }

        private void UpdateTeachers()
        {
            Teachers = new BindableCollection<Teacher>(_teachersService.GetAll());
            var newTeacher = new Teacher
            {
                Id = Guid.Empty,
                Name = " �������� �������������"
            };
            Teachers.Add(newTeacher);
        }

        private void SetTeachersFromPlan()
        {
            if (State != ReferenceStates.EditRecord && State != ReferenceStates.NewRecord)
            {
                return;
            }

            IEnumerable<Teacher> planTeachers;
            var planItem = _learningPlansService.GetPlanItem(SelectedSubject,
                Schedule.CurrentSchedule, SelectedGroup);
            if (planItem == null)
            {
                return;
            }

            switch (SelectedLessonType.Key)
            {
                case LessonType.Lection:
                    planTeachers = planItem.LectionTeachers;
                    break;
                case LessonType.Practice:
                    planTeachers = planItem.PractTeachers;
                    break;
                default:
                    return;
            }

            bool needSyncTeachers = false;
            foreach (var lectionteacher in planTeachers)
            {
                if (SelectedTeachers.All(x => x.Id != lectionteacher.Id))
                {
                    needSyncTeachers = true;
                    break;
                }
            }

            if (needSyncTeachers && SelectedTeachers.Any() &&
                MessageBox.Show("�������� �������������� �� �����?",
                "����������� ��������", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            {
                needSyncTeachers = false;
            }

            if (needSyncTeachers)
            {
                foreach (var lectionteacher in planTeachers)
                {
                    if (SelectedTeachers.All(x => x.Id != lectionteacher.Id))
                    {
                        SelectedTeachers.Add(lectionteacher);
                    }
                }
            }
        }

        public void ItemSelectionChangeEvent(object sender, ItemSelectionChangedEventArgs e)
        {
            var teacher = e.Item as Teacher;
            if (teacher == null) return;

            if (teacher.Id == Guid.Empty && CurrentField == Screens.None)
            {
                SelectedTeachers.Remove(teacher);
                if (CurrentField == Screens.None)
                {
                    CurrentField = Screens.TeachersReference;
                    Events.Publish(new ScreenEvent(
                        Screens.TeachersReference, ScreenOperations.NewRecord));
                }
            }
        }

        #endregion

        public override void New(Screens newRecordCreator = Screens.None, object newRecordCreatorDetails = null)
        {
            base.New(newRecordCreator, newRecordCreatorDetails);
            if (PresetTeacher != null)
            {
                var teacher = Teachers.FirstOrDefault(x => x.Id == PresetTeacher.Id);
                if (teacher != null) SelectedTeachers.Add(teacher);
            }

            if (PresetRoom != null)
            {
                SelectedAuditory = Auditories.FirstOrDefault(x => x.Id == PresetRoom.Id);
            }

            CurrentItem.LessonHour = CurrentLessonHour;
        }

        public override void Edit()
        {
            base.Edit();
            if (PresetRoom != null)
            {
                SelectedAuditory = Auditories.FirstOrDefault(x => x.Id == PresetRoom.Id);
            }
        }

        #endregion
    }
}