using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.ViewModels.Shared;
using Xceed.Wpf.Toolkit.Primitives;

namespace ScheduleMaker.ViewModels.References.ComplexReferences
{
    public class LearningPlanViewModel : BaseReferenceViewModel<LearningPlan>,
        IHandle<VisibilityEvent>, IHandle<UpdateChildsEvent>
    {
        #region ����

        private readonly GroupsService _groupsService;
        private readonly SubjectsService _subjectsService;
        private readonly SchedulesService _schedulesService;
        private readonly TeachersService _teachersService;
        private readonly LearningPlansService _learningPlansService;
        private readonly RoomsService _roomsService;
        private BindableCollection<Group> _groups;
        private BindableCollection<Subject> _subjects;
        private BindableCollection<Schedule> _schedules;
        private BindableCollection<Teacher> _selectedLectionsTeachers;
        private BindableCollection<Teacher> _selectedPracticeTeachers;
        private BindableCollection<Teacher> _lectionsTeachers;
        private BindableCollection<Teacher> _practiceTeachers;
        private int? _lectionsCount;
        private int? _practiceCount;
        private BindableCollection<Room> _auditories;
        private BindableCollection<Room> _selectedPracticeRooms;

        #endregion

        #region ������������

        public LearningPlanViewModel(
            LearningPlansService learningPlanService,
            SchedulesService schedulesService,
            IEventAggregator events,
            EditBarViewModel editBarViewModel,
            FindPanelViewModel findPanelVModel,
            GroupsService groupsService,
            SubjectsService subjectsService,
            TeachersService teachersService,
            LearningPlansService learningPlansService,
            RoomsService roomsService)
            : base(learningPlanService, events)
        {
            FindPanelVModel = findPanelVModel;
            EditBarVModel = editBarViewModel;
            FindPanelVModel.IsPanelVisible = true;
            EditBarVModel.IsPanelVisible = true;
            events.Subscribe(this);
            _groupsService = groupsService;
            _subjectsService = subjectsService;
            _schedulesService = schedulesService;
            _teachersService = teachersService;
            _learningPlansService = learningPlansService;
            _roomsService = roomsService;
        }

        #endregion

        #region ��������

        public Screens CurrentField { get; set; }

        public EditBarViewModel EditBarVModel { get; private set; }

        public FindPanelViewModel FindPanelVModel { get; private set; }

        public BindableCollection<Room> SelectedPracticeRooms
    {
            set
            {
                _selectedPracticeRooms = value;
                NotifyOfPropertyChange(() => SelectedPracticeRooms);
            }
            get { return _selectedPracticeRooms; }
        }

        public BindableCollection<Room> Auditories
        {
            set
            {
                _auditories = value;
                NotifyOfPropertyChange(() => Auditories);
            }
            get { return _auditories; }
        }

        #region ������

        public BindableCollection<Group> Groups
        {
            set
            {
                _groups = value;
                NotifyOfPropertyChange(() => Groups);
            }
            get { return _groups; }
        }

        public BindableCollection<Subject> Subjects
        {
            set
            {
                _subjects = value;
                NotifyOfPropertyChange(() => Subjects);
            }
            get { return _subjects; }
        }

        public BindableCollection<Schedule> Schedules
        {
            set
            {
                _schedules = value;
                NotifyOfPropertyChange(() => Schedules);
            }
            get { return _schedules; }
        }

        #endregion

        #region Selected items

        public Group SelectedGroup
        {
            get => Group.CurrentGroup;
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.GroupsReference;
                        Events.Publish(new ScreenEvent(
                            Screens.GroupsReference, ScreenOperations.NewRecord));
                    }

                    return;
                }

                Group.CurrentGroup = value;
                base.UpdateData();
                NotifyOfPropertyChange(() => SelectedGroup);
            }
        }

        public Subject SelectedSubject
        {
            get { return CurrentItem.Subject; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.SubjectsReference;
                        Events.Publish(new ScreenEvent(Screens.SubjectsReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                CurrentItem.Subject = value;
                NotifyOfPropertyChange(() => SelectedSubject);
            }
        }

        public Schedule SelectedSchedule
        {
            get { return Schedule.CurrentSchedule; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.SchedulesReference;
                        Events.Publish(new ScreenEvent(Screens.SchedulesReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                Schedule.CurrentSchedule = value;
                base.UpdateData();
                UpdateGroups();
                NotifyOfPropertyChange(() => SelectedSchedule);
            }
        }

        #endregion

        public int LessonsCount
        {
            get { return CurrentItem.LessonsCount; }
        }

        public int? LectionsCount
        {
            set
            {
                _lectionsCount = value;
                NotifyOfPropertyChange(() => LectionsCount);
            }
            get { return _lectionsCount; }
        }

        public int? PracticeCount
        {
            set
            {
                _practiceCount = value;
                NotifyOfPropertyChange(() => PracticeCount);
            }
            get { return _practiceCount; }
        }

        public BindableCollection<Teacher> LectionsTeachers
        {
            set
            {
                _lectionsTeachers = value;
                NotifyOfPropertyChange(() => LectionsTeachers);
            }
            get { return _lectionsTeachers; }
        }

        public BindableCollection<Teacher> SelectedLectionsTeachers
        {
            set
            {
                _selectedLectionsTeachers = value;
                NotifyOfPropertyChange(() => SelectedLectionsTeachers);
            }
            get { return _selectedLectionsTeachers; }
        }

        public BindableCollection<Teacher> PracticeTeachers
        {
            set
            {
                _practiceTeachers = value;
                NotifyOfPropertyChange(() => PracticeTeachers);
            }
            get { return _practiceTeachers; }
        }

        public BindableCollection<Teacher> SelectedPracticeTeachers
        {
            set
            {
                _selectedPracticeTeachers = value;
                NotifyOfPropertyChange(() => SelectedPracticeTeachers);
            }
            get { return _selectedPracticeTeachers; }
        }

        public override bool CanNew
        {
            get { return SelectedSchedule != null && SelectedGroup != null && base.CanNew; }
        }

        public override bool CanSave
        {
            get
            {
                return SelectedGroup != null && SelectedSubject != null && base.CanSave &&
                       LectionsCount.HasValue && PracticeCount.HasValue;
            }
        }

        protected override Screens CurrentScreen
        {
            get { return Screens.CommonSchedule; }
        }

        public override IEnumerable<LearningPlan> AlternativeSourceOfRecords
        {
            get
            {
                if (SelectedSchedule != null && SelectedGroup != null)
                {
                    var plans = _learningPlansService.GetForGroup(SelectedSchedule.Id, SelectedGroup.Id);
                    return plans;
                }

                return new List<LearningPlan>();
            }
        }

        private void UpdateAuditories()
        {
            Auditories = new BindableCollection<Room>(_roomsService.GetAll());
            var newRoom = new Room
            {
                Id = Guid.Empty,
                Name = " �������� ���������"
            };
            Auditories.Add(newRoom);
        }

        #endregion

        #region ������

        public override void UpdateFields()
        {
            base.UpdateFields();
            NotifyOfPropertyChange(() => SelectedGroup);
            NotifyOfPropertyChange(() => SelectedSubject);
            NotifyOfPropertyChange(() => LessonsCount);
            PracticeCount = CurrentItem.PracticeCount;
            LectionsCount = CurrentItem.LectionsCount;
            NotifyOfPropertyChange(() => SelectedLectionsTeachers);
            NotifyOfPropertyChange(() => SelectedPracticeTeachers);
            UpdateSelectedPracticeRooms();
            UpdateSelectedTeachers();
        }

        private void UpdateSelectedPracticeRooms()
        {
            SelectedPracticeRooms = new BindableCollection<Room>(CurrentItem.PractRooms);
        }

        private void UpdateSelectedTeachers()
        {
            SelectedLectionsTeachers = new BindableCollection<Teacher>
                (CurrentItem.LectionTeachers);
            SelectedPracticeTeachers = new BindableCollection<Teacher>
                (CurrentItem.PractTeachers);
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case "SelectedSubject":
                    if (SelectedSubject == null)
                    {
                        return "���������� ������� �������";
                    }

                    if (ReferenceService.Exists(x =>
                        x.Schedule.Id == SelectedSchedule.Id && x.Id != CurrentItem.Id &&
                        x.Group.Id == SelectedGroup.Id && x.Subject.Id == SelectedSubject.Id))
                    {
                        return "���� ��� ������� �������� ��� �����������";
                    }

                    break;
                case "PracticeCount":
                    if (!PracticeCount.HasValue)
                    {
                        return "���� \"����� ������������ �������\" ������ ���� ���������";
                    }

                    if (PracticeCount < 0)
                    {
                        return "�������� ���� \"����� ������������ �������\" ������ ���� ���������������";
                    }

                    break;
                case "LectionsCount":
                    if (!LectionsCount.HasValue)
                    {
                        return "���� \"����� ������\" ������ ���� ���������";
                    }

                    if (LectionsCount < 0)
                    {
                        return "�������� ���� \"����� ������\" ������ ���� ���������������";
                    }

                    break;
            }

            return null;
        }

        public override void Save()
        {
            if (SelectedGroup == null || SelectedSchedule == null)
            {
                Cancel();
                return;
            }

            if (_lectionsCount.HasValue)
            {
                CurrentItem.LectionsCount = _lectionsCount.Value;
            }

            if (_practiceCount.HasValue)
            {
                CurrentItem.PracticeCount = _practiceCount.Value;
            }

            CurrentItem.Schedule = SelectedSchedule;
            CurrentItem.Group = SelectedGroup;
            CurrentItem.PractTeachers = SelectedPracticeTeachers;
            CurrentItem.LectionTeachers = SelectedLectionsTeachers;
            CurrentItem.PractRooms = SelectedPracticeRooms;
            CurrentField = Screens.None;
            base.Save();
            UpdateGroups();
        }

        public override void Cancel()
        {
            base.Cancel();
            CurrentField = Screens.None;
        }

        public override void UpdateData()
        {
            base.UpdateData();
            IsInitialization = true;
            UpdateSchedules();
            UpdateGroups();
            UpdateSubjects();
            UpdateTeachers();
            UpdateAuditories();
            IsInitialization = false;
        }

        public override bool Delete()
        {
            var result = base.Delete();
            UpdateGroups();
            return result;
        }

        #region ����������� �������

        public void Handle(VisibilityEvent message)
        {
            if (message.Sender == typeof(PanelViewModel))
            {
                EditBarVModel.IsPanelVisible = !message.IsVisible;
                FindPanelVModel.IsPanelVisible = !message.IsVisible;
            }
        }

        public void Handle(UpdateChildsEvent message)
        {
            switch (message.RecordCreator)
            {
                case Screens.GroupsReference:
                    UpdateGroups();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedGroup = Groups.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.SubjectsReference:
                    if (IsEditMode)
                    {
                        UpdateSubjects();
                        if (message.UpdatedRecordId != Guid.Empty)
                        {
                            SelectedSubject = Subjects.FirstOrDefault(x =>
                                x.Id == message.UpdatedRecordId);
                        }
                    }
                    break;
                case Screens.RoomsReference:
                    if (IsEditMode)
                    {
                        UpdateAuditories();
                        if (message.UpdatedRecordId != Guid.Empty)
                        {
                            SelectedPracticeRooms.Add(Auditories.FirstOrDefault(
                                x => x.Id == message.UpdatedRecordId));
                            NotifyOfPropertyChange(() => SelectedPracticeRooms);
                        }
                    }
                    break;
                case Screens.SchedulesReference:
                    CurrentField = Screens.SchedulesReference;
                    UpdateSchedules();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedSchedule = Schedules.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.TeachersReference:
                    UpdateTeachers();

                    if (message.UpdatedRecordId != Guid.Empty &&
                        message.RecordCreatorDetails != null)
                    {
                        switch (message.RecordCreatorDetails.ToString())
                        {
                            case "SelectedPracticeTeachers":
                                SelectedPracticeTeachers.Add(PracticeTeachers.FirstOrDefault(
                                    x => x.Id == message.UpdatedRecordId));
                                NotifyOfPropertyChange(() => SelectedPracticeTeachers);
                                break;
                            case "SelectedLectionsTeachers":
                                SelectedLectionsTeachers.Add(LectionsTeachers.FirstOrDefault(
                                    x => x.Id == message.UpdatedRecordId));
                                NotifyOfPropertyChange(() => SelectedLectionsTeachers);
                                break;
                        }
                    }
                    break;
            }
        }

        #endregion


        #region ������ ���������� �����

        private void UpdateGroups()
        {
            Groups = new BindableCollection<Group>(
                _groupsService.GetAvailableInSchedule(SelectedSchedule));
            var newGroup = new Group
            {
                Id = Guid.Empty,
                Name = " ������� ������"
            };
            Groups.Add(newGroup);
            var selectedGroup = SelectedGroup;
            if (selectedGroup != null)
            {
                SelectedGroup = null;
            }

            SelectedGroup = Group.CurrentGroup != null 
                ? Groups.FirstOrDefault(x => x.Id == Group.CurrentGroup.Id)
                : Groups.FirstOrDefault(x => x.Id == selectedGroup?.Id);
        }

        private void UpdateSubjects()
        {
            Subjects = new BindableCollection<Subject>(_subjectsService.GetAll());
            var newSubject = new Subject
            {
                Id = Guid.Empty,
                Name = " �������� �������"
            };
            Subjects.Add(newSubject);
        }

        private void UpdateSchedules()
        {
            Guid selectedScheduleId = SelectedSchedule != null
                ? SelectedSchedule.Id
                : Guid.Empty;
            Schedules = new BindableCollection<Schedule>(_schedulesService.GetAll()
                .OrderByDescending(x => x.Name));
            var newItem = new Schedule
            {
                Id = Guid.Empty,
                Name = "������� �����"
            };
            Schedules.Insert(0, newItem);
            if (selectedScheduleId != Guid.Empty)
            {
                SelectedSchedule = _schedulesService.GetById(selectedScheduleId);
            }
        }

        private void UpdateTeachers()
        {
            LectionsTeachers = new BindableCollection<Teacher>(_teachersService.GetAll());
            var newTeacher = new Teacher
            {
                Id = Guid.Empty,
                Name = " �������� �������������"
            };
            LectionsTeachers.Add(newTeacher);
            PracticeTeachers = LectionsTeachers;
        }

        public void LectionsTeachersChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            var teacher = e.Item as Teacher;
            if (teacher == null) return;

            if (teacher.Id == Guid.Empty && CurrentField == Screens.None)
            {
                SelectedLectionsTeachers.Remove(teacher);
                if (CurrentField == Screens.None)
                {
                    CurrentField = Screens.TeachersReference;
                    Events.Publish(new ScreenEvent(
                        Screens.TeachersReference, ScreenOperations.NewRecord,
                        "SelectedLectionsTeachers"));
                }
            }
        }


        public void PracticeTeachersChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            var teacher = e.Item as Teacher;
            if (teacher == null) return;

            if (teacher.Id == Guid.Empty && CurrentField == Screens.None)
            {
                SelectedPracticeTeachers.Remove(teacher);
                if (CurrentField == Screens.None)
                {
                    CurrentField = Screens.TeachersReference;
                    Events.Publish(new ScreenEvent(
                        Screens.TeachersReference, ScreenOperations.NewRecord,
                        nameof(SelectedPracticeTeachers)));
                }
            }
        }

        public void PracticeRoomsChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            var room = e.Item as Room;
            if (room == null) return;

            if (room.Id == Guid.Empty && CurrentField == Screens.None)
            {
                SelectedPracticeRooms.Remove(room);
                if (CurrentField == Screens.None)
                {
                    CurrentField = Screens.TeachersReference;
                    Events.Publish(new ScreenEvent(
                        Screens.RoomsReference, ScreenOperations.NewRecord,
                        nameof(SelectedPracticeRooms)));
                }
            }
        }
        
        #endregion

        #endregion
    }
}