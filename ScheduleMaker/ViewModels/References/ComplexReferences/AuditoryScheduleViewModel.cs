﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.ViewModels.Shared;

namespace ScheduleMaker.ViewModels.References.ComplexReferences
{
    public class AuditoryScheduleViewModel : BaseViewModel, IHandle<UpdateChildsEvent>
    {
        #region Поля

        private readonly LessonHoursService _lessonHoursService;
        private readonly SchedulesService _schedulesService;
        private readonly RoomsService _roomsService;
        private BindableCollection<WeekDay> _weekDays;
        private readonly IEventAggregator _events;
        private WeekDay _selectedWeekDay;
        private ObservableCollection<Schedule> _schedules;
        private BindableCollection<Room> _auditories;

        #endregion

        #region Конструкторы

        public AuditoryScheduleViewModel(SchedulesService schedulesService,
            LessonHoursService lessonHoursService, LessonsViewModel lessonsVModel,
            LessonHourEditorViewModel lessonHourVModel, IEventAggregator events, 
            RoomsService roomsService)
        {
            _events = events;
            _roomsService = roomsService;
            _schedulesService = schedulesService;
            _lessonHoursService = lessonHoursService;
            LessonHourVModel = lessonHourVModel;
            LessonsVModel = lessonsVModel;
            WeekDays = new BindableCollection<WeekDay>();
            NotifyOfPropertyChange(() => LessonHourVModel);
            _events.Subscribe(this);
        }

        #endregion

        #region Свойства

        public LessonHourEditorViewModel LessonHourVModel { get; private set; }

        public LessonsViewModel LessonsVModel { get; private set; }

        public BindableCollection<WeekDay> WeekDays
        {
            set
            {
                _weekDays = value;
                NotifyOfPropertyChange(() => WeekDays);
            }
            get { return _weekDays; }
        }

        public WeekDay SelectedWeekDay
        {
            get { return _selectedWeekDay; }
            set
            {
                _selectedWeekDay = value;
                if (value != null)
                {
                    var currentLesson = _selectedWeekDay.LessonHours.FirstOrDefault(x =>
                             x.StartTime.TimeOfDay == SelectedLessonHour.StartTime.TimeOfDay);
                    if (LessonHourVModel.CurrentItem.Id == Guid.Empty || currentLesson == null)
                    {
                        TimeSpan minTimeToStart = TimeSpan.MaxValue;
                        LessonHour mostPossibleLesson = null;
                        foreach (var hour in _selectedWeekDay.LessonHours)
                        {
                            if (hour.StartTime.TimeOfDay <= DateTime.Now.TimeOfDay &&
                                hour.StartTime.TimeOfDay + hour.Schedule.LessonDuration
                                    >= DateTime.Now.TimeOfDay)
                            {
                                mostPossibleLesson = hour;
                                break;
                            }

                            var def = hour.StartTime.TimeOfDay - DateTime.Now.TimeOfDay;
                            if (def.Duration() < minTimeToStart)
                            {
                                mostPossibleLesson = hour;
                                minTimeToStart = def.Duration();
                            }
                        }

                        SelectedLessonHour = mostPossibleLesson;
                    }
                    else
                    {
                        SelectedLessonHour = currentLesson;
                    }

                    LessonHourVModel.Day = value.Day;
                }

                NotifyOfPropertyChange(() => SelectedWeekDay);
            }
        }

        public BindableCollection<Room> Auditories
        {
            set
            {
                _auditories = value;
                NotifyOfPropertyChange(() => Auditories);
            }
            get { return _auditories; }
        }

        public Room SelectedAuditory
        {
            get { return Room.CurrentRoom; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.RoomsReference;
                        _events.Publish(new ScreenEvent(Screens.RoomsReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                Room.CurrentRoom = value;
                LessonsVModel.PresetRoom = value;
                UpdateWeekDays();
                NotifyOfPropertyChange(() => SelectedAuditory);
            }
        }

        public LessonHour SelectedLessonHour
        {
            get { return LessonHourVModel.CurrentItem; }
            set
            {
                LessonHourVModel.CurrentItem = value;
                LessonsVModel.CurrentLessonHour = value;
                NotifyOfPropertyChange(() => SelectedLessonHour);
            }
        }

        public ObservableCollection<Schedule> Schedules
        {
            set
            {
                _schedules = value;
                NotifyOfPropertyChange(() => Schedules);
            }
            get { return _schedules; }
        }

        public Schedule SelectedSchedule
        {
            get { return Schedule.CurrentSchedule; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.SchedulesReference;
                        _events.Publish(new ScreenEvent(Screens.SchedulesReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                Schedule.CurrentSchedule = value;
                UpdateWeekDays();
                NotifyOfPropertyChange(() => SelectedSchedule);
            }
        }

        public Screens CurrentField { get; set; }

        #endregion

        #region Методы

        public override void UpdateData()
        {
            Guid selectedScheduleId = SelectedSchedule != null
                ? SelectedSchedule.Id : Guid.Empty;
            Schedules = new ObservableCollection<Schedule>(_schedulesService.GetAll()
                .OrderByDescending(x => x.Name));
            var newItem = new Schedule
            {
                Id = Guid.Empty,
                Name = "Создать новое"
            };
            Schedules.Insert(0, newItem);
            if (selectedScheduleId != Guid.Empty)
            {
                SelectedSchedule = _schedulesService.GetById(selectedScheduleId);
                UpdateWeekDays();
            }

            LessonsVModel.PresetRoom = SelectedAuditory ?? new Room();

            UpdateAuditoriess();
        }

        private void UpdateWeekDays()
        {
            var selectedDay = SelectedWeekDay;
            LessonHour selectedLessonHour = null;
            if (SelectedSchedule != null)
            {
                selectedLessonHour = SelectedLessonHour;
                SelectedSchedule.LessonHours = _lessonHoursService.GetAll()
                    .Where(x => x.Schedule.Id == SelectedSchedule.Id).ToList();
                LessonHourVModel.Schedule = SelectedSchedule;
            }

            WeekDays = new BindableCollection<WeekDay>(
                _schedulesService.GetWeekDaysForAuditory(SelectedSchedule, SelectedAuditory));
            SelectedWeekDay = selectedDay == null
                ? WeekDays.FirstOrDefault(weekDay => weekDay.Day == DateTime.Now.DayOfWeek)
                : WeekDays.FirstOrDefault(weekDay => weekDay.Day == selectedDay.Day);

            if (selectedLessonHour != null)
            {
                var currentLesson = SelectedSchedule.LessonHours.FirstOrDefault(x =>
                    x.Id == selectedLessonHour.Id);
                if (currentLesson != null)
                {
                    SelectedLessonHour = currentLesson;
                }
            }
        }

        public void DeleteLessonHour(Guid id)
        {
            LessonHour currentHour = SelectedLessonHour;
            LessonHourVModel.SelectRecord(id);
            if (LessonHourVModel.Delete())
            {
                if (id != currentHour.Id)
                {
                    SelectedLessonHour = currentHour;
                }

                UpdateWeekDays();
            }
        }

        public void EditLessonHour(Guid id)
        {
            LessonHourVModel.SelectRecord(id);
            LessonHourVModel.Edit();
        }

        #endregion

        public void Handle(UpdateChildsEvent message)
        {
            switch (message.RecordCreator)
            {
                case Screens.AuditorySchedule:
                    UpdateWeekDays();
                    break;

                case Screens.SchedulesReference:
                    UpdateData();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedSchedule = Schedules.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.LessonHours:
                    UpdateData();
                    break;
                case Screens.RoomsReference:
                    UpdateAuditoriess();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedAuditory = Auditories.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }

                    NotifyOfPropertyChange(() => SelectedAuditory);
                    break;
            }
        }

        private void UpdateAuditoriess()
        {
            Auditories = new BindableCollection<Room>(_roomsService.GetAll());
            var newRoom = new Room
            {
                Id = Guid.Empty,
                Name = " Добавить аудиторию"
            };
            Auditories.Add(newRoom);
        }
    }
}
