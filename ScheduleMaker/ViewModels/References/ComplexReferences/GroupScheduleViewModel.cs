﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Helpers;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.Models;
using ScheduleMaker.ViewModels.Shared;
using Xceed.Wpf.Toolkit.Primitives;

namespace ScheduleMaker.ViewModels.References.ComplexReferences
{
    public class GroupScheduleViewModel : BaseReferenceViewModel<Lesson>, IHandle<UpdateChildsEvent>,
        IHandle<GroupScheduleEvent>
    {
        #region Поля

        private readonly LessonHoursService _lessonHoursService;
        private readonly SchedulesService _schedulesService;
        private readonly GroupsService _groupsService;
        private readonly SubjectsService _subjectsService;
        private readonly TeachersService _teachersService;
        private readonly RoomsService _roomsService;
        private readonly LearningPlansService _learningPlansService;
        private readonly LessonsService _lessonsService;
        private readonly IEventAggregator _events;
        private ObservableCollection<Schedule> _schedules;
        private BindableCollection<Group> _groups;
        private BindableCollection<WeekDayLessonsViewModel> _weekDaysVModels;
        private bool _denyLessonsLoading;
        private BindableCollection<Subject> _subjects;
        private BindableCollection<Room> _auditories;
        private BindableCollection<KeyValuePair<LessonType, string>> _lessonTypes;
        private BindableCollection<Teacher> _teachers;
        private BindableCollection<KeyValuePair<WeekType, string>> _weekTypes;
        private BindableCollection<Teacher> _selectedTeachers;
        private bool _isWeekTypesAvailable;
        private BindableCollection<KeyValuePair<SubGroup, string>> _subGroups;
        private bool _isSubGroupsAvailable;
        private bool _denyRedirecting;

        #endregion

        #region Конструкторы

        public GroupScheduleViewModel(LessonsService lessonsService, SchedulesService schedulesService,
            LessonHoursService lessonHoursService, GroupsService groupsService,
            IEventAggregator events, SubjectsService subjectsService, TeachersService teachersService,
            RoomsService roomsService, LearningPlansService learningPlansService)
            : base(lessonsService, events)
        {
            _events = events;
            _lessonsService = lessonsService;
            _subjectsService = subjectsService;
            _teachersService = teachersService;
            _roomsService = roomsService;
            _learningPlansService = learningPlansService;
            _schedulesService = schedulesService;
            _lessonHoursService = lessonHoursService;
            _groupsService = groupsService;
            _events.Subscribe(this);
            _denyLessonsLoading = false;
            WeekDaysVModels = new BindableCollection<WeekDayLessonsViewModel>();
        }

        #endregion

        #region Свойства

        public Screens CurrentField { get; set; }

        public ObservableCollection<Schedule> Schedules
        {
            set
            {
                _schedules = value;
                NotifyOfPropertyChange(() => Schedules);
            }
            get { return _schedules; }
        }

        public Schedule SelectedSchedule
        {
            get { return Schedule.CurrentSchedule; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.SchedulesReference;
                        _events.Publish(new ScreenEvent(Screens.SchedulesReference,
                            ScreenOperations.NewRecord, Screens.CommonSchedule));
                    }

                    return;
                }

                Schedule.CurrentSchedule = value;
                UpdateGroups();
                UpdateWeekDaysLessonHours();
                if (IsEditMode)
                {
                    Cancel();
                }

                NotifyOfPropertyChange(() => SelectedSchedule);
            }
        }

        public BindableCollection<Group> Groups
        {
            set
            {
                _groups = value;
                NotifyOfPropertyChange(() => Groups);
            }
            get { return _groups; }
        }

        public BindableCollection<WeekDayLessonsViewModel> WeekDaysVModels
        {
            set
            {
                _weekDaysVModels = value;
                NotifyOfPropertyChange(() => SelectedGroup);
            }
            get { return _weekDaysVModels; }
        }

        public BindableCollection<Subject> Subjects
        {
            set
            {
                _subjects = value;
                NotifyOfPropertyChange(() => Subjects);
            }
            get { return _subjects; }
        }

        public BindableCollection<KeyValuePair<LessonType, string>> LessonTypes
        {
            set
            {
                _lessonTypes = value;
                NotifyOfPropertyChange(() => LessonTypes);
            }
            get { return _lessonTypes; }
        }

        public override void New(Screens newRecordCreator = Screens.None, object newRecordCreatorDetails = null)
        {
            Cancel();
            base.New(newRecordCreator, newRecordCreatorDetails);
        }

        public BindableCollection<Teacher> Teachers
        {
            set
            {
                _teachers = value;
                NotifyOfPropertyChange(() => Teachers);
            }
            get { return _teachers; }
        }

        public BindableCollection<Room> Auditories
        {
            set
            {
                _auditories = value;
                NotifyOfPropertyChange(() => Auditories);
            }
            get { return _auditories; }
        }

        public BindableCollection<KeyValuePair<WeekType, string>> WeekTypes
        {
            set
            {
                _weekTypes = value;
                NotifyOfPropertyChange(() => WeekTypes);
            }
            get { return _weekTypes; }
        }

        public BindableCollection<KeyValuePair<SubGroup, string>> SubGroups
        {
            set
            {
                _subGroups = value;
                NotifyOfPropertyChange(() => SubGroups);
            }
            get { return _subGroups; }
        }

        #region Selected items

        public Group SelectedGroup
        {
            get { return Group.CurrentGroup; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.GroupsReference;
                        _events.Publish(new ScreenEvent(
                            Screens.GroupsReference, ScreenOperations.NewRecord));
                    }

                    return;
                }

                Group.CurrentGroup = value;
                UpdateWeekDaysLessonHours();
                if (IsEditMode)
                {
                    Cancel();
                }

                NotifyOfPropertyChange(() => SelectedGroup);
            }
        }

        public Subject SelectedSubject
        {
            get { return CurrentItem.Subject; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.SubjectsReference;
                        _events.Publish(new ScreenEvent(Screens.SubjectsReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                CurrentItem.Subject = value;
                NotifyOfPropertyChange(() => SelectedSubject);
                SetTeachersFromPlan();
            }
        }

        public BindableCollection<Teacher> SelectedTeachers
        {
            set
            {
                _selectedTeachers = value;
                NotifyOfPropertyChange(() => SelectedTeachers);
            }
            get { return _selectedTeachers; }
        }

        public Room SelectedAuditory
        {
            get { return CurrentItem.Room; }
            set
            {
                if (value != null && value.Id == Guid.Empty)
                {
                    if (CurrentField == Screens.None)
                    {
                        CurrentField = Screens.RoomsReference;
                        Events.Publish(new ScreenEvent(Screens.RoomsReference,
                            ScreenOperations.NewRecord));
                    }

                    return;
                }

                CurrentItem.Room = value;
                NotifyOfPropertyChange(() => SelectedAuditory);
            }
        }

        public KeyValuePair<LessonType, string> SelectedLessonType
        {
            get
            {
                return new KeyValuePair<LessonType, string>(
              CurrentItem.LessonType, CurrentItem.LessonType.Description());
            }
            set
            {
                CurrentItem.LessonType = value.Key;
                NotifyOfPropertyChange(() => SelectedLessonType);
                SetTeachersFromPlan();
                SetRoomsFromPlan();
            }
        }

        public KeyValuePair<WeekType, string> SelectedWeekType
        {
            get
            {
                return new KeyValuePair<WeekType, string>(
                    CurrentItem.WeekType, CurrentItem.WeekType.Description());
            }
            set
            {
                CurrentItem.WeekType = value.Key;
                NotifyOfPropertyChange(() => SelectedSubGroup);
                NotifyOfPropertyChange(() => SelectedWeekType);
                NotifyOfPropertyChange(() => SelectedGroup);
            }
        }

        public KeyValuePair<SubGroup, string> SelectedSubGroup
        {
            get
            {
                return new KeyValuePair<SubGroup, string>(
                    CurrentItem.SubGroup, CurrentItem.SubGroup.Description());
            }
            set
            {
                CurrentItem.SubGroup = value.Key;
                NotifyOfPropertyChange(() => SelectedSubGroup);
                NotifyOfPropertyChange(() => SelectedWeekType);
                NotifyOfPropertyChange(() => SelectedGroup);
            }
        }

        public string Description
        {
            get { return CurrentItem.Description; }
            set
            {
                CurrentItem.Description = value;
                NotifyOfPropertyChange(() => Description);
            }
        }

        public string LessonTime
        {
            get
            {
                return CurrentItem.LessonHour != null
                    ? string.Format("{0} ({1})", CurrentItem.LessonHour.DayName, CurrentItem.LessonHour.DisplayName)
                    : "";
            }
        }

        #endregion

        protected override Screens CurrentScreen { get { return Screens.GroupSchedule; } }

        #endregion

        #region Методы

        public override void UpdateData()
        {
            _denyRedirecting = false;
            base.UpdateData();
            UpdateScheduls();
            UpdateGroups();
            UpdateWeekDaysLessonHours();
            UpdateSubjects();
            UpdateAuditories();
            UpdateTeachers();
            UpdateWeekTypes();
            UpdateSubGroups();
            LessonTypes = new BindableCollection<KeyValuePair<LessonType, string>>(
                EnumHelpers.GetAllItemsWithDescription<LessonType>());
        }

        private void UpdateWeekTypes()
        {
            WeekTypes = new BindableCollection<KeyValuePair<WeekType, string>>(
               EnumHelpers.GetAllItemsWithDescription<WeekType>());
            IsWeekTypesAvailable = !_lessonsService.ExistsOpposite(CurrentItem);
        }

        private void UpdateSubGroups()
        {
            SubGroups = new BindableCollection<KeyValuePair<SubGroup, string>>(
               EnumHelpers.GetAllItemsWithDescription<SubGroup>());
            IsSubGroupsAvailable = !_lessonsService.ExistsOpposite(CurrentItem);
        }

        public bool IsWeekTypesAvailable
        {
            get { return _isWeekTypesAvailable; }
            set
            {
                if (_isWeekTypesAvailable != value)
                {
                    _isWeekTypesAvailable = value;
                    NotifyOfPropertyChange(() => IsWeekTypesAvailable);
                }
            }
        }

        public bool IsSubGroupsAvailable
        {
            get { return _isSubGroupsAvailable; }
            set
            {
                if (_isSubGroupsAvailable != value)
                {
                    _isSubGroupsAvailable = value;
                    NotifyOfPropertyChange(() => IsSubGroupsAvailable);
                }
            }
        }

        private void SetTeachersFromPlan()
        {
            if (State != ReferenceStates.EditRecord && State != ReferenceStates.NewRecord)
            {
                return;
            }

            IEnumerable<Teacher> planTeachers;
            var planItem = _learningPlansService.GetPlanItem(SelectedSubject,
                Schedule.CurrentSchedule, SelectedGroup);
            if (planItem == null)
            {
                return;
            }

            switch (SelectedLessonType.Key)
            {
                case LessonType.Lection:
                    planTeachers = planItem.LectionTeachers;
                    break;
                case LessonType.Practice:
                    planTeachers = planItem.PractTeachers;
                    break;
                default:
                    return;
            }

            bool needSyncTeachers = false;
            foreach (var lectionteacher in planTeachers)
            {
                if (SelectedTeachers.All(x => x.Id != lectionteacher.Id))
                {
                    needSyncTeachers = true;
                    break;
                }
            }

            if (needSyncTeachers && SelectedTeachers.Any() &&
                MessageBox.Show("Добавить преподавателей из плана?",
                "Подтвердите действие", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            {
                needSyncTeachers = false;
            }

            if (needSyncTeachers)
            {
                foreach (var lectionteacher in planTeachers)
                {
                    if (SelectedTeachers.All(x => x.Id != lectionteacher.Id))
                    {
                        SelectedTeachers.Add(lectionteacher);
                    }
                }
            }
        }

        private void SetRoomsFromPlan()
        {
            if (State != ReferenceStates.EditRecord && State != ReferenceStates.NewRecord)
            {
                return;
            }

            var planItem = _learningPlansService.GetPlanItem(SelectedSubject,
                Schedule.CurrentSchedule, SelectedGroup);
            if (planItem == null)
            {
                return;
            }

            Auditories = planItem.PractRooms.Any()
                ? new BindableCollection<Room>(planItem.PractRooms)
                : new BindableCollection<Room>(_roomsService.GetAll());
            var newRoom = new Room
            {
                Id = Guid.Empty,
                Name = " Добавить аудиторию"
            };
            Auditories.Add(newRoom);
        }

        public void Handle(UpdateChildsEvent message)
        {
            switch (message.RecordCreator)
            {
                case Screens.GroupSchedule:
                    UpdateWeekDays();
                    break;
                case Screens.SchedulesReference:
                    UpdateData();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedSchedule = Schedules.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.GroupsReference:
                    UpdateGroups();
                    if (message.UpdatedRecordId != Guid.Empty)
                    {
                        SelectedGroup = Groups.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.SubjectsReference:
                    UpdateSubjects();
                    if (message.UpdatedRecordId != Guid.Empty && IsEditMode)
                    {
                        SelectedSubject = Subjects.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.RoomsReference:
                    UpdateAuditories();
                    if (message.UpdatedRecordId != Guid.Empty && IsEditMode)
                    {
                        SelectedAuditory = Auditories.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId);
                    }
                    break;
                case Screens.TeachersReference:
                    UpdateTeachers();
                    if (message.UpdatedRecordId != Guid.Empty && IsEditMode)
                    {
                        SelectedTeachers.Add(Teachers.FirstOrDefault(x =>
                            x.Id == message.UpdatedRecordId));
                    }

                    NotifyOfPropertyChange(() => SelectedTeachers);
                    break;
            }
        }

        private void UpdateWeekDays()
        {
            if (SelectedSchedule != null)
            {
                SelectedSchedule.LessonHours = _lessonHoursService.GetAll()
                    .Where(x => x.Schedule.Id == SelectedSchedule.Id).ToList();
            }
        }

        private void UpdateGroups()
        {
            _denyLessonsLoading = true;
            Groups = new BindableCollection<Group>(
                _groupsService.GetAvailableInSchedule(SelectedSchedule));
            var newGroup = new Group
            {
                Id = Guid.Empty,
                Name = " Создать группу"
            };
            Groups.Add(newGroup);
            if (Group.CurrentGroup != null)
            {
                SelectedGroup = Groups.FirstOrDefault(x => x.Id == Group.CurrentGroup.Id);
            }

            _denyLessonsLoading = false;
        }

        private void UpdateScheduls()
        {
            _denyLessonsLoading = true;
            Guid selectedScheduleId = SelectedSchedule != null
                ? SelectedSchedule.Id : Guid.Empty;
            Schedules = new ObservableCollection<Schedule>(_schedulesService.GetAll()
                .OrderByDescending(x => x.Name));
            var newItem = new Schedule
            {
                Id = Guid.Empty,
                Name = "Создать новое"
            };
            Schedules.Insert(0, newItem);
            if (selectedScheduleId != Guid.Empty)
            {
                SelectedSchedule = _schedulesService.GetById(selectedScheduleId);
                UpdateWeekDays();
            }
            _denyLessonsLoading = false;
        }

        private void UpdateSubjects()
        {
            Subjects = new BindableCollection<Subject>(_subjectsService.GetAll());
            var newSubject = new Subject
            {
                Id = Guid.Empty,
                Name = " Добавить предмет"
            };
            Subjects.Add(newSubject);
        }

        private void UpdateAuditories()
        {
            Auditories = new BindableCollection<Room>(_roomsService.GetAll());
            var newRoom = new Room
            {
                Id = Guid.Empty,
                Name = " Добавить аудиторию"
            };
            Auditories.Add(newRoom);
        }

        private void UpdateTeachers()
        {
            Teachers = new BindableCollection<Teacher>(_teachersService.GetAll());
            var newTeacher = new Teacher
            {
                Id = Guid.Empty,
                Name = " Добавить преподавателя"
            };
            Teachers.Add(newTeacher);
        }

        private void UpdateSelectedTeachers()
        {
            SelectedTeachers = CurrentItem != null
                ? new BindableCollection<Teacher>(CurrentItem.Teachers)
                : new BindableCollection<Teacher>();
        }

        private void UpdateWeekDaysLessonHours()
        {
            if (_denyLessonsLoading) return;
            WeekDaysVModels.Clear();
            if (SelectedSchedule == null || SelectedGroup == null) return;
            var weekDaysLessons =
                _lessonHoursService.GetWeekDaysLessons(SelectedSchedule, SelectedGroup);
            foreach (var weekDay in weekDaysLessons.Keys)
            {
                WeekDaysVModels.Add(new WeekDayLessonsViewModel(weekDaysLessons[weekDay], weekDay, _events));
            }


            if (!_denyRedirecting && SelectedSchedule != null && SelectedGroup != null &&
                SelectedGroup.Id != Guid.Empty && !WeekDaysVModels.Any() && CurrentScreen == Screens.GroupSchedule)
            {
                _events.Publish(new ScreenEvent(Screens.CommonSchedule));
                _denyRedirecting = true;
            }
        }

        public void Handle(GroupScheduleEvent message)
        {
            switch (message.Operation)
            {
                case GroupScheduleOperations.DeleteLesson:
                    SelectedRecords.Add(_lessonsService.GetById(message.Lesson.Id));
                    if (Delete())
                    {
                        UpdateWeekDaysLessonHours();
                    }

                    break;
                case GroupScheduleOperations.EditLesson:
                    CurrentItem = message.Lesson;
                    UpdateFields();
                    Edit();
                    _events.Publish(new ReferncePanelsEvent());
                    break;
                case GroupScheduleOperations.NewLesson:
                    New();
                    CurrentItem = message.Lesson;
                    UpdateFields();
                    _events.Publish(new ReferncePanelsEvent());
                    break;
            }
        }

        public override void UpdateFields()
        {
            base.UpdateFields();
            CurrentItem.Group = SelectedGroup;
            if (CurrentItem.LessonHour != null)
            {
                CurrentItem.LessonHour = _lessonHoursService.GetById(CurrentItem.LessonHour.Id);
            }

            NotifyOfPropertyChange(() => SelectedGroup);
            NotifyOfPropertyChange(() => SelectedAuditory);
            NotifyOfPropertyChange(() => SelectedLessonType);
            NotifyOfPropertyChange(() => SelectedSubject);
            NotifyOfPropertyChange(() => SelectedWeekType);
            NotifyOfPropertyChange(() => SelectedSubGroup);
            NotifyOfPropertyChange(() => Description);
            NotifyOfPropertyChange(() => LessonTime);
            UpdateWeekTypes();
            UpdateSubGroups();
            UpdateSelectedTeachers();
        }

        public void ItemSelectionChangeEvent(object sender, ItemSelectionChangedEventArgs e)
        {
            var teacher = e.Item as Teacher;
            if (teacher == null) return;

            if (teacher.Id == Guid.Empty && CurrentField == Screens.None)
            {
                SelectedTeachers.Remove(teacher);
                if (CurrentField == Screens.None)
                {
                    CurrentField = Screens.TeachersReference;
                    Events.Publish(new ScreenEvent(
                        Screens.TeachersReference, ScreenOperations.NewRecord));
                }
            }
        }

        public override void Save()
        {
            CurrentItem.Teachers = SelectedTeachers;
            CurrentField = Screens.None;
            base.Save();
            UpdateWeekDaysLessonHours();
        }

        public override bool CanSave
        {
            get
            {
                return SelectedGroup != null && SelectedSubject != null
                  && base.CanSave;
            }
        }

        public override string GetErrors(string propertyName)
        {
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return null;
            }

            switch (propertyName)
            {
                case "SelectedGroup":
                    if (SelectedGroup == null)
                    {
                        return "Необходимо выбрать группу";
                    }

                    var existsLesson = _lessonsService.GetGetLessonsWithPreconditions(CurrentItem,
                        SelectedSubGroup.Key, SelectedWeekType.Key).FirstOrDefault();
                    if (existsLesson != null)
                    {
                        return string.Format("Для данной группы на текущее время уже назначено занятие ({0} - {1})",
                            existsLesson.SubGroup.Description(),
                            existsLesson.Subject.Name);
                    }

                    break;
            }

            return null;
        }

        public override IEnumerable<Error> GetWarnings(string propertyName)
        {
            var warnings = new List<Error>();
            if (State == ReferenceStates.None || State == ReferenceStates.DataProcessing)
            {
                return warnings;
            }

            // преверки групп и аудиторий
            if (SelectedGroup != null && SelectedAuditory != null &&
                (propertyName == "SelectedGroup" || propertyName == "SelectedAuditory"))
            {
                if (ReferenceService.GetAll().Any(x => x.Id != CurrentItem.Id &&
                    x.LessonHour.Id == CurrentItem.LessonHour.Id &&
                    (x.Room != null && x.Room.Id == SelectedAuditory.Id) &&
                    x.Group.Id != SelectedGroup.Id && (x.WeekType == SelectedWeekType.Key || SelectedWeekType.Key == WeekType.Simple || x.WeekType == WeekType.Simple)))
                {
                    warnings.Add(new Error(MessageType.Warning, string.Format(
                        "На данное время в аудитории \"{0}\" уже назначено занятие у другой группы", SelectedAuditory.Name),
                        new[] { "SelectedGroup", "SelectedAuditory" }, 1
                    ));
                }


                var totalStudentsCount = ReferenceService.GetAll().Where(x =>
                    x.LessonHour.Id == CurrentItem.LessonHour.Id &&
                    x.Id != CurrentItem.Id && x.Room != null &&
                    x.Room.Id == SelectedAuditory.Id && (x.WeekType == SelectedWeekType.Key || SelectedWeekType.Key == WeekType.Simple || x.WeekType == WeekType.Simple))
                        .Select(x => x.SubGroup == SubGroup.Separateless ? x.Group.MembersCount : (double?)x.Group.MembersCount / 2.0).Sum() ?? 0;
                var overflow = totalStudentsCount + (SelectedSubGroup.Key == SubGroup.Separateless 
                    ? CurrentItem.Group.MembersCount : CurrentItem.Group.MembersCount / 2.0)
                    - SelectedAuditory.SeatsCount;
                if (overflow > 0)
                {
                    string seatWordEnd = "";
                    if (overflow % 10 == 1)
                    {
                        seatWordEnd = "о";
                    }
                    else if (overflow % 10 < 5 && overflow % 10 != 0)
                    {
                        seatWordEnd = "а";
                    }
                    warnings.Add(new Error(MessageType.Warning, string.Format(
                        "В выбранной аудитории недостаточно мест (превышение лимита: {0} мест{1})", overflow, seatWordEnd),
                        new[] { "SelectedGroup", "SelectedAuditory" }, 2
                    ));
                }
            }

            // преверки преподавателей и аудиторий
            if ((propertyName == "SelectedTeachers" || propertyName == "SelectedAuditory" || propertyName == "SelectedWeekType") &&
                SelectedTeachers.Any() && SelectedAuditory != null)
            {
                foreach (var selectedTeacher in SelectedTeachers)
                {
                    var busyman = ReferenceService.GetAll().Where(x =>
                        x.LessonHour.Id == CurrentItem.LessonHour.Id && 
                        (x.WeekType == SelectedWeekType.Key || x.WeekType == WeekType.Simple || SelectedWeekType.Key == WeekType.Simple) && CurrentItem.Id != x.Id &&
                        x.Room != null && x.Room.Id != SelectedAuditory.Id && x.Teachers.Any(
                        y => y.Id == selectedTeacher.Id)).Select(x => new
                        {
                            Teacher = x.Teachers.FirstOrDefault(y => y.Id == selectedTeacher.Id),
                            x.Room
                        }).FirstOrDefault();
                    if (busyman != null)
                    {
                        warnings.Add(new Error(MessageType.Warning, string.Format(
                            "В данное время у преподавателя \"{0}\" уже есть занятие в другой аудитории ({1})",
                            busyman.Teacher.Name, busyman.Room.Name),
                            new[] { "SelectedTeachers", "SelectedAuditory", "SelectedWeekType" }, 3)
                        );
                    }
                }
            }

            // проверка групп и предметов
            if ((propertyName == "SelectedSubject" || propertyName == "SelectedLessonType") &&
                SelectedGroup != null && SelectedSubject != null)
            {
                var planItem = _learningPlansService.GetPlanItem(
                    SelectedSubject, Schedule.CurrentSchedule, SelectedGroup);
                if (planItem != null)
                {
                    double actualCount = 0;
                    switch (SelectedLessonType.Key)
                    {
                        case LessonType.Lection:
                            if (planItem.LectionsCount < actualCount)
                            {
                                warnings.Add(new Error(MessageType.Warning, string.Format(
                                    "Плановое число лекций по данному предмету превышено (значение по плану - \"{0}\", фактическое значение -\"{1}\")",
                                    planItem.LectionsCount, actualCount),
                                    new[] { "SelectedSubject", "SelectedLessonType" }, 4)
                                    );
                            }
                            break;
                        case LessonType.Practice:
                            if (planItem.PracticeCount < actualCount)
                            {
                                warnings.Add(new Error(MessageType.Warning, string.Format(
                                    "Плановое число практических занятий по данному предмету превышено (значение по плану - \"{0}\", фактическое значение -\"{1}\")",
                                    planItem.PracticeCount, actualCount),
                                    new[] { "SelectedSubject", "SelectedLessonType" }, 5)
                                    );
                            }
                            break;
                    }
                }
            }

            return warnings;
        }
        #endregion
    }
}
