﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Caliburn.Micro;
using ScheduleMaker.Framework;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.ViewModels.Shared;

namespace ScheduleMaker.ViewModels.References
{
    /// <summary>
    /// Режимы редактирования
    /// </summary>
    public enum ReferenceStates
    {
        /// <summary>
        /// Без редактирования
        /// </summary>
        None,
        /// <summary>
        /// Создание новой записи
        /// </summary>
        NewRecord,
        /// <summary>
        /// Редактирование записи
        /// </summary>
        EditRecord,
        /// <summary>
        /// Обработка записи
        /// </summary>
        DataProcessing
    }

    /// <summary>
    /// Базовый класс моделей представлений справочников
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности справочника</typeparam>
    public abstract class BaseReferenceViewModel<TEntity> : BaseViewModel, IReference
        where TEntity : Record<TEntity>, new()
    {
        #region Fields

        /// <summary>
        /// Сервис для работы с записями справочника
        /// </summary>
        protected readonly BaseReferenceService<TEntity> ReferenceService;

        /// <summary>
        /// Агрегатор событий
        /// </summary>
        protected readonly IEventAggregator Events;

        /// <summary>
        /// Состояние справочника
        /// </summary>
        private ReferenceStates _state;

        /// <summary>
        /// Коллекция текущих записей
        /// </summary>
        private ObservableCollection<TEntity> _records;

        /// <summary>
        /// Текущий элемент справочника, над которым выполняются операции
        /// </summary>
        private TEntity _currentItem;

        /// <summary>
        /// Индекс текущего элемента в коллекции записей справочника
        /// </summary>
        private int _selectedIndex;

        /// <summary>
        /// Страница, инициировавшая создание новой записи
        /// </summary>
        protected Screens NewRecordCreator { get; set; }

        /// <summary>
        /// Дополнительная информация о странице, инициировавшей создание новой записи
        /// </summary>
        protected object NewRecordCreatorDetails;

        #endregion

        #region Constructors

        public BaseReferenceViewModel(BaseReferenceService<TEntity> referenceService, IEventAggregator events)
        {
            ReferenceService = referenceService;
            Events = events;
            _selectedIndex = -1;
            State = ReferenceStates.None;
            SelectedRecords = new HashSet<TEntity>();
        }

        #endregion

        #region Properties

        #region Can...

        /// <summary>
        /// Проверка возможности удаления записи
        /// </summary>
        public bool CanDelete
        {
            get { return (SelectedRecords.Count > 0 && State == ReferenceStates.None); }
        }

        /// <summary>
        /// Проверка возможности редактирования записи
        /// </summary>
        public bool CanEdit
        {
            get
            {
                return (SelectedRecords.Count == 1 && State == ReferenceStates.None);
            }
        }

        /// <summary>
        /// Проверка возможности создания записи
        /// </summary>
        public virtual bool CanNew
        {
            get { return (State == ReferenceStates.None); }
        }

        /// <summary>
        /// Проверка возможности применения фильтра записей
        /// </summary>
        public bool CanFilter
        {
            get { return (State == ReferenceStates.None); }
        }

        /// <summary>
        /// Проверка возможности сохранения записи
        /// </summary>
        public override bool CanSave
        {
            get { return (Error == null); }
        }


        #endregion

        protected bool IsInitialization { get; set; }

        /// <summary>
        /// Индекс текущего элемента в коллекции записей справочника
        /// </summary>
        public int SelectedIndex
        {
            set
            {
                _selectedIndex = value;
                NotifyOfPropertyChange(() => SelectedIndex);
            }
            get { return _selectedIndex; }
        }

        /// <summary>
        /// Состояние справочника
        /// </summary>
        public ReferenceStates State
        {
            get { return _state; }
            protected set
            {
                _state = value;
                if (_state == ReferenceStates.DataProcessing)
                {
                    return; 
                }

                IsEditMode = _state != ReferenceStates.None;
                if (!IsEditMode && NewRecordCreator!= Screens.None)
                {
                   NewRecordCreator = Screens.None;
                   Events.Publish(new ScreenEvent(CurrentScreen, ScreenOperations.UpdateTitle));
                }

                Events.Publish(new ReferncePanelsEvent());
                NotifyOfPropertyChange(() => State);
            }
        }

        public virtual IEnumerable<TEntity> AlternativeSourceOfRecords
        {
            get { return null; }
        } 

        /// <summary>
        /// Текущий элемент справочника, над которым выполняются операции
        /// </summary>
        public virtual TEntity CurrentItem
        {
            set
            {
                _currentItem = value;
                NotifyOfPropertyChange(() => CurrentItem);
                UpdateFields();
            }
            get
            {
                if (_currentItem == null)
                {
                    _currentItem = new TEntity();
                    _currentItem.SetDefaults();
                }

                return _currentItem;
            }
        }

        /// <summary>
        /// Выбранные записи справочника (для группового удаления)
        /// </summary>
        public HashSet<TEntity> SelectedRecords { get; private set; }

        /// <summary>
        /// Текст фильтра записей
        /// </summary>
        public string FilterText { get; set; }

        /// <summary>
        /// Текущие записи справочника
        /// </summary>
        public ObservableCollection<TEntity> Records
        {
            get { return _records; }
            protected set
            {
                _records = value;
                NotifyOfPropertyChange(() => Records);
            }
        }

        protected abstract Screens CurrentScreen { get; } 

        #endregion

        #region Methods

        /// <summary>
        /// Оповещение представления о необходимости обновить содержимое полей
        /// </summary>
        public virtual void UpdateFields()
        {
            
            NotifyOfPropertyChange(() => FilterText);
            NotifyOfPropertyChange(() => CanEdit);
            NotifyOfPropertyChange(() => CanDelete);
            NotifyOfPropertyChange(() => Error);
        }

        /// <summary>
        /// Отмена редактирования записи
        /// </summary>
        public virtual void Cancel()
        {
            IsEditMode = false;
            if (CurrentItem.Id != Guid.Empty)
            {
                var newItem = new TEntity { Id = CurrentItem.Id };
                var oldItem = Records.FirstOrDefault(rec => rec.Id == CurrentItem.Id);
                if (oldItem != null)
                {
                    SelectedIndex = Records.TakeWhile(rec => rec.Id != CurrentItem.Id).Count();
                    oldItem.CloneTo(newItem);
                }

                CurrentItem = newItem;
            }
            else
            {
                CurrentItem = null;
                if (SelectedRecords.Count == 1)
                {
                    var selected = SelectedRecords.First();
                    var newItem = new TEntity { Id = selected.Id };
                    selected.CloneTo(newItem);
                    CurrentItem = newItem;
                }
            }

            if (NewRecordCreator != Screens.None)
            {
                Events.Publish(new ScreenEvent(NewRecordCreator, ScreenOperations.DenyUpdateData));
                NewRecordCreator = Screens.None;
            }

            State = ReferenceStates.None;
            Warnings.Clear();
            NotifyOfPropertyChange(() => CanEdit);
            UpdateFields();
        }

        /// <summary>
        /// Удаление записи
        /// </summary>
        /// <returns></returns>
        public virtual bool Delete()
        {
            if (MessageBox.Show(
                "Удалить " + (SelectedRecords.Count == 1 ? "запись?" : "выбранные записи? \n(" + SelectedRecords.Count + " шт.)"),
                "Подтвердите действие", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var deletedRecs = new List<TEntity>();
                foreach (var record in SelectedRecords)
                {
                    deletedRecs.Add(record);
                    ReferenceService.Delete(record);
                }

                int selectedIndex = -1;
                foreach (var record in deletedRecs)
                {
                    selectedIndex = Records.TakeWhile(rec => rec.Id != record.Id).Count();
                    Records.Remove(Records.FirstOrDefault(rec => rec.Id == record.Id));
                }

                SelectedRecords.Clear();
                if (selectedIndex > -1 && Records.Count > 0)
                {
                    SelectedIndex = selectedIndex < Records.Count ? selectedIndex : selectedIndex - 1;
                }

                Events.Publish(new UpdateChildsEvent(CurrentScreen));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Переход в режим редактирование записи
        /// </summary>
        public virtual void Edit()
        {
            State = ReferenceStates.EditRecord;
            UpdateFields();
        }

        /// <summary>
        /// Переход в режим создания записи
        /// </summary>
        public virtual void New(Screens newRecordCreator = Screens.None, 
            object newRecordCreatorDetails = null)
        {
            NewRecordCreator = newRecordCreator;
            NewRecordCreatorDetails = newRecordCreatorDetails;
            CurrentItem = new TEntity();
            if (CurrentItem.SetDefaults())
            {
                State = ReferenceStates.NewRecord;
                UpdateFields();
            }
            else
            {
                State = ReferenceStates.NewRecord;
            }

            NotifyOfPropertyChange(() => CanSave);
        }

        /// <summary>
        /// Фильтрация запсей
        /// </summary>
        /// <param name="filterText"></param>
        public void Filter(string filterText)
        {
            FilterText = filterText;
            UpdateData();
        }

        /// <summary>
        /// Сохранение записи
        /// </summary>
        public virtual void Save()
        {
            if (CanSave)
            {
                try
                {
                    switch (_state)
                    {
                        case ReferenceStates.NewRecord:
                            ReferenceService.Add(CurrentItem);
                            Guid newRecordId = CurrentItem.Id;
                            State = ReferenceStates.DataProcessing;
                            Records.Add(CurrentItem);
                            CurrentItem = Records.FirstOrDefault(x => x.Id == newRecordId);
                            SelectedIndex = Records.TakeWhile(rec => 
                                rec.Id != CurrentItem.Id).Count();
                            SelectedRecords.Clear();
                            SelectedRecords.Add(CurrentItem);
                            break;
                        case ReferenceStates.EditRecord:
                            ReferenceService.Update(CurrentItem);
                            var record = Records.FirstOrDefault(rec => rec.Id == CurrentItem.Id);
                            CurrentItem.CloneTo(record);
                            UpdateData();
                            break;
                    }

                }
                catch (DbUpdateException e)
                {
                    MessageBox.Show(e.InnerException?.InnerException?.Message, "Ошибка БД",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                Events.Publish(new UpdateChildsEvent(CurrentScreen, CurrentItem?.Id, 
                    NewRecordCreatorDetails));
                if (NewRecordCreator != Screens.None)
                {
                    Events.Publish(new ScreenEvent(NewRecordCreator, 
                        ScreenOperations.DenyUpdateData, null, CurrentScreen));
                    NewRecordCreator = Screens.None;
                }

                State = ReferenceStates.None;
                Warnings.Clear();
            }
        }

        /// <summary>
        /// Обновление содержимого полей справочника
        /// </summary>
        public override void UpdateData()
        {
            if (IsInitialization)
            {
                return;
            }

            int selectedIndex = -1;
            State = ReferenceStates.DataProcessing;
            if (SelectedRecords.Count == 1)
            {
                selectedIndex = Records.TakeWhile(rec => rec.Id != CurrentItem.Id).Count();
            }

            var oldSelected = new List<TEntity>();
            foreach (var record in SelectedRecords)
            {
                oldSelected.Add(record);
            }

            SelectedRecords.Clear();
            var dbRecords = AlternativeSourceOfRecords ?? ReferenceService.GetAll();
            if (!string.IsNullOrEmpty(FilterText))
            {
                dbRecords = dbRecords.Where(record => record.Contains(FilterText));
            }

            Records = new ObservableCollection<TEntity>(dbRecords);
            foreach (var record in oldSelected)
            {
                var newRecord = Records.FirstOrDefault(rec => rec.Id == record.Id);
                if (newRecord != null)
                {
                    SelectedRecords.Add(newRecord);
                }
            }

            if (selectedIndex > -1 && SelectedIndex > -1)
            {
                SelectedIndex = selectedIndex;
            }

            NotifyOfPropertyChange(() => SelectedRecords);
            if (NewRecordCreator == Screens.None)
            {
                Cancel();
            }
            else
            {
               State = ReferenceStates.NewRecord;
            }
        }

        /// <summary>
        /// Обработчик события выбора записей
        /// </summary>
        /// <param name="e"></param>
        public void SelectedRowsChangeEvent(SelectionChangedEventArgs e)
        {
            if (State == ReferenceStates.DataProcessing && NewRecordCreator != Screens.None)
            {
                SelectedRecords.Clear();
                return;
            }

            foreach (var addedRow in e.AddedItems)
            {
                SelectedRecords.Add(addedRow as TEntity);
                Events.Publish(new ReferncePanelsEvent());
                NotifyOfPropertyChange(() => CanEdit);
            }

            foreach (var removedRow in e.RemovedItems)
            {
                SelectedRecords.Remove(removedRow as TEntity);
                Events.Publish(new ReferncePanelsEvent());
                NotifyOfPropertyChange(() => CanEdit);
            }

            if (SelectedRecords.Count == 1)
            {
                var selected = SelectedRecords.First();
                var newItem = new TEntity { Id = selected.Id };
                selected.CloneTo(newItem);
                CurrentItem = newItem;
            }
            else
            {
                CurrentItem = null;
            }


            NotifyOfPropertyChange(() => SelectedRecords);
            State = ReferenceStates.None;
        }

        /// <summary>
        /// Выбор записи по ее идентификатору
        /// </summary>
        /// <param name="recordId"></param>
        public void SelectRecord(Guid recordId)
        {
            UpdateData();
            SelectedRecords.Clear();
            var record = Records.FirstOrDefault(x => x.Id == recordId);
            SelectedRecords.Add(record);
            CurrentItem = record;
        }

        #endregion
    }
}