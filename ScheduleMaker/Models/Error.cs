﻿using System.Collections.Generic;

namespace ScheduleMaker.Models
{
    public enum MessageType
    {
        Error, Warning
    }

    public class Error
    {
        public Error(MessageType type, string message)
        {
            Message = message;
            Type = type;
            Fields = new List<string>();
            Code = null;

        }

        public Error(MessageType type, string message, string field)
            : this(type, message)
        {
            Fields.Add(field);
        }

        public Error(MessageType type, string message, IEnumerable<string> fields, int code)
            : this(type, message)
        {
            foreach (var field in fields)
            {
                Fields.Add(field);
            }
            
            Code = code;
        }

        public string Message { set; get; }

        public MessageType Type { set; get; }

        public ICollection<string> Fields { get; set; }

        public int? Code { get; set; }
    }
}
