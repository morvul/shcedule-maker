using System;
using ScheduleMaker.ViewModels;

namespace ScheduleMaker.Services
{
    /// <summary>
    /// Defines methods of a settings provider.
    /// </summary>
    public interface ISettingsService
    {
        #region Window settings

        /// <summary>
        /// Shift position of window from left.
        /// </summary>
        int WindowLeft { get; set; }

        /// <summary>
        /// Shift position of window from top.
        /// </summary>
        int WindowTop { get; set; }

        /// <summary>
        /// Window hieght.
        /// </summary>
        int WindowHeight { get; set; }

        /// <summary>
        /// Window width.
        /// </summary>
        int WindowWidth { get; set; }

        /// <summary>
        /// Whether  is the window in state Maximized.
        /// </summary>
        bool WindowMax { get; set; }

        #endregion

        /// <summary>
        /// Startup schedule.
        /// </summary>
        Guid CurrentScheduleId { get; set; }

        /// <summary>
        /// Startup group.
        /// </summary>
        Guid CurrentGroupId { get; set; }
        
        /// <summary>
        /// Startup screen.
        /// </summary>
        Screens CurrentScreen { get; set; }


        bool IsActionsExpanded { get; set; }

        bool IsReferencesExpanded { get; set; }

        bool IsSchedulesExpanded { get; set; }

        bool IsPrintingExpanded { get; set; }

        /// <summary>
        /// Startup teacher
        /// </summary>
        Guid CurrentTeacherId { get; set; }

        /// <summary>
        /// Startup auditory
        /// </summary>
        Guid CurrentAuditoryId { get; set; }

        string CommonScheduleTemplatePath { get; set; }

        string ResultDir { get; set; }

        string GroupScheduleTemplatePath { get; set; }
        TimeSpan BreakSize { get; set; }

        /// <summary>
        /// Load settings from the application properties.
        /// </summary>
        void Load();

        /// <summary>
        /// Save settings if they have been modified.
        /// </summary>
        void Save();

    }
}
