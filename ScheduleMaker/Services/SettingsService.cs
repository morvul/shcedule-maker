﻿using System;
using System.IO;
using System.Linq;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.ViewModels;

namespace ScheduleMaker.Services
{
    /// <summary>
    /// A class that just encapsulates the application settings.
    /// </summary>
    internal class SettingsService : ISettingsService
    {
        #region Constructors

        public SettingsService()
        {
            Load();
        }

        #endregion

        #region Fields

        private int _windowLeft;
        private int _windowTop;
        private int _windowHeight;
        private int _windowWidth;
        private bool _windowMax;
        private Screens _currentScreen;
        private bool _isActionsExpanded;
        private bool _isReferencesExpanded;
        private Guid _currentScheduleId;
        private Guid _currentGroupId;
        private Guid _currentTeacherId;
        private Guid _currentAuditoryId;
        private bool _isSchedulesExpanded;
        private bool _isPrintingExpanded;
        private string _commonScheduleTemplatePath;
        private string _resultDir;
        private string _groupScheduleTemplatePath;
        private TimeSpan _breakSize;

        #endregion

        #region Properties

        #region Window properties

        public int WindowLeft
        {
            get { return _windowLeft; }
            set
            {
                if (_windowLeft == value)
                    return;
                _windowLeft = value;
                Modified = true;
            }
        }

        public int WindowTop
        {
            get { return _windowTop; }
            set
            {
                if (_windowTop == value)
                    return;
                _windowTop = value;
                Modified = true;
            }
        }

        public int WindowHeight
        {
            get { return _windowHeight; }
            set
            {
                if (_windowHeight == value)
                    return;
                _windowHeight = value;
                Modified = true;
            }
        }

        public int WindowWidth
        {
            get { return _windowWidth; }
            set
            {
                if (_windowWidth == value)
                    return;
                _windowWidth = value;
                Modified = true;
            }
        }

        public bool WindowMax
        {
            get { return _windowMax; }
            set
            {
                if (_windowMax == value)
                    return;
                _windowMax = value;
                Modified = true;
            }
        }

        #endregion

        public Guid CurrentScheduleId
        {
            get { return _currentScheduleId; }
            set
            {
                if (_currentScheduleId == value)
                    return;
                _currentScheduleId = value;
                Modified = true;
            }
        }

        public Guid CurrentGroupId
        {
            get { return _currentGroupId; }
            set
            {
                if (_currentGroupId == value)
                    return;
                _currentGroupId = value;
                Modified = true;
            }
        }

        public Screens CurrentScreen
        {
            get { return _currentScreen; }
            set
            {
                if (_currentScreen == value)
                    return;
                _currentScreen = value;
                Modified = true;
            }
        }

        public bool IsActionsExpanded
        {
            get { return _isActionsExpanded; }
            set
            {
                if (_isActionsExpanded == value)
                    return;
                _isActionsExpanded = value;
                Modified = true;
            }
        }

        public bool IsReferencesExpanded
        {
            get { return _isReferencesExpanded; }
            set
            {
                if (_isReferencesExpanded == value)
                    return;
                _isReferencesExpanded = value;
                Modified = true;
            }
        }

        public bool IsSchedulesExpanded {
            get { return _isSchedulesExpanded; }
            set
            {
                if (_isSchedulesExpanded == value)
                    return;
                _isSchedulesExpanded = value;
                Modified = true;
            }
        }

        public bool IsPrintingExpanded
        {
            get { return _isPrintingExpanded; }
            set
            {
                if (_isPrintingExpanded == value)
                    return;
                _isPrintingExpanded = value;
                Modified = true;
            }
        }

        public Guid CurrentTeacherId
        {
            get { return _currentTeacherId; }
            set
            {
                if (_currentTeacherId == value)
                    return;
                _currentTeacherId = value;
                Modified = true;
            }
        }

        public Guid CurrentAuditoryId
        {
            get { return _currentAuditoryId; }
            set
            {
                if (_currentAuditoryId == value)
                    return;
                _currentAuditoryId = value;
                Modified = true;
            }
        }

        public string CommonScheduleTemplatePath {
            get { return _commonScheduleTemplatePath; }
            set
            {
                if (_commonScheduleTemplatePath == value)
                    return;
                _commonScheduleTemplatePath = value;
                Modified = true;
            }
        }

        public string ResultDir
        {
            get { return _resultDir; }
            set
            {
                if (_resultDir == value)
                    return;
                _resultDir = value;
                Modified = true;
            }
        }

        public string GroupScheduleTemplatePath {
            get { return _groupScheduleTemplatePath; }
            set
            {
                if (_groupScheduleTemplatePath == value)
                    return;
                _groupScheduleTemplatePath = value;
                Modified = true;
            }
        }

        public TimeSpan BreakSize {
            get { return _breakSize; }
            set
            {
                if (_breakSize == value)
                    return;
                _breakSize = value;
                LessonHour.BreakSize = value;
                Modified = true;
            }
        }

        /// <summary>
        /// Returns whether the settings have been modified.
        /// </summary>
        /// <value>
        ///   <c>true</c> if modified; otherwise, <c>false</c>.
        /// </value>
        public bool Modified { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Actually update and save the application settings.
        /// </summary>
        private void SaveSettings()
        {
            Properties.Settings.Default.WindowLeft = WindowLeft;
            Properties.Settings.Default.WindowTop = WindowTop;
            Properties.Settings.Default.WindowWidth = WindowWidth;
            Properties.Settings.Default.WindowHeight = WindowHeight;
            Properties.Settings.Default.WindowMax = WindowMax;
            Properties.Settings.Default.CurrentScheduleId = CurrentScheduleId;
            Properties.Settings.Default.CurrentGroupId = CurrentGroupId;
            Properties.Settings.Default.CurrentTeacherId = CurrentTeacherId;
            Properties.Settings.Default.CurrentAuditoryId = CurrentAuditoryId;
            Properties.Settings.Default.CurrentScreen = (int)CurrentScreen;
            Properties.Settings.Default.IsOperationsExpanded = IsReferencesExpanded;
            Properties.Settings.Default.IsSchedulesExpanded = IsSchedulesExpanded;
            Properties.Settings.Default.IsPrintingExpanded = IsPrintingExpanded;
            Properties.Settings.Default.IsActionsExpanded = IsActionsExpanded;
            Properties.Settings.Default.CommonScheduleTemplatePath = CommonScheduleTemplatePath;
            Properties.Settings.Default.GroupScheduleTemplatePath = GroupScheduleTemplatePath;
            Properties.Settings.Default.ResultDir = ResultDir;
            Properties.Settings.Default.BreakSize = BreakSize;
            Properties.Settings.Default.Save();
            Modified = false;
        }

        public void Load()
        {
            WindowLeft = Properties.Settings.Default.WindowLeft;
            WindowTop = Properties.Settings.Default.WindowTop;
            WindowWidth = Properties.Settings.Default.WindowWidth;
            WindowHeight = Properties.Settings.Default.WindowHeight;
            WindowMax = Properties.Settings.Default.WindowMax;
            CurrentScheduleId = Properties.Settings.Default.CurrentScheduleId;
            CurrentGroupId = Properties.Settings.Default.CurrentGroupId;
            CurrentTeacherId = Properties.Settings.Default.CurrentTeacherId;
            CurrentAuditoryId = Properties.Settings.Default.CurrentAuditoryId;
            CurrentScreen = (Screens)Properties.Settings.Default.CurrentScreen;
            IsReferencesExpanded = Properties.Settings.Default.IsOperationsExpanded;
            IsActionsExpanded = Properties.Settings.Default.IsActionsExpanded;
            IsSchedulesExpanded = Properties.Settings.Default.IsSchedulesExpanded;
            IsPrintingExpanded = Properties.Settings.Default.IsPrintingExpanded;
            CommonScheduleTemplatePath = 
                GetFullPath(Properties.Settings.Default.CommonScheduleTemplatePath);
            GroupScheduleTemplatePath = Properties.Settings.Default.GroupScheduleTemplatePath;
            ResultDir = GetFullPath(Properties.Settings.Default.ResultDir);
            BreakSize = Properties.Settings.Default.BreakSize;
            Modified = false;
        }

        public void Save()
        {
            if (!Modified)
                return;
            SaveSettings();
        }

        private string GetFullPath(string path)
        {
            if (path == "Desktop")
            {
                path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + '\\';
            }

            if (!File.Exists(path) && !Directory.Exists(path))
            {
                if (path.FirstOrDefault() != '\\')
                {
                    path = '\\' + path;
                }

                string fullPath = Environment.CurrentDirectory + path;
                if (!File.Exists(fullPath) && !Directory.Exists(path))
                {
                    return null;
                }

                path = fullPath;
            }
            else
            {
                path = Path.GetFullPath(path);
            }

            return path;
        }

        #endregion
    }
}
