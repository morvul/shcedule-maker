﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using ScheduleMaker.Logic.Helpers;

namespace ScheduleMaker.Helpers
{
    public class EnumDescriptionConverter : MarkupExtension, IValueConverter
    {
        public bool NeedBrackets { set; get; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum myEnum = (Enum)value;
            string description = myEnum.Description();
            return NeedBrackets ? '(' + description + ')' : description;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
