﻿using System;
using ScheduleMaker.ViewModels;

namespace ScheduleMaker.Framework
{
    public class UpdateChildsEvent
    {
        public UpdateChildsEvent(Screens sender)
        {
            RecordCreator = sender;
            UpdatedRecordId = Guid.Empty;
            RecordCreatorDetails = null;
        }

        public UpdateChildsEvent(Screens sender, Guid updatedRecordId)
        {
            RecordCreator = sender;
            UpdatedRecordId = updatedRecordId;
            RecordCreatorDetails = null;
        }

        public UpdateChildsEvent(Screens sender, Guid? updatedRecordId, 
            object recordCreatorDetails)
        {
            RecordCreator = sender;
            UpdatedRecordId = updatedRecordId;
            RecordCreatorDetails = recordCreatorDetails;
        }

        public Screens RecordCreator { get; private set; }

        public object RecordCreatorDetails { get; private set; }

        public Guid? UpdatedRecordId { get; private set; }
    }
}
