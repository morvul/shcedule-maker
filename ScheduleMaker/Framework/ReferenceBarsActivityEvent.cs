﻿using ScheduleMaker.ViewModels.References;

namespace ScheduleMaker.Framework
{
    public class ReferenceBarsActivityEvent 
    {
        public ReferenceBarsActivityEvent(IReference sender)
        {
            IsActive = sender != null;
            Reference = sender;
        }

        public IReference Reference { get; private set; }

        public bool IsActive { get; private set; }
    }
}
