﻿using ScheduleMaker.Logic.Model;

namespace ScheduleMaker.Framework
{
    public enum GroupScheduleOperations
    {
        None,
        NewLesson,
        DeleteLesson,
        EditLesson
    }

    public class GroupScheduleEvent
    {
        public GroupScheduleEvent()
        {
            Operation = GroupScheduleOperations.None;
            Lesson = null;
        }

        public GroupScheduleEvent(GroupScheduleOperations operation, Lesson lesson)
            : this()
        {
            Operation = operation;
            Lesson = lesson;
        }

        public Lesson Lesson { get; private set; }

        public GroupScheduleOperations Operation { get; private set; }
    }
}
