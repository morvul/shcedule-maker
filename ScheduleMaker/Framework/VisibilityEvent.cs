﻿using System;

namespace ScheduleMaker.Framework
{
    public class VisibilityEvent
    {
        public VisibilityEvent(bool isVisible, Type sender)
        {
            IsVisible = isVisible;
            Sender = sender;
        }

        public Type Sender { get; private set; }

        public bool IsVisible { get; private set; }
    }
}
