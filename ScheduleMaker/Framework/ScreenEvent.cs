﻿using ScheduleMaker.ViewModels;

namespace ScheduleMaker.Framework
{
    public enum ScreenOperations
    {
        None,
        NewRecord,
        DenyUpdateData,
        UpdateTitle,
        GoToPrevious
    }

    public class ScreenEvent
    {
        public ScreenEvent(Screens screen, ScreenOperations operation = ScreenOperations.None,
            object details = null, Screens sender = Screens.None)
        {
            Screen = screen;
            Operation = operation;
            Details = details;
            Sender = sender;
        }

        public ScreenEvent(ScreenOperations operation, 
            object details = null, Screens sender = Screens.None)
        {
            Operation = operation;
            Screen = Screens.None;
            Details = details;
            Sender = sender;
        }

        public Screens Screen { get; private set; }

        public Screens Sender { get; private set; }

        public ScreenOperations Operation { get; private set; }

        public object Details { get; private set; }
    }
}
