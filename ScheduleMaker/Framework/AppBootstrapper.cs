using System;
using System.Collections.Generic;
using Caliburn.Micro;
using ScheduleMaker.Logic.Model;
using ScheduleMaker.Logic.Services;
using ScheduleMaker.Services;
using ScheduleMaker.ViewModels.References;
using ScheduleMaker.ViewModels.References.ComplexReferences;
using ScheduleMaker.ViewModels.Shared;

namespace ScheduleMaker.Framework
{
    public class AppBootstrapper : BootstrapperBase
    {
        SimpleContainer _container;

        public AppBootstrapper()
        {
            Start();
        }

        protected override void Configure()
        {
            _container = new SimpleContainer();
            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<IEventAggregator, EventAggregator>();
            _container.Singleton<ISettingsService, SettingsService>();
            _container.Singleton<RoomsService>();
            _container.Singleton<ScheduleGeneratorService>();
            _container.Singleton<TeachersService>();
            _container.Singleton<GroupsService>();
            _container.Singleton<SubjectsService>();
            _container.Singleton<SchedulesService>();
            _container.Singleton<HolidayService>();
            _container.Singleton<LessonsService>();
            _container.Singleton<LessonHoursService>();
            _container.Singleton<LearningPlansService>();
            _container.Singleton<PrintingService>(); 
            _container.Singleton<MainViewModel>();
            _container.Singleton<PanelViewModel>();
            _container.Singleton<TeachersViewModel>();
            _container.Singleton<RoomsViewModel>();
            _container.Singleton<SubjectsViewModel>();
            _container.Singleton<LearningPlanViewModel>();
            _container.Singleton<ScheduleGeneratorViewModel>();
            _container.Singleton<GroupsViewModel>();
            _container.Singleton<SchedulesViewModel>();
            _container.Singleton<DataContext>();
            _container.PerRequest<EditBarViewModel>();
            _container.PerRequest<FindPanelViewModel>();
            _container.PerRequest<CommonScheduleViewModel>();
            _container.PerRequest<TeacherScheduleViewModel>();
            _container.PerRequest<AuditoryScheduleViewModel>();
            _container.PerRequest<GroupScheduleViewModel>();
            _container.PerRequest<LessonHourEditorViewModel>();
            _container.PerRequest<LessonsViewModel>();
            _container.PerRequest<SettingsViewModel>();
            _container.PerRequest<HolidaysViewModel>();
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = _container.GetInstance(service, key);
            if (instance != null)
            {
                return instance;
            }

            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {
            DisplayRootViewFor<MainViewModel>();
        }
    }
}